
#  Main Features

## User management
- User info and management
- User role based privilidges
- Labels to mark user characters
- Wechat(s) used for sale changles

## Custoemr management
- Customers grouped in Countries
- Customer info and management
- Customer labels to record characters
- Sale come in changle
- Customer history order number
- Customer's history orders

## Course management
- Course info and management
- Course documents and download (can be ziped before download)
- Course search

## Order management
- Orders management in status (admin confirming, waiting assignemnt,  bg  assistent confirm, processing, QC verifying, complete, revisino, error/refund)
- New order
  - link sale chanle (for statistic and report)
  - link course by search
  - link customer by search
  - mark key info in labels
  - order deadline and real deadline in different timezone
  - payment with bound, part and full
  - payment with different currency (could be transfered to specific currency for report and accunting)
  - payment evidenc upload
  - add documents in different group
  - order note and customer expecation in detail
  - auto process order
- Order management
  - payment confirm by admin or accunting
  - order update, delete and status management
  - create new order by copy existing order
  - order document and customer note download to a zip file
  - order operation log
  - order search
  - order sort in create time, deadline, real deadline
  - history orders management
- admin confirming
  - confirm payment
  - confirm order
- waiting assign
  - assistants search
  - Assign to assistants with currency and price
  - invite Quality Control
  - invite translator
- waiting assistant confirm
  - multi assistants confirm status
  - accept order
  - reject order
  - moving to waiting again
- processing
  - who is processing
  - complete and upload assignment result
- ...
- ...

## privilidge
- different roles have different privilidges, user interface will be different
- different roles have different operation panels
- different roles have different view for the orders, customers, reports ...


## report in graph
- orders trends
- orders in different countries and schooles
- orders in different
- customers and new customers in different countries
- orders created by sales in create time and deadline (matrix)
- orders assigned to assistants in deadline (matrix)
- quick access to sales' assistants' orders through the matrix
- sales orders in different status
- sale KPI panel
  - revenue overview and orders details (by month)
  - KPI overview and orders details (by month)
  - revenue in different currency overview and orders details (by month)
  - daily accument in accunting currency by days (by month)
  - Bar chart for Revenue by month in different currency
  - Bar chart for KPI overview comparing with other staffs

import React, { Component } from "react";
import Dashboard from './Dashboard'
import { withStyles, createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import cyan from '@material-ui/core/colors/cyan';
import pink from '@material-ui/core/colors/pink';
import './App.css'

import abc from './common/afk'
const styles = theme => ({})



const theme = createMuiTheme({
    palette: {
        primary: cyan,
        primary2Color: cyan[300],
        primary3Color: cyan[100],
        secondary: pink,
        // accent1Color: colors.pink600,
        // accent2Color: colors.pink300,
        // accent3Color: colors.pink100,
        // textColor: colors.grey800,
        // canvasColor: colors.white,
        // borderColor: colors.grey300,
        // pickerHeaderColor: colors.pink300,
    }
})

class App extends Component {

  render() {
    console.log(theme)
    return (
        <div>
            {/*<AFK />*/}
          <MuiThemeProvider theme={theme} >
            <Dashboard />
          </MuiThemeProvider  >
        </div>
    );
  }
}

export default withStyles(styles)(App);

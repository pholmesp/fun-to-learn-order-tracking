import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import PersonIcon from '@material-ui/icons/Person';
import AddIcon from '@material-ui/icons/Add';
import Popover from '@material-ui/core/Popover';
import blue from '@material-ui/core/colors/blue';

const styles = theme => ({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  button: {
    margin: theme.spacing.unit,
  },
});

@withStyles(styles)
@inject('userStore', 'ordersStore')
@withRouter
@observer
export default class SaleFuncBar extends React.Component {
  state = {
    anchorEl: null,

  }
  
  componentDidMount(){
    this.props.userStore.getAllUsers()
  }
  handleOrderStatus = event => {
    this.setState({
      anchorEl: event.currentTarget,
      assignToRole: event.currentTarget.value,
    });
  };

  handleAssign = event => {
    this.setState({
      anchorEl: event.currentTarget,
      assignToRole: event.currentTarget.value,
    });
  };

  handleAssignClose = () => {
    this.setState({
      anchorEl: null,
      assignToRole: null,
    });
  };

  handleAssignToUser(user){
    let path = this.props.match.path.split('/')[1]
    console.log(path)
    let ordersChecked = [...this.props.ordersStore.ordersChecked.values()]
    for(let order of ordersChecked){
      console.log(order)
      if(order.status == path){
        order.currentProcessorRole = user.role
        order.currentProcessor_iduser = user.iduser
        order.status = 'processing'
        this.props.ordersStore.updateOrder(order)
        this.handleAssignClose
      }
    }
    
  }

  render() {
    const { anchorEl, assignToRole } = this.state
    const { classes, onClose, selectedValue, match } = this.props;
    const path = match.path.split('/')[1]
    const { allUsers } = this.props.userStore
    const open = Boolean(anchorEl);
    console.log(this.props)

    return (
      <div className={classes.root}>
        {
          path == 'confirming' ? 
            <div>
              <Button variant="contained" onClick={this.handleOrderStatus} value="waiting" color="secondary"  className={classes.button}>
                Confirm
              </Button>
              <Button variant="contained" onClick={this.handleOrderStatus} value="error" color="secondary"  className={classes.button}>
                Reject
              </Button>
            </div>:
          path == 'waiting' ?
            <div>
              <Button onClick={this.handleAssign} value="counsellor" variant="contained" color="secondary" className={classes.button}>
                Assign To Counsellor
              </Button>
              <Button variant="contained" onClick={this.handleAssign} value="saleman" color="secondary" className={classes.button}>
                Assinto To Sale
              </Button>
              <Button  onClick={this.handleAssign} value="qa" variant="contained" color="secondary" className={classes.button}>
                Assign TO QA
              </Button>
              <Button  onClick={this.handleAssign} value="translater" variant="contained" color="secondary" className={classes.button}>
                Assign TO Translater
              </Button>
              <Button variant="contained" onClick={this.handleOrderStatus} value="error" color="secondary"  className={classes.button}>
                Reject
              </Button>
            </div>:
          path === 'processing' ?
            <div>
              <Button variant="contained" onClick={this.handleAssign} value="complete" color="secondary"  className={classes.button}>
                Complete
              </Button> 
              <Button variant="contained" onClick={this.handleOrderStatus} value="waiting" color="secondary"  className={classes.button}>
                Move to Waiting
              </Button>
              <Button variant="contained"  onClick={this.handleOrderStatus} value="error" color="secondary"  className={classes.button}>
                Reject
              </Button>
            </div>:
          path === 'complete' ?
            <div>
              <Button variant="contained" onClick={this.handleOrderStatus} value="waiting" color="secondary"  className={classes.button}>
                Move to Waiting
              </Button> 
            </div>:
          path === 'other' ? 
            <div>
              <Button variant="contained" onClick={this.handleOrderStatus} value="waiting" color="secondary"  className={classes.button}>
                Move to Waiting
              </Button> 
          </div> : ''
        }
            <Popover
              id="simple-popper"
              open={open}
              anchorEl={anchorEl}
              onClose={this.handleAssignClose}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'center',
              }}
              transformOrigin={{
                vertical: 'top',
                horizontal: 'center',
              }}
            >
              <List>
                {allUsers && allUsers.map(user => {
                  if(user.role == assignToRole)
                  return (
                    <ListItem button onClick={() => this.handleAssignToUser(user)} key={user.iduser}>
                      <ListItemText primary={user.name} secondary={user.email} />
                    </ListItem>
                  )
                  })}
              </List>
            </Popover>
            <hr />
      </div>
    );
  }
}
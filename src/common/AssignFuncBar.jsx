import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { inject, observer } from 'mobx-react';
import { withRouter, Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import TextField from '@material-ui/core/TextField';
import Popover from '@material-ui/core/Popover';
import blue from '@material-ui/core/colors/blue';
import Divider from '@material-ui/core/Divider';

import moment from 'moment';
import { PermissibleRender } from '@brainhubeu/react-permissible';

import loading from './loading'
import {addOrderComment} from '../util/addOrderComment'
import {updateOrderStatus} from '../util/updateOrder'
import {notifyXieshouInEmail} from '../util/notifyInEmail'
import authStore from "../stores/authStore";


const styles = theme => ({
  root:{
    
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  button: {
    margin: theme.spacing.unit,
  },
});

@withStyles(styles)
@inject('userStore', 'ordersStore', 'authStore', 'helperStore')
@withRouter
@observer
export default class AssignFuncBar extends React.Component {
  state = {
    anchorEl: null,
    checkedUser: [],
    search: ''
  }
  
  componentDidMount(){
    this.props.userStore.getAllUsers()
  }


  handleOrderStatus = event => {
    let ordersChecked = [...this.props.ordersStore.ordersChecked.values()]
    for(let order of ordersChecked){
      if(order.status == this.props.currentStatus){
        updateOrderStatus(order, event.currentTarget.value)
        // if(order.orderPackage_idorderPackage){
        //   this.props.ordersStore.updatePackageStatus(order.orderPackage_idorderPackage, event.currentTarget.value)
        // }
      }
    }
  };

  handleOrderMoveToWaiting = event => {
    let ordersChecked = [...this.props.ordersStore.ordersChecked.values()]
    const targetValue = event.currentTarget.value
    for(let order of ordersChecked){
      if(order.status == this.props.currentStatus){
        if(targetValue === 'waiting'){
          if(window.confirm('Are you sure to move this order to waiting? assigned assistants will be deleted!! you can find the operation detail at comments. ')){
            this.props.ordersStore.deleteOrderResultByOrderId(order.idorder).then(res => {
              addOrderComment(order, this.props.authStore.currentUser.iduser, `${this.props.authStore.currentUser.name}(${this.props.authStore.currentUser.iduser}) deleted all assistants. `)
              updateOrderStatus(order, targetValue)
            })
          } 
        }
      }
    }
  };
  handleOrderConfirm = event => {
    let ordersChecked = [...this.props.ordersStore.ordersChecked.values()]
    const targetValue = event.currentTarget.value
    for(let order of ordersChecked){
      if(order.status === 'confirming' && targetValue === 'waiting'){
        if(order.paymentVerify === 'verified'){
          // this.props.ordersStore.deleteOrderResultByOrderId(order.idorder).then(res => {
            addOrderComment(order, this.props.authStore.currentUser.iduser, `${this.props.authStore.currentUser.name}(${this.props.authStore.currentUser.iduser}) confirmed the order. `)
            updateOrderStatus(order, targetValue)
          // })
        }else{
          alert('The payment does NOT verified!, please verify (admin or account) the payment first! ')
        }
      }
    }
  };

  handleOrderComplete = event => {
    let ordersChecked = [...this.props.ordersStore.ordersChecked.values()]
    const targetValue = event.currentTarget.value
    for(let order of ordersChecked){
      if(targetValue === 'complete'){
        if(order.paymentStatus === 'full paid' && order.paymentVerify === 'verified'){
          // this.props.ordersStore.deleteOrderResultByOrderId(order.idorder).then(res => {
            // addOrderComment(order, this.props.authStore.currentUser.iduser, `${this.props.authStore.currentUser.name}(${this.props.authStore.currentUser.iduser}) close this order as complete. `)
          // })
          let realDeadlineMonth = moment(order.realDeadline).format('MM')
          if(realDeadlineMonth===moment.tz().format('MM')) {
            alert('This order has been archived, please find it in month ' + realDeadlineMonth + ' of History Orders. ')
          }
          updateOrderStatus(order, targetValue)
        }else if(order.paymentStatus !== 'full paid'){
          alert('The order does not FULL PAID, please contact sale to update the payment as full paid. ')
        }else if(order.paymentVerify !== 'verified'){
          alert('The full payment does NOT verified!, please verify (admin or account) the payment first! ')
        }
      }
    }
  };

  handleAssign = event => {
    this.setState({
      anchorEl: event.currentTarget,
      assignToRole: event.currentTarget.value,
    });
  };

  handleAssignClose = () => {
    this.setState({
      anchorEl: null,
      assignToRole: null,
    });
  };

  inviteUser(order, user) {
    let submitInvitation = {}
    submitInvitation['orders_idorder'] = order.idorder 
    submitInvitation['role'] = this.state.assignToRole
    // submitInvitation['name'] = user.name
    submitInvitation['user_iduser'] = user.iduser
    this.props.ordersStore.updateInvitation(submitInvitation)
    .then(res=>
      addOrderComment(order, this.props.authStore.currentUser.iduser, `${this.props.authStore.currentUser.name}(id: ${this.props.authStore.currentUser.iduser}) invited ${user.name} (id: ${user.iduser}) to join ${order.name} (${order.idorder})`))
        .then(()=>{
          //we notify the QA when there is an invitation
          console.log('QA invitation ws is connected!!')
          let message={
            orderID:order.idorder,
            to_salesID:order.user_iduser,
            to_xieshouID:0,
            to_qaID:user.iduser,
            from_iduser:this.props.authStore.currentUser.iduser,
            type:5
          }
          // console.log(message)
          authStore.ws.send(JSON.stringify(message))
          //
        })
  }
  handleOrderDelete = () => {
    let ordersChecked = [...this.props.ordersStore.ordersChecked.values()]
    for(let order of ordersChecked){
      if(window.confirm('Are you sure to delete order '+order.name +" ?")){
        this.props.ordersStore.deleteOrder(order.idorder)

      }
    }
  }

  inputValidate(){
    let validateStatus = false
    if(!this.state.currency_cost){
      this.setState({currency_costValidate: 'Currency can not be null'})
      validateStatus = true
    }
    if(!this.state.cost){
      this.setState({costValidate: 'Cost can not be null'})
      validateStatus = true
    }
    return validateStatus
  }

  assignToCounsellor(order, user){
    if(this.inputValidate()) return alert('Currency and cost to writer can not be empty.')


    let submitOrder = {}
    submitOrder['orders_idorder'] = order.idorder 
    submitOrder['dateTime'] = moment.tz().format('YYYY-MM-DDTHH:mm:ss')
    submitOrder['assigned_iduser'] = user.iduser
    this.props.ordersStore.updateUserOrder(submitOrder).then(res =>{
      notifyXieshouInEmail(user.email, user.name, order.name, order.fileUrl, 'CustomerWords.txt', order.description)
      .then(res => {
        console.log(res)
        if(res.status === 200){
          alert(`Counsellor ${user.name} has been notified in email ${user.email}.`)
                //ws notify the counsellor and salesman
                console.log('xieshou invitation ws is connected!!')
                let message={
                  orderID:order.idorder,
                  to_salesID:order.user_iduser,
                  to_xieshouID:user.iduser,
                  to_qaID:0,
                  from_iduser:this.props.authStore.currentUser.iduser,
                  type:4
                };
                // console.log(message)
                authStore.ws.send(JSON.stringify(message))
                //
        }else{
          alert(`Notify Error!!!  please check ${user.name}\'s email ${user.email}, and contact with ${user.name} to confirm the assignment assigned correctly. `)
        }
      })


      let orderObj = {} 
      let fieldStatus = {}
      let fieldRole = {}
      let fieldIduser = {}
      let filedCostCurrentcy = {}
      let filedCost = {}
      orderObj['idorder'] = order.idorder
      fieldStatus['key'] = 'status'
      fieldStatus['value'] = 'waiting writer'
      // fieldStatus['value'] = 'processing'
      fieldRole['key'] = 'currentProcessorRole'
      fieldRole['value'] = user.name
      fieldIduser['key'] = 'currentProcessor_iduser'
      fieldIduser['value'] = user.iduser
      filedCostCurrentcy['key'] = 'currency_cost'
      filedCostCurrentcy['value'] = this.state.currency_cost
      filedCost['key'] = 'cost'
      filedCost['value'] = this.state.cost
      orderObj['fields'] = [fieldStatus, fieldRole, fieldIduser, filedCostCurrentcy, filedCost]

      this.props.ordersStore.updateOrder(orderObj)
      // if(order.orderPackage_idorderPackage){
      //   this.props.ordersStore.updatePackageStatus(order.orderPackage_idorderPackage, 'waiting writer')
      // }
    })  
  }

  handleAssignUser(role){
    let ordersChecked = [...this.props.ordersStore.ordersChecked.values()]
    if(ordersChecked.length===0) return alert('Order(s) should be choosed')
    if(this.state.checkedUser.length===0) return alert('User(s) should be choosed')
    for(let order of ordersChecked){
      for(let user of this.state.checkedUser){
        if(order.status == this.props.currentStatus){
          if(role==='counsellor') this.assignToCounsellor(order, user)
          else this.inviteUser(order, user)
        }
      }
      this.setState({checkedUser: []}) 
      this.handleAssignClose() 
    }
  }
  
  handleCheckUser(user){
    let users = this.state.checkedUser 
    if(this.state.checkedUser.indexOf(user)===-1){
      users.push(user)
      this.setState({checkedUser: users})
    }else{
      users = users.filter(item => item !== user)
      this.setState({checkedUser: users})
    }
  }
  handleInputChange = (event) => {
    let {id, value} = event.target 
    console.log(id, value)
    if((''+value).length ===0){
      this.setState({[id]:value, [id+'Validate']: id+' can not be null'})
    }else{
      this.setState({[id]:value, [id+'Validate']: ''})
    }
  }
  render() {
    const { anchorEl, assignToRole } = this.state
    const { classes, onClose, selectedValue, match, currentStatus, userStore, authStore, ordersStore, helperStore} = this.props;
    const allUsers = userStore.Users.filter(ele=>ele.status==='valid')

    let filtedUsers = []
    if(this.state.search){
      for(let ele of allUsers){
        if(JSON.stringify(ele).toLowerCase().indexOf(this.state.search.toLowerCase())>=0){
          filtedUsers.push(ele)
        }
      }
    }else{
      filtedUsers = allUsers
    }
    const open = Boolean(anchorEl);
    // console.log(this.props)

    // userStore.isLoading || authStore.isLoading || ordersStore.isLoading || !allUsers ? loading : ''

    let adminOperations = 
        currentStatus == 'confirming' ? 
          <Grid>
            <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>
              <Button variant="outlined" onClick={this.handleOrderConfirm} value="waiting" color="secondary"  className={classes.button}>
                Confirm
              </Button>
              <Button variant="outlined" onClick={this.handleOrderComplete} value="complete" color="secondary"  className={classes.button}>
                Complete
              </Button> 
              <Button variant="outlined" onClick={this.handleOrderStatus} value="error" color="secondary"  className={classes.button}>
                Reject Order
              </Button>
            </PermissibleRender>  
            <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['saleman']} oneperm>
              <Button variant="outlined" onClick={this.handleOrderDelete} value="waiting" color="secondary"  className={classes.button}>
                Delete
              </Button> 
            </PermissibleRender>  
          </Grid>:
        currentStatus == 'waiting' ?
          <Grid>
            {/* <Button variant="outlined" onClick={this.handleAssign} value="saleman" color="secondary" className={classes.button}>
              Assinto To Sale
            </Button> */}
            <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>
              <Button onClick={this.handleAssign} value="counsellor" variant="outlined" color="secondary" className={classes.button}>
                Assign To Counsellors
              </Button>
              <Button  onClick={this.handleAssign} value="qa" variant="outlined" color="secondary" className={classes.button}>
                Invite QA
              </Button>
              <Button  onClick={this.handleAssign} value="translater" variant="outlined" color="secondary" className={classes.button}>
                Invite Translator
              </Button>
            </PermissibleRender>
            <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>
              <Button variant="outlined" onClick={this.handleOrderComplete} value="complete" color="secondary"  className={classes.button}>
                Complete
              </Button> 
              <Button variant="outlined"  onClick={this.handleOrderStatus} value="error" color="secondary"  className={classes.button}>
                Reject Order
              </Button>
            </PermissibleRender>
          </Grid>:
        currentStatus === 'waiting writer' ?
          <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>
            <Button variant="outlined" onClick={this.handleOrderMoveToWaiting} value="waiting" color="secondary"  className={classes.button}>
              Move to Waiting
            </Button>
            <Button variant="outlined" onClick={this.handleOrderStatus} value="processing" color="secondary"  className={classes.button}>
              Move to Process
            </Button>
            <Button variant="outlined" onClick={this.handleOrderComplete} value="complete" color="secondary"  className={classes.button}>
              Complete
            </Button> 
            <Button variant="outlined"  onClick={this.handleOrderStatus} value="error" color="secondary"  className={classes.button}>
              Reject Order
            </Button>
          </PermissibleRender>:
        currentStatus === 'processing' ?
          <Grid>
            <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>
              <Button  onClick={this.handleAssign} value="qa" variant="outlined" color="secondary" className={classes.button}>
                Invite QA
              </Button>
              <Button  onClick={this.handleAssign} value="translater" variant="outlined" color="secondary" className={classes.button}>
                Invite Translator
              </Button>
            </PermissibleRender>
            <Button variant="outlined" onClick={this.handleOrderComplete} value="complete" color="secondary"  className={classes.button}>
              Confirm & Complete
            </Button> 
            <Button variant="outlined" onClick={this.handleOrderMoveToWaiting} value="waiting" color="secondary"  className={classes.button}>
              Move to Waiting
            </Button>
            <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>
              <Button variant="outlined"  onClick={this.handleOrderStatus} value="error" color="secondary"  className={classes.button}>
                Reject Order
              </Button>
            </PermissibleRender>
          </Grid>:
        currentStatus === 'verifying' ?
          <Grid>
            <Button variant="outlined" onClick={this.handleOrderComplete} value="complete" color="secondary"  className={classes.button}>
              Confirm & Complete
            </Button> 
            <Button variant="outlined" onClick={this.handleOrderStatus} value="modifying" color="secondary"  className={classes.button}>
              Need Modifying
            </Button>
            <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>
              <Button variant="outlined" onClick={this.handleOrderComplete} value="complete" color="secondary"  className={classes.button}>
                Complete
              </Button> 
              <Button variant="outlined"  onClick={this.handleOrderStatus} value="error" color="secondary"  className={classes.button}>
                Reject Order
              </Button>
            </PermissibleRender>
          </Grid>:
        currentStatus === 'complete' ?
          <Grid>
            <Button variant="outlined" onClick={this.handleOrderMoveToWaiting} value="waiting" color="secondary"  className={classes.button}>
              Move to Waiting
            </Button> 
            <Button variant="outlined" onClick={this.handleOrderStatus} value="modifying" color="secondary"  className={classes.button}>
              Need Modifying
            </Button>
            <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>
              <Button variant="outlined"  onClick={this.handleOrderStatus} value="error" color="secondary"  className={classes.button}>
                Reject Order
              </Button>
            </PermissibleRender>
          </Grid>:
        currentStatus === 'modifying' ?
          <Grid>
            <Button variant="outlined" onClick={this.handleOrderMoveToWaiting} value="waiting" color="secondary"  className={classes.button}>
              Move to Waiting
            </Button> 
            <Button variant="outlined" onClick={this.handleOrderComplete} value="complete" color="secondary"  className={classes.button}>
              Confirm & Complete
            </Button>
            <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>
              <Button variant="outlined"  onClick={this.handleOrderStatus} value="error" color="secondary"  className={classes.button}>
                Reject Order
              </Button>
            </PermissibleRender>
          </Grid>:
        currentStatus === 'error' ? 
          <Grid>
            <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>
              <Button variant="outlined" onClick={this.handleOrderMoveToWaiting} value="waiting" color="secondary"  className={classes.button}>
                Move to Waiting
              </Button> 
              <Button variant="outlined" onClick={this.handleOrderDelete} value="waiting" color="secondary"  className={classes.button}>
                Delete
              </Button> 
            </PermissibleRender>
        </Grid> : ''
    return (
      <Grid className={classes.root} item>
        <Grid container item>
          {this.props&& this.props.fromPackage?
             null
            :
              <Button variant="outlined"  component={Link} to="/newOrder"  color="secondary" className={classes.button}>
                New Order
              </Button>
          }

          <Button variant="outlined" component={Link} to="/newPackage" color="secondary" className={classes.button}>
            New Package
          </Button>

          {adminOperations }
        </Grid>
        <Popover
          id="simple-popper"
          open={open}
          anchorEl={anchorEl}
          onClose={this.handleAssignClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          <List>
            <ListItem>
              <Grid item className={classes.search}>
                <TextField 
                  id='search'
                  placeholder="Search Orders" 
                  className={classes.textField} 
                  onChange={this.handleInputChange}
                  value={this.state.search}
                  />
              </Grid>
            </ListItem>
            {/* {console.log(assignToRole)} */}
            {filtedUsers.map(user => 
                (''+user.role).split('|').indexOf(assignToRole) >=0 ?
                  <ListItem button key={user.iduser}>
                    <Checkbox
                      checked={this.state.checkedUser.indexOf(user) !== -1}
                      tabIndex={-1}
                      disableRipple
                      onClick={() => this.handleCheckUser(user)}
                    />
                    <ListItemText primary={`${user.name} - ${user.role}`} secondary={user.labels ? user.labels.replace('|', ',') : ''} />
                  </ListItem>
                : ''
              )}
              <Divider />
              <ListItem>
                <Grid>
                <TextField
                  id="currency_cost"
                  required
                  select
                  label = "Currency"
                  value={this.state.currency_cost||""}
                  onChange={this.handleInputChange}
                  error ={this.state.currency_costValidate ? true : false  }
                  helperText={this.state.currency_costValidate}
                  className={classes.textField}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  SelectProps={{
                    native: true,
                    MenuProps: {
                      className: classes.menu,
                    },
                  }}
                >
                  <option value="" disabled>
                    Select Currency
                  </option>
                  {helperStore.currencyArray.map(option => (
                    <option key={option.idcurrency} value={option.idcurrency}>
                      {option.currency}
                    </option>
                  ))}
                </TextField>   
                <TextField
                  id="cost"
                  required
                  onChange={this.handleInputChange}
                  value={this.state.cost}
                  label="Price to Writer"
                  error ={this.state.costValidate ? true : false  }
                  helperText={this.state.costValidate}
                  className={classes.textField}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />  
              </Grid>
            </ListItem>
            <Divider />
          </List>
            
            <Button variant="outlined" onClick={() => this.handleAssignUser(assignToRole)} className={classes.button}>
              Assign
            </Button>
        </Popover>
      </Grid>
    );
  }
}
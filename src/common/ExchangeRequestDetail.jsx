import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Popover from '@material-ui/core/Popover';
import moment from 'moment'
import ExchangeRequestNew from './ExchangeRequestNew'
import {PermissibleRender} from '@brainhubeu/react-permissible';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
  button: {
    margin: theme.spacing.unit,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit * 2,
    // width: theme.breakpoints.values.sm,
  },
});
  
@withStyles(styles)
@inject('exchangeRequestStore', 'exchangeReserveStore', 'helperStore', 'authStore')
@withRouter
@observer

export default class ExchangeRequestDetail extends Component {
  
  state = {
    request: this.props.request||{"currency_idcurrency":"", "amount":"",  "accountInfo":"", "dateTime":"", "deadLine":"", "returnImg":"", "status":""},
    mode: 'view',
    anchorEl: null,
    currency: 1,
    amount: 0,
  };

  handleOpen = () =>  {
    let request = Object.assign({}, this.state.request)
    request['status'] = 'opening'
    this.props.exchangeRequestStore.updateExchangeRequest(request)
  }

  handleHold = () =>  {
    let request = Object.assign({}, this.state.request)
    request['status'] = 'returning'
    this.props.exchangeRequestStore.updateExchangeRequest(request)
  }

  handleReserve = () =>  {
    let request = Object.assign({}, this.state.request)
    request['status'] = 'reserved'
    this.props.exchangeRequestStore.updateExchangeRequest(request)

    let reserve = {}
    reserve['idexchangeRequest'] = request.idexchangeRequest
    reserve['creatIduser'] = this.props.authStore.currentUser.iduser
    reserve['dateTime'] = moment.tz().format('YYYY-MM-DDTHH:mm:ss')
    this.props.exchangeReserveStore.updateExchangeReserve(reserve)
  }

  handleComplete = () =>  {
    let request = Object.assign({}, this.props.request)
    request['status'] = 'completed'
    this.props.exchangeRequestStore.updateExchangeRequest(request)
  }

  handleViewChange = () => {
    this.setState({mode:'view'})
  }

  handleInputChange = (event) => {
    if(event.target.value.trim().length===0){
      this.setState({
        [event.target.id+'Validate']: event.target.id + ' is required',
        [event.target.id]: event.target.value
      })
    }else{
      this.setState({
        [event.target.id+'Validate']: '',
        [event.target.id]: event.target.value
      })
    }
  }

  handleFilesUpload = (event) => {
    const files = Array.from(event.target.files)
    const formData = new FormData()
    files.forEach((file) => {
      formData.append('file', file)
    })
    this.props.helperStore.uploadFiles(formData).then(res =>{
      let orginalReserve = this.state.reserve
      orginalReserve['paymentImg'] = res
      this.setState({reserve: orginalReserve})
    })
  }

  generateFileLink(filesObj) {
    let files = []
    let filesArray = filesObj
    if (typeof(filesObj) === 'string'){
      filesArray = JSON.parse(filesObj)
    }
    filesArray = filesArray||[]
    for(let i=0; i<filesArray.length; i++){
      let file = filesArray[i]
      const size = file.size/1024 >= 1024? Math.round(file.size/(1024*1024)*100)/100 + ' MB': Math.round(file.size/1024*100)/100+' KB'
      files.push(<li key={i}><a href={this.props.helperStore.ROOT_API_URL + '/img/' + file.filename} key={i}>{file.originalname} - <span className={this.props.classes.span}>{size}</span></a></li>)
    }
    return files
  }

  handleConfirmPop = event => {
    this.setState({
      anchorEl: event.currentTarget,
    });
  }

  handleConfirmClose = () => {
    this.setState({
      anchorEl: null,
    });
  };

  handleConfirm = () => {
    let request = Object.assign({}, this.state.request)
    request['status'] = 'confirming'
    if(typeof(request.returnImg)=='object') 
      request.returnImg = JSON.stringify(request.returnImg)

    // let reserve = Object.assign({}, this.state.reserve)
    // reserve['currency'] = this.state.currency
    // reserve['amount'] = this.state.amount
    
    // if(typeof(reserve.paymentImg)=='object') 
    //   reserve.paymentImg = JSON.stringify(reserve.paymentImg)

    this.props.exchangeRequestStore.updateExchangeRequest(request)
    this.handleConfirmClose()
  }

  render() {
    const { classes, helperStore, authStore, currentStatus } = this.props;
    const { request, mode, anchorEl } = this.state;
    const open = Boolean(anchorEl)

      if(mode=='edit') return <ExchangeRequestNew request={request} cancelFunction={this.handleViewChange} />

      let adminOperations = 
        currentStatus === 'opening' ?
          <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['saleman']} oneperm>
            <Button  color="secondary" className={classes.button} value='request' onClick={this.handleReserve}>Reserve</Button>
          </PermissibleRender>:
        currentStatus === 'confirming' ? 
          <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>
            <Button  color="secondary" className={classes.button} value='request' onClick={this.handleOpen}>Open</Button>
            <Button  color="secondary" className={classes.button} value='request' onClick={this.handleHold}>Hold</Button>
            <Button  color="secondary" className={classes.button} value='request' onClick={this.handleComplete}>Complete</Button>
          </PermissibleRender>:
        currentStatus === 'returning' ?
          <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>
            <Button  color="secondary" className={classes.button} value='request' onClick={this.handleConfirmPop}>Confirm</Button>
          </PermissibleRender>:''

      return (
        <TableRow key={request.idexchangeRequest}>
          <TableCell>{request.idexchangeRequest}</TableCell>
          <TableCell>{request.currency_idcurrency ? helperStore.getCurrency(request.currency_idcurrency).currency:'' }{request.amount?request.amount:''}</TableCell>
          <TableCell>{request.accountInfo}</TableCell>
          <TableCell>{request.dateTime}</TableCell>
          <TableCell>{request.deadLine}</TableCell>
          <TableCell>{this.generateFileLink(request.returnImg)}</TableCell>
          <TableCell>{request.status}</TableCell>
          <TableCell>{adminOperations}</TableCell>
          <Popover
            id="simple-popper"
            open={open}
            anchorEl={anchorEl}
            onClose={this.handleConfirmClose}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
          >
            <TextField
              id="currency" required select
              value={this.state.currency} 
              label= "Currency" 
              onChange={this.handleInputChange}
              error={this.state.currencyValidate ? false : true }
              helperText={this.state.currencyValidate}
              className={classes.textField}
              InputLabelProps={{
                  shrink: true,
              }}
              SelectProps={{
              native: true,
              MenuProps: {
                  className: classes.menu,
              }}}
            >
            <option value="" disabled selected>
              Select Currency
            </option>
            {helperStore.currencyArray.map(option => (
            <option key={option.idcurrency} value={option.idcurrency}>
              {option.currency}
            </option>
            ))}
            </TextField>   
            <TextField
              id="amount" required
              value={this.state.amount} 
              label="Amount" 
              onChange={this.handleInputChange}
              error ={this.state.amountValidate ? false : true }
              helperText={this.state.amountValidate}
              className={classes.textField}
              InputLabelProps={{
                  shrink: true,
              }}
            />
            <TextField
              id="fileUrl"
              label="Support Documents - Select Multipal supported files"
              type="file"
              onChange = {this.handleFilesUpload}
              fullWidth
              inputProps={{ multiple: true }}
              className={classes.textField}
              InputLabelProps={{
                shrink: true,
              }}
            />
            <Grid xs={12} className={classes.fileList}>
              {this.generateFileLink(request.returnImg)}
            </Grid>
            <Button variant="contained" onClick={this.handleConfirm} className={classes.button}>
              Confirm
            </Button>
          </Popover>
        </TableRow>
      )
  }
  
}

import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';

import blue from '@material-ui/core/colors/blue';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Popover from '@material-ui/core/Popover';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import {PermissibleRender} from '@brainhubeu/react-permissible';
import {success, warning, info} from '../common/Colors'

const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit * 2,
    // width: theme.breakpoints.values.sm,
  },
  tabsRoot: {
    width: '100%',
    borderBottom: '1px solid #e8e8e8',
  },
  paddingTop: {
    paddingTop: theme.spacing.unit*2,
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  listitemBackColor: {
    backgroundColor: blue[100],
  },
  title: {
    padding: '5px',
    marginRight: '10px',
    fontSize: '16px',
  },
  label: {
    padding: '4px',
    paddingLeft: '8px',
    paddingRight: '8px',
    marginRight: '8px',
    marginTop: '5px',
    borderRadius: 6,
    fontSize: '12px',
    textDecoration: 'None',
  },
  label2: {
    padding: '10px 8px',
    marginRight: '8px',
    marginTop: '5px',
    borderRadius: 6,
    fontSize: '12px',
    textDecoration: 'None',
  },
  colorInfo: {
    backgroundColor: info,
  },
  colorSuccess: {
    backgroundColor: success,
  },
  colorWarning: {
    backgroundColor: warning,
  },
});

@withStyles(styles)
@inject('priceRequestStore', 'helperStore', 'userStore', 'authStore')
@withRouter
@observer
export default class PriceQuoteDetail extends Component {
  state = {
    // quote: this.props.quote||{"user_iduser":"", "priceRequest_idpriceRequest":"", "status":""},
    // counsellor: this.props.counsellor,
    anchorEll: null,
    currency: 1,
    mode: 'view',
    finalPrice: 0,
    finalCurrency: 1,
    expanded: false,
  };
  handleChange = (event, currentStatus) => {
    this.setState({ currentStatus: currentStatus });
  };

  handleExpandClick(idpriceRequest) {
    let updatingExpanded = this.state.expanded
    updatingExpanded[idpriceRequest] = !updatingExpanded[idpriceRequest]
    this.setState(state => ({ expanded: updatingExpanded }));
  };

  handleCurrencyChange = (event) => {
    this.setState({
      finalCurrency: event.target.value,
    });
  };

  handleInputChange = (event) => {
    if(event.target.value.trim().length===0){
      this.setState({
        [event.target.id+'Validate']: event.target.id + ' is required',
        [event.target.id]: event.target.value
      })
    }else{
      this.setState({
        [event.target.id+'Validate']: '',
        [event.target.id]: event.target.value
      })
    }
  }

  handleFinalQuoteChange = (event) => {
    const {id, value} = event.target
    this.setState({[id]: value})
  }

  handelShowRequestDetail = (event) => {
    this.setState({expanded: !this.state.expanded})
  }

  handleFilesUpload = (event) => {
    const files = Array.from(event.target.files)
    const formData = new FormData()
    files.forEach((file) => {
      formData.append('file', file)
    })
    this.props.helperStore.uploadFiles(formData).then(res =>{
      let orginalOrder = this.state.request
      orginalOrder['fileUrl'] = res
      this.setState({request: orginalOrder})
    })
  }

  generateFileLink(filesObj){
    let files = []
    let filesArray = filesObj
    if (typeof(filesObj) === 'string'){
      filesArray = JSON.parse(filesObj)
    }
    filesArray = filesArray||[]
    for(let i=0; i<filesArray.length; i++){
      let file = filesArray[i]
      const size = file.size/1024 >= 1024? Math.round(file.size/(1024*1024)*100)/100 + ' MB': Math.round(file.size/1024*100)/100+' KB'
      files.push(<li key={i}><a href={this.props.helperStore.ROOT_API_URL + '/img/' + file.filename} key={i}>{file.originalname} - <span className={this.props.classes.span}>{size}</span></a></li>)
    }
    return files
  }

  handleApplyConfirmPop = event => {
    console.log(event.currentTarget, event.currentTarget.value)
    this.setState({
      anchorEll: event.currentTarget,
    });
  }

  handleApplyClose = () => {
    this.setState({
      anchorEll: null,
    });
  };

  handleApplyConfirm = () => {
    if(this.state.anchorEll.value==='request') this.handleRequestConfirm()
    else this.handleQuoteConfirm()
  }
  handleRequestConfirm () {
    let quote = Object.assign({}, this.props.quote)
    quote['status'] = 'confirm'
    let request = Object.assign({}, this.props.request)
    request['currency'] = this.state.finalCurrency
    request['finallPrice'] = this.state.finalPrice
    request['status'] = 'confirm'
    this.props.priceRequestStore.updatePriceQuote(quote)
    this.props.priceRequestStore.updatePriceRequest(request)
    this.handleApplyClose()
  }

  handleQuoteConfirm () {
    let quote = Object.assign({}, this.props.quote)
    quote['status'] = 'confirming'
    quote['currency'] = this.state.finalCurrency
    quote['price'] = this.state.finalPrice
    this.props.priceRequestStore.updatePriceQuote(quote)
    this.handleApplyClose()
  }
  generateLabels(labelsObj){
    let labels = []
    let labelArray = labelsObj
    if (typeof(labelsObj) === 'string'){
      labelArray = JSON.parse(labelsObj)
    }
    labelArray = labelArray || []
    for(let i=0; i<labelArray.length; i++){
      let labelobj = labelArray[i]
      labels.push(<span className={this.props.classes.label} key={i} >{labelobj}</span>)
    }
    return labels
  }

render(){
  let { classes, helperStore,  index, request, authStore , quote, counsellor, priceRequestStore} = this.props;
  let { anchorEll} = this.state
  const openn = Boolean(anchorEll)
  // console.log(quote.priceRequest_idpriceRequest, request)
  let finnalCurrency =  request.currency ? helperStore.getCurrency(request.currency).currency+':' : ''
  let finnalPrice =  request.finallPrice ? request.finallPrice : '' 

  return(
    <React.Fragment>
    <TableRow key={counsellor.id} className={[ quote.status==='confirm' ? classes.colorSuccess : '' ].join(' ')}>
      <TableCell component="th" scope="row">
        {index+1}.
      </TableCell>
      <TableCell>{counsellor.name}</TableCell>
      <TableCell>{this.generateLabels(counsellor.labels.split('|'))}</TableCell>
      <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'counsellor']} oneperm>
        <TableCell> {quote.currency ? helperStore.getCurrency(quote.currency).currency+':' : '' }{ quote.price?quote.price:'' }</TableCell>
      </PermissibleRender>
      <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'saleman']} oneperm>
        <TableCell> {quote.status === 'confirm' ? finnalCurrency + finnalPrice: ''}</TableCell>
      </PermissibleRender>
      
      <TableCell>{quote.status}</TableCell>
      <TableCell>
        <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>
          <Button  color="secondary" className={classes.button} value='quote' onClick={this.handleApplyConfirmPop}>Offer</Button>
        </PermissibleRender>
        <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['counsellor']} oneperm>
          <Button  color="secondary" className={classes.button} value='quote' onClick={this.handelShowRequestDetail}>Detail</Button>
        </PermissibleRender>
        <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['counsellor']} oneperm>
          <Button  color="secondary" className={classes.button} value='quote' disabled={quote.status === 'requesting' ? false : true} onClick={this.handleApplyConfirmPop}>Offer</Button>
        </PermissibleRender>
        <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>
          <Button  color="secondary" className={classes.button} value='request' onClick={this.handleApplyConfirmPop}>Confirm</Button>
        </PermissibleRender>
      </TableCell>
      <Popover
        id="simple-popper"
        open={openn}
        anchorEl={anchorEll}
        onClose={this.handleApplyClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <TextField
          id="finalCurrency" required select
          value={this.state.finalCurrency} 
          label= "Currency" 
          onChange={this.handleInputChange}
          error={this.state.finalCurrencyValidate ? false : true }
          helperText={this.state.finalCurrencyValidate}
          className={classes.textField}
          InputLabelProps={{
              shrink: true,
          }}
          SelectProps={{
          native: true,
          MenuProps: {
              className: classes.menu,
          }}}
        >
        <option value="" disabled selected>
          Select Currency
        </option>
        {helperStore.currencyArray.map(option => (
        <option key={option.idcurrency} value={option.idcurrency}>
          {option.currency}
        </option>
        ))}
        </TextField>   
        <TextField
          id="finalPrice" required
          value={this.state.finalPrice} 
          label="Price" 
          onChange={this.handleInputChange}
          error ={this.state.finalPriceValidate ? false : true }
          helperText={this.state.finalPriceValidate}
          className={classes.textField}
          InputLabelProps={{
              shrink: true,
          }}
        />
        <Button variant="contained" onClick={this.handleApplyConfirm} className={classes.button}>
          Confirm
        </Button>
      </Popover>
    </TableRow>
    {
      this.state.expanded ?
      <TableRow>
        <TableCell colSpan={6}>
            {request.description}
            {this.generateFileLink(request.fileUrl)}
        </TableCell>
      </TableRow>
      : ''
    }
    </React.Fragment>
    );
  }
} 


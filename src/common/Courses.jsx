import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter, Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search'
import blue from '@material-ui/core/colors/blue';
import TablePagination from "@material-ui/core/TablePagination";

import CourseDetail from './CourseDetail'
import loading from '../util/loading'


const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  padding: {
    paddingBottom: theme.spacing.unit*2,
    paddingTop: theme.spacing.unit*1,
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  listitemBackColor: {
    backgroundColor: blue[100],
  },
  textField: {
    marginLeft: 8,
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  search:{
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
  }
});

@withStyles(styles)
@inject('ordersStore', 'courseStore', 'helperStore')
@withRouter
@observer
export default class Courses extends Component{
  state = {
    search: '',
    page: 0, 
    rowsPerPage: 10,
  }
  handleInputChange = event => {
    const {id, value} = event.target
    this.setState({[id]: value})
  }
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };
  render(){
    const { classes, courseStore, ordersStore } = this.props;
    const {  page, rowsPerPage } = this.state
    const courses = courseStore.Courses.filter(ele => ele.status==='valid')

    courseStore.isLoading || loading

    let filtered = []
    if(this.state.search){
      for(let ele of courses){
        if(JSON.stringify(ele).toLowerCase().indexOf(this.state.search.toLowerCase())>=0){
          filtered.push(ele)
        }
      }
    }else{
      filtered = courses
    }


    return(
      <Grid  container className={classes.root}>
        <Grid container justify={'space-between'} >
          <Grid item className={classes.search}>
            <TextField 
              id="search"
              placeholder="Search Course ..." 
              className={classes.textField} 
              onChange={this.handleInputChange}
              value={this.state.search}
            />
            <IconButton className={classes.iconButton} aria-label="Search">
              <SearchIcon />
            </IconButton>
          </Grid>
          <Grid>
            <Button variant="contained"  component={Link} to="/newCourse"  color="secondary" className={classes.button}>
              New Course
            </Button>
          </Grid>
        </Grid>
        <Grid item xs={12} className={classes.padding}><Divider/></Grid>
        <Grid container item spacing={16} xs={12}>
          {filtered.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row, index) => {
              return <CourseDetail course={row} key={index} />
          })}
        </Grid>
        <TablePagination
            rowsPerPageOptions={[10, 20, 50]}
            component="div"
            count={filtered.length}
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              'aria-label': 'Previous Page',
            }}
            nextIconButtonProps={{
              'aria-label': 'Next Page',
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
      </Grid>
    );
  }
} 

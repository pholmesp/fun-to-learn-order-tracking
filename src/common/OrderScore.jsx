import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter, Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button'
import {PermissibleRender} from '@brainhubeu/react-permissible';
import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile';

import GridListTileBar from '@material-ui/core/GridListTileBar';
import SettingsOverscan from '@material-ui/icons/SettingsOverscan';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import { Spin, Descriptions, List } from 'antd';
import {info, gray, bginfo} from './Colors'
import {GenerateFileLinkView} from '../util/generateFileLink'

import EditOrderScore from './EditOrderScore';
import 'antd/dist/antd.css';
const styles = theme => ({
  marginTop: {
    marginTop: theme.spacing.unit,
  },
  marginBottom: {
    marginBottom: theme.spacing.unit,
  },
  info:{
    backgroundColor: info,
  },
  bginfo:{
    backgroundColor: bginfo,
    padding: theme.spacing.unit, 
    borderRadius: '3px'
  },
  span: {
    fontSize: '14px',
    color: gray,
    paddingLeft: '5px'
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
  list:{
    color: gray,
    padding: '3px',
    fontSize: '14px',
    textDecoration: 'underline',
  },
});

@withStyles(styles)
@inject('ordersStore', 'authStore', 'userStore', 'helperStore', 'orderResultStore')
@withRouter
@observer
export default class OrderScore extends Component{
  state = {
      setScore: false,
      uploadingFile: false,
      editResult: false,
      fileDisplayOpen: false,
      activeFile: {},
  }

  handlePaymentImgDislpay(file){
    this.setState({
      activeFile: file,
      fileDisplayOpen: true,
    })
  }
  handlePaymentImgDisplayCLose = () => {
    this.setState({
      activeFile: {},
      fileDisplayOpen: false,
    })
  }

  handleSetScore = () => {
    this.setState({
      setScore: true, 
    })
  }
  handleCancleSetScore = () => {
    this.setState({
      setScore: false,
    })
  }

  render(){
    let { classes, authStore, orderResultStore} = this.props;
    let orderResult = orderResultStore.orderScore.get(this.props.idorder)||{}
    let tempScoreShot = orderResult.scoreShot||false
    let scoreShot = false
    if(typeof(tempScoreShot) === 'string') scoreShot = JSON.parse(tempScoreShot)[0]

    return(
      <React.Fragment>
        <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'saleman']} oneperm>      
          {
            // orderResult.score||orderResult.scorePercent ?
            this.state.setScore ? 
              <EditOrderScore orderResult={orderResult} cancel={this.handleCancleSetScore} />
            :
            orderResult.score || orderResult.scorePercent ? 
            <Grid container item className={[classes.bginfo, classes.padding, classes.marginBottom].join(' ')} justify="space-between" xs={12}>
              <Grid item xs={2}>
                <Typography  variant="body1"  gutterBottom> Score: {orderResult.score}</Typography>
                <Typography  variant="body1"  gutterBottom> Score (Percent): {orderResult.scorePercent}</Typography>
              </Grid>
              <Grid item xs={3} >
                {console.log(scoreShot)}
                {
                  scoreShot ? 
                  <GridList cellHeight={40}>
                    <GridListTile>
                      <img src={this.props.helperStore.ROOT_API_URL + '/img/' + scoreShot.filename} alt={scoreShot.filename} />
                      <GridListTileBar
                          actionIcon={
                            <IconButton className={this.props.classes.icon} onClick={() => this.handlePaymentImgDislpay(scoreShot)}>
                              <SettingsOverscan />
                            </IconButton>
                          }
                        />
                    </GridListTile>
                  </GridList>
                  : ''
                }
              </Grid>
              <Grid item xs={1}>
                <Button  size="small" className={classes.button} color="secondary" onClick={this.handleSetScore}>
                  Edit Score
                </Button>
              </Grid>
            </Grid> 
            :
            <Grid className={[classes.bginfo, classes.padding, classes.marginBottom].join(' ')} cs={12}>
              <Button  size="small" className={classes.button} color="secondary" onClick={this.handleSetScore}>
                Upload Score
              </Button>
            </Grid>
            
          }
          <Dialog
            fullWidth={true}
            maxWidth={'md'}
            open={this.state.fileDisplayOpen}
            onClose={this.handlePaymentImgDisplayCLose}
          >
            <DialogContent>
              <img className={classes.image} src={this.props.helperStore.ROOT_API_URL + '/img/' +this.state.activeFile.filename} alt={this.state.activeFile.filename} />
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handlePaymentImgDisplayCLose} color="primary">
                Close
              </Button>
            </DialogActions>
          </Dialog>
        </PermissibleRender>
      </React.Fragment>
    );
  }
} 

import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import FolderOpen from '@material-ui/icons/FolderOpen';
import ArrowBack from '@material-ui/icons/ArrowBack';
import moment from 'moment'



const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit * 2,
    // width: theme.breakpoints.values.sm,
  },
  tabsRoot: {
    width: '100%',
    brequestBottom: '1px solid #e8e8e8',
  },
  paddingTop: {
    paddingTop: theme.spacing.unit*2,
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  label: {
    padding: '4px',
    paddingLeft: '8px',
    paddingRight: '8px',
    marginRight: '8px',
    marginTop: '5px',
    brequestRadius: 6,
    fontSize: '12px',
    textDecoration: 'None',
  },
});

@withStyles(styles)
@inject('exchangeRequestStore', 'helperStore')
@withRouter
@observer
export default class ExchangeRequestNew extends Component {
  state = {
    request: this.props.request||{"currency_idcurrency":"", "amount":"",  "accountInfo":"", "dateTime":"", "deadLine":"", "returnImg":"", "status":""},
    requestValidate:  {"currency_idcurrency":"",  "amount":"",  "accountInfo":"", "deadLine":""},
    mode: 'view',
  };

  handleInputChange = (event) => {
    console.log(event.target)
    let updatingrequestValidation = this.state.requestValidate
    if(event.target.value.trim().length===0){
      updatingrequestValidation[event.target.id] = event.target.id + ' is required'
    }else{
      updatingrequestValidation[event.target.id] = "" 
    }
    let updatingrequest = this.state.request
    updatingrequest[event.target.id] = event.target.value
    this.setState({
      request: updatingrequest,
      requestValidate: updatingrequestValidation
    })
  }

  handleFilesUpload = (event) => {
    const files = Array.from(event.target.files)
    const formData = new FormData()
    files.forEach((file) => {
      formData.append('file', file)
    })
    this.props.helperStore.uploadFiles(formData).then(res =>{
      let orginalrequest = this.state.request
      orginalrequest['returnImg'] = res
      this.setState({request: orginalrequest})
    })
  }

  validateField(){
    let updatingrequestValidate = this.state.requestValidate
    let validateStatus = false
    for(let key in updatingrequestValidate){
      console.log(this.state.request[key], key)
      if((''+this.state.request[key]).trim().length===0){
        updatingrequestValidate[key] = key + ' is required'
        validateStatus = true
      }
    }
    this.setState({
      requestValidate: updatingrequestValidate
    })
    return validateStatus
  }
  utcDatetime(dt, tz){
    return moment.tz(dt,tz).format()
  }

  handleCreateExchangeRequest = () => {
    if(!this.validateField()){
      let request = Object.assign({},this.state.request)
      if(typeof(request.returnImg)==='object') request.returnImg = JSON.stringify(request.returnImg)
      if(!request.idexchangeRequest) request.status='opening'  //opening -> reserved -> confirming -> completed, returning
      if(!request.idexchangeRequest) request.dateTime=moment.tz().format('YYYY-MM-DDTHH:mm:ss')
      request.deadLine = this.utcDatetime(request.deadLine, this.state.timeZone)
      this.props.exchangeRequestStore.updateExchangeRequest(request).then(res =>{
        if(res.statusCode === 200){
          if(this.props.cancelFunction) this.props.cancelFunction()
          if(this.props.goBack) this.props.goBack()
          this.props.history.push('/')
        }else{
          this.setState({
            error: JSON.stringify(res)
          })
        }
      })
    }
  }

  generateFileLink(filesObj){
    let files = []
    let filesArray = filesObj
    console.log(filesObj)
    if (typeof(filesObj) === 'string'){
        filesArray = filesObj.length===0? [] : JSON.parse(filesObj)
    }
    filesArray = filesArray||[]
    for(let i=0; i<filesArray.length; i++){
      let file = filesArray[i]
      const size = file.size/1024 >= 1024? Math.round(file.size/(1024*1024)*100)/100 + ' MB': Math.round(file.size/1024*100)/100+' KB'
      files.push(<li key={i}><a href={this.props.helperStore.ROOT_API_URL + '/img/' + file.filename} key={i}>{file.originalname} - <span className={this.props.classes.span}>{size}</span></a></li>)
    }
    return files
  }

  render(){
    const { classes, helperStore, history } = this.props;
    const { request } = this.state;

    return(
      <div className={classes.root}>
        <h3>
          <IconButton>
            <ArrowBack onClick={() => this.props.cancelFunction ? this.props.cancelFunction() : history.goBack()} />
          </IconButton>
          &nbsp;
          <FolderOpen/> { this.props.formName || "Create a New Exchange Request"}
        </h3>
        <Grid className={classes.bgerror}>{this.state.error}</Grid>
        <form className={classes.container}>
          <TextField
            id="currency_idcurrency" required select
            label= "Currency" 
            value={request.currency_idcurrency} 
            onChange={this.handleInputChange}
            error={this.state.requestValidate.currency_idcurrency.length === 0 ? false : true }
            helperText={this.state.requestValidate.currency_idcurrency}
            className={classes.textField}
            InputLabelProps={{
                shrink: true,
            }}
            SelectProps={{
            native: true,
            MenuProps: {
                className: classes.menu,
            }}}
          >
            <option value="" disabled selected>
              Select Currency
            </option>
            {helperStore.currencyArray.map(option => (
              <option key={option.idcurrency} value={option.idcurrency}>
                {option.currency}
              </option>
            ))}
          </TextField>   
          <TextField
            id="amount" required
            label="Amount" 
            value={request.amount} 
            onChange={this.handleInputChange}
            error ={this.state.requestValidate.amount.length === 0 ? false : true }
            helperText={this.state.requestValidate.amount}
            className={classes.textField}
            InputLabelProps={{
                shrink: true,
            }}
          />
          <TextField
            id="deadLine" required
            // value={order.realDeadline}
            onChange={this.handleInputChange}
            label="Deadline"
            type="datetime-local"
            defaultValue={this.state.request.deadLine || moment().add(14, 'days').format("YYYY-MM-DD") + "T23:59:59"}
            error ={this.state.requestValidate.deadLine.length === 0 ? false : true  }
            helperText={this.state.requestValidate.deadLine}
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            id="accountInfo" required fullWidth
            label="Account Info"
            value={request.accountInfo}
            onChange={this.handleInputChange}
            error ={this.state.requestValidate.accountInfo.length === 0 ? false : true }
            helperText={this.state.requestValidate.accountInfo}
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            id="returnImg" fullWidth
            label="Support Documents - Select Multipal supported files"
            type="file"
            onChange = {this.handleFilesUpload}
            inputProps={{ multiple: true }}
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <Grid xs={12} className={classes.fileList}>
            {this.generateFileLink(request.returnImg)}
          </Grid>
          <Button  variant="contained" onClick={() => this.handleViewChange('view')} className={classes.button} color="primary">Cancel</Button>
          <Button  variant="contained" onClick={this.handleCreateExchangeRequest} className={classes.button} color="secondary">{this.props.formName ||"Confirm"}</Button>
      </form>
    </div>
    );
  }
} 


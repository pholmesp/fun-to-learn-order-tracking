import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import { Spin } from 'antd';
import {info, bginfo} from './Colors'
import orderResultStore from '../stores/orderResultStore';

const styles = theme => ({
  marginTop: {
    marginTop: theme.spacing.unit,
  },
  marginBottom: {
    marginBottom: theme.spacing.unit,
  },
  info:{
    backgroundColor: info,
  },
  bginfo:{
    backgroundColor: bginfo,
    padding: theme.spacing.unit, 
    borderRadius: '3px'
  },
  fullWidth: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit,
    border: 2,
    width: '100%',
  }, 
});

@withStyles(styles)
@inject('ordersStore', 'helperStore', 'authStore', 'orderResultStore')
@withRouter
@observer
export default class EditOrderScore extends Component{
  constructor(props){
    super(props)
    this.state = {
      orderResult: this.props.orderResult,  // the result of order detail, which is added by counsellor,xz
      uploadingFile: false,
      isLoading: false
    };
  }

  handleInputChange = (event) => {
    let orginalOrder = this.state.orderResult
    orginalOrder[event.target.id] = event.target.value
    this.setState({orderResult: orginalOrder})
  }

  handleFilesUpload = (event) => {
    const files = Array.from(event.target.files)
    const formData = new FormData()
    files.forEach((file) => {
      formData.append('file', file)
    })
    formData.append('fileclass', 'score screen shot')
    this.setState({uploadingFile: true})
    this.props.helperStore.uploadFiles(formData).then(res =>{
      let orginalOrder = this.state.orderResult
      orginalOrder['scoreShot'] = res
      this.setState({orderResult: orginalOrder, uploadingFile: false})
    })
  }

  handleUpdateScore = () => {
    this.setState({isLoading: true})
    let result = Object.assign({}, this.state.orderResult)
    delete result['fileclass']
    if(typeof(result.scoreShot)=='object') result.scoreShot = JSON.stringify(result.scoreShot)
    this.props.ordersStore.updateUserOrder(result).then(rr => {
      // console.log(rr)
      if(rr.statusCode == 200)  { 
        orderResultStore.getOrderScore(this.props.orderResult.orders_idorder, false)
        this.props.cancel()
      }else{
        this.setState({
          isLoading: false, 
          errorInfo: JSON.stringify(rr)
        })
      }
    })
    
  }


  render(){
    let { classes } = this.props;
    return(
      <React.Fragment>
        <Spin spinning={this.state.uploadingFile || this.state.isLoading}>
          <Grid container className={[classes.bginfo, classes.padding, classes.marginBottom].join(' ')}>
            <Grid container item>
              <Grid item sm={12} lg={2}>
              <TextField
                id="score"
                fullWidth
                select
                label = "Score"
                value= {this.state.orderResult.score||''}
                onChange={this.handleInputChange}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
                SelectProps={{
                  native: true,
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
              >
              <option value="" disabled>
                Select Score
              </option>
                {['HD', 'D', 'Credit', 'Pass', 'Failed'].map(option => (
                  <option key={option} value={option}>
                    {option}
                  </option>
                ))}
              </TextField>  
            </Grid>
            <Grid item sm={12} lg={4}>
            <TextField
              id="scorePercent"
              fullWidth
              label="Score in Percent: HD=80% D=70% Credit=60% Pass=50%, or other as marked in assignment"
              value={this.state.orderResult.scorePercent||''}
              onChange = {this.handleInputChange}
              className={classes.textField}
              InputLabelProps={{
                shrink: true,
              }}
            />
            </Grid>
            <Grid item sm={12} lg={4}>
            <TextField
                id="attachement"
                label="Score Screen Shot"
                type="file"
                fullWidth
                onChange = {this.handleFilesUpload}
                inputProps={{ multiple: false }}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </Grid>
          </Grid>
          <Grid item className={classes.marginTop} >
            <Button variant="outlined"  size="small"  color="secondary" className={classes.button} onClick={this.props.cancel}>
              Cancle
            </Button>
            &nbsp;
            <Button variant="outlined"  size="small"  color="secondary" className={classes.button} onClick={this.handleUpdateScore}>
              Save
            </Button>
          </Grid>
        </Grid>
        </Spin>
      </React.Fragment>
    );
  }
} 

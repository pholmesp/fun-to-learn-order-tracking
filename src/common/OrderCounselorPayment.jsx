import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter, Link } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField';
import Popover from '@material-ui/core/Popover';
import {success, warning, gray} from './Colors';
import {updateOrderStatus, updateOrderCurrencyCost, updateOrderCost, updateCounselorPayment} from '../util/updateOrder'

const styles = theme => ({
  Important: {
    backgroundColor: warning,
    textTransform: 'none',
  },
  Success: {
    backgroundColor: success,
    textTransform: 'none',
  },
  label:{
    padding: '3px',
    paddingLeft: '5px',
    paddingRight: '5px',
    marginRight: '5px',
    marginTop: '4px',
    borderRadius: 3,
    fontSize: '12px',
    color: gray, 
    textDecoration: 'None',
    textTransform: 'none',
  },
  button: {
    marginTop: '10px',
  },
  padding: {
    padding: '10px'
  }
});

@withStyles(styles)
@inject('helperStore')
@withRouter
@observer
export default class OrderCounselorPayment extends Component {
  
  constructor(props) {
    super(props)
    this.state = {
      order: Object.assign({}, this.props.order),
      anchorEl: null,
      orderValidate: {"currency_cost": "", "cost": ""},
    };
  }

  handlePopoverShow = event => {
    this.setState({
      anchorEl: event.currentTarget,
    });
  };

  handlePopoverClose = () => {
    this.setState({
      anchorEl: null,
    });
  };

  handleConfirmCounselorPaid = () =>{
    if (window.confirm('Are you sure to Confirm the counselor paid?')) {
      updateCounselorPayment(this.props.order, 'paid')
      this.handlePopoverClose()
    } 
  }

  handleInputChange = (event) =>{
    console.log(event.target)
    let updatingOrderValidation = this.state.orderValidate
    if(event.target.id in [...Object.keys(updatingOrderValidation)]){
      updatingOrderValidation[event.target.id] = ""
    }
    let updatingOrder = this.state.order
    updatingOrder[event.target.id] = event.target.value
    this.setState({
      order: updatingOrder,
      orderValidate: updatingOrderValidation
    })
  }

  handleSubmit = () => {
    if (window.confirm('Are you sure to Change the counselor cost?')) {
      updateCounselorPayment(this.props.order, 'unpaid')
      updateOrderCurrencyCost(this.state.order)
      updateOrderCost(this.state.order)
      this.handlePopoverClose()
    } 
  }
  
  render() {
    let { helperStore, classes } = this.props;
    let { anchorEl, orderValidate, order } = this.state;

    const open = Boolean(anchorEl);

    return (
      <span>
        <a onClick={this.handlePopoverShow} value="qa" size="small" className={[classes.label, JSON.parse(this.props.order.counselorPayment)=='paid'?classes.Success:classes.Important].join(' ')}>
          Counsellor {this.props.order.currency_cost ? helperStore.getCurrency(this.props.order.currency_cost) ? helperStore.getCurrency(this.props.order.currency_cost).currency + ':' + this.props.order.cost : '':''} {JSON.parse(this.props.order.counselorPayment)=='paid'?'paid':'unpaid'}
        </a>

        <Popover
          id="simple-popper"
          className={classes.padding}
          open={open}
          anchorEl={anchorEl}
          onClose={this.handlePopoverClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          <div className={classes.padding}>
          <div>
            Change Counsellor Price: 
          </div>
          <div>
            <TextField
              id="currency_cost"
              required
              select
              label = "Currency"
              value={order.currency_cost||""}
              onChange={this.handleInputChange}
              error ={orderValidate.currency_cost.length === 0 ? false : true  }
              helperText={orderValidate.currency_cost}
              className={classes.textField}
              InputLabelProps={{
                  shrink: true,
              }}
              SelectProps={{
                native: true,
                MenuProps: {
                  className: classes.menu,
                },
              }}
            >
              <option value="" disabled>
              Select Currency
            </option>
              {helperStore.currencyArray.map(option => (
                <option key={option.idcurrency} value={option.idcurrency}>
                  {option.currency}
                </option>
              ))}
            </TextField>   
            <TextField
              id="cost"
              required
              onChange={this.handleInputChange}
              value={order.cost}
              label="Cost"
              error ={this.state.orderValidate.cost.length === 0 ? false : true  }
              helperText={this.state.orderValidate.cost}
              className={classes.textField}
              InputLabelProps={{
                  shrink: true,
              }}
            />  
            <Button color="secondary" size='small' className={classes.button} onClick={this.handleSubmit}>
              Change Cost
            </Button>
          </div>
          <hr/>
          <div style={{marginLeft: '10px'}}>
            <Button  onClick={this.handleConfirmCounselorPaid} variant="contained" color='secondary' size="small" className={classes.button}>
              Confirm Counselor Paid
            </Button>
          </div>
          </div>
        </Popover>
      </span>
    )
  }
}
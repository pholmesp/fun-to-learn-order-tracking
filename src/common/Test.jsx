
import React from 'react';
import ReactDOM from 'react-dom';
// import 'antd/dist/antd.css';
import { Descriptions, Badge } from 'antd';

export default  class Test extends React.Component{
    render(){
        return(
            <Descriptions title="User Info" layout="vertical" size="small" bordered>
            
            <Descriptions.Item label="Status" >
                <Badge status="processing" text="Running" />
            </Descriptions.Item>
            
            <Descriptions.Item label="Config Info">
                Data disk type: MongoDB
                <br />
                Database version: 3.4
                <br />
                Package: dds.mongo.mid
                <br />
                Storage space: 10 GB
                <br />
                Replication_factor:3
                <br />
                Region: East China 1<br />
            </Descriptions.Item>
            </Descriptions>
        )
    }
}
  
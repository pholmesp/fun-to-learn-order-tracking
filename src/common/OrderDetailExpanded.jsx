import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter, Link } from 'react-router-dom';
import moment from 'moment'
import momenttz from 'moment-timezone'

import { withStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import Checkbox from '@material-ui/core/Checkbox';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText'
import Divider from '@material-ui/core/Divider';
import CircularProgress from '@material-ui/core/CircularProgress';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Switch from '@material-ui/core/Switch';
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Button from '@material-ui/core/Button'

import TextField from '@material-ui/core/TextField'
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import ListSubheader from '@material-ui/core/ListSubheader';
import GridListTileBar from '@material-ui/core/GridListTileBar';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Popover from '@material-ui/core/Popover';

import Edit from '@material-ui/icons/Edit';
import SettingsOverscan from '@material-ui/icons/SettingsOverscan';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import {PermissibleRender} from '@brainhubeu/react-permissible';

import { Spin } from 'antd';

import {success, info, warning, error, gray, bginfo, bgwarning, bgerror, bgsuccess, lightBlue, bgLightBlue} from './Colors'
import NewOrder from './NewOrder'
import Comments from './Comments'
import OrderResult from './OrderResult'
import {AutoCompleteUser} from './AutoComplete'
import addOrderComment from '../util/addOrderComment'
import {GenerateFileLinkView, GenerateFileLinkEdit} from '../util/generateFileLink'
import {GenerateLabelView} from '../util/generateLabel'
import downloadAllFiles from '../util/downloadAllFiles'
import {updateOrderStatus, confirmOrderPayment} from '../util/updateOrder'
import {notifyCustomerInEmail} from '../util/notifyInEmail'
import EditOrderResult from './EditOrderResult';


const styles = theme => ({
  root: {
    // padding: theme.spacing.unit * 2,
    textAlign: 'left',
    borderRadius: '3px',
    width: '100%',
    paddingTop: theme.spacing.unit ,
    marginTop: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 2,
    paddingRight: theme.spacing.unit * 2,
  },
  paddingTop:{
    paddingTop: theme.spacing.unit * 2,
  },
  paddingLeft:{
    paddingLeft: theme.spacing.unit * 2,
  },
  marginTop: {
    marginTop: theme.spacing.unit,
  },
  marginRight: {
    marginRight: theme.spacing.unit *1,
  },
  marginBottom: {
    marginBottom: theme.spacing.unit,
  },
  Extremely: {
    backgroundColor: error,
  },
  Important: {
    backgroundColor: warning,
  },
  Normal: {
    backgroundColor: info,
  }, 
  Success: {
    backgroundColor: success,
  },
  info:{
    backgroundColor: info,
  },
  bgerror:{
    backgroundColor: bgerror,
  },
  bgwarning:{
    backgroundColor: bgwarning,
  },
  bginfo:{
    backgroundColor: bginfo,
    padding: theme.spacing.unit, 
    borderRadius: '3px'
  },
  bgsuccess: {
    backgroundColor: bgsuccess,
  },
  normal: {
    backgroundColor: bgLightBlue,
  },
  errorText: {
    color: error, 
    fontSize: '14px',
    textDecoration: 'underline',
  },
  label:{
    padding: '3px',
    paddingLeft: '5px',
    paddingRight: '5px',
    marginRight: '5px',
    marginTop: '4px',
    borderRadius: 3,
    fontSize: '12px',
    color: gray, 
    textDecoration: 'None',
  },
  list:{
    color: gray,
    padding: '3px',
    fontSize: '14px',
    textDecoration: 'underline',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
    [theme.breakpoints.up('sm')]: {
      marginRight: -8,
    },
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  img: {
    padding: 5,
    paddingBottom: 8,
    width: theme.breakpoints.values.lg/4,
  },
  image: {
    maxWidth: '100%',
    maxHeight: '100%',
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
  span: {
    fontSize: '14px',
    color: gray,
    paddingLeft: '5px'
  },
  fullWidth: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit,
    border: 2,
    width: '100%',
  }, 
});

@withStyles(styles)
@inject('ordersStore', 'courseStore', 'helperStore', 'userStore', 'authStore', 'customerStore')
@withRouter
@observer
export default class OrderDetail extends Component{
  constructor(props){
    super(props)
    this.state = {
      checked: this.props.ordersStore.checked(this.props.match.params.idorder||this.props.order.idorder),
      value: 0,
      expanded: this.props.match.params.idorder? true : false,
      mode: 'view', 
      showComment: false, 
      editResult: false,
      setStore: false,
      userOrder: {},  // the result of order detail, which is added by counsellor,xz
      activeFile: {},
      fileClass: {},
      fileDisplayOpen: false,
      uploadingFile: false,
      anchorEl: null,
      search: '',
      assignToRole: null
    };
  }
 
  handleCheck = () => {
    if (this.state.checked) {
      this.props.ordersStore.delOrdersChecked(this.props.match.params.idorder||this.props.order.idorder)
    } else {
      this.props.ordersStore.putOrdersChecked(this.props.match.params.idorder||this.props.order.idorder)
    }
    this.setState({
      checked: !this.state.checked,
    });
  };
  handleAssign = event => {
    this.setState({
      anchorEl: event.currentTarget,
      assignToRole: event.currentTarget.value,
    });
  };

  handleAssignClose = () => {
    this.setState({
      anchorEl: null,
      assignToRole: null,
    });
  };

  handleAssignUser(user){
    if(window.confirm("You are assign this order to QA " + user.name)){
      const order = this.props.order
      let submitInvitation = {}
      submitInvitation['orders_idorder'] = order.idorder 
      submitInvitation['user_iduser'] = user.iduser
      submitInvitation['role'] = this.state.assignToRole

      this.props.ordersStore.updateInvitation(submitInvitation)
      .then(res=>
        addOrderComment(order, this.props.authStore.currentUser.iduser, `${this.props.authStore.currentUser.name}(id: ${this.props.authStore.currentUser.iduser}) invited ${user.name} (id: ${user.iduser}) to join ${order.name} (${order.idorder})`)
      )
      this.handleAssignClose() 
    } 
  }
  handleSearchQa = (event) =>{
    this.setState({[id]:value})
  }

  handleExpandClick = () => {
    this.setState(state => ({ expanded: !state.expanded }));
  };
  handleOrderEdit = () => {
    this.setState({
      mode: "edit",
    })
  }
  handleCancleEdit = () => {
    this.setState({
      mode:"view",
    })
  }
  handleShowComment = () => {
    this.setState({showComment: !this.state.showComment})
  }
  handleResultUploadView = () => {
    this.setState({
      editResult: true
    })
  }
  handelCancelResultUploadView = () => {
    this.setState({
      editResult: false
    })
  }

  handleSwitch = order => (event) => {
    console.log(event.target, event.target.value, event.target.checked)
    let newOrder = Object.assign({}, this.props.order)
    let order = this.props.order
    if(order.paymentStatus !== 'full paid') return alert('Order should be full paid to set AUTO')
    if(order.paymentVerify !== 'verified') return alert('Order full payment shoule be verified by account to set AUTO')
    let orderObj = {} 
    let fieldStatus = {}
    orderObj['idorder'] = order.idorder
    fieldStatus['key'] = 'autoProcess'
    fieldStatus['value'] = order.autoProcess===0? 1 : 0
    orderObj['fields'] = [fieldStatus]
    // console.log(orderObj)
    this.props.ordersStore.updateOrder(orderObj).then(res => {
      if(res.statusCode === 200){
        let autoContent = `${this.props.authStore.currentUser.name}(${this.props.authStore.currentUser.iduser}) set this order to AUTO process, which means that this order will be marked as complete once Assistant submit result to this order, and the result will be send to customer directly.`
        let manualContent = `${this.props.authStore.currentUser.name}(${this.props.authStore.currentUser.iduser}) set this order to MANUAL process, order will be processed normally as order -> assistant complete -> QA verifying -> complete.`
        let content  = fieldStatus['value'] === 0 ? manualContent : autoContent
        addOrderComment(order, this.props.authStore.currentUser.iduser, content)
      }
    })
  }

  handleInputChange = (event) => {
    let orginalOrder = this.state.userOrder
    // console.log(event.target.value, event.target.id, event.target.name, event.currentTarget)
    orginalOrder[event.target.id] = event.target.value
    this.setState({userOrder: orginalOrder})
  }

  handleAcceptorder = () => {
    let accpetedAssistant = undefined
    if(this.props.authStore.currentUser.role.indexOf('counsellor')>=0) accpetedAssistant = this.props.ordersStore.getOrderResultById(this.props.order.idorder).filter(ele=>ele.assigned_iduser===this.props.authStore.currentUser.iduser)[0]
    else accpetedAssistant = this.props.ordersStore.getOrderResultById(this.props.order.idorder)[0]

    let result = Object.assign({}, accpetedAssistant)
    result['confirm'] = 1
    this.props.ordersStore.updateUserOrder(result).then(res => {
      console.log(accpetedAssistant, result, res)
      if(res.statusCode == 200)  { 
        let orderObj = {} 
        orderObj['idorder'] = this.props.order.idorder
        let fieldStatus = {}
        let fieldCurrentProcessor = {}
        fieldStatus['key'] = 'status'
        fieldCurrentProcessor['key'] = 'currentProcessorRole'
        fieldStatus['value'] = 'processing'; 
        fieldCurrentProcessor['value'] = 'counsellor'
        orderObj['fields'] = [fieldStatus, fieldCurrentProcessor]
        return this.props.ordersStore.updateOrder(orderObj)
      }else{
        alert("confirm accepting order error, please refresh and try again! ")
      }
    })
  }
  
  handleRejectOrder = () => {
    let order = this.props.order
    if (window.confirm('Are you sure REJECT this order?')) {
      this.props.ordersStore.deleteOrderResultByOrderIdUserId(order.idorder, this.props.authStore.currentUser.iduser).then(() => {
        addOrderComment(order, this.props.authStore.currentUser.iduser, `${this.props.authStore.currentUser.name}(${this.props.authStore.currentUser.iduser}) reject this order assign, the order was moved back to 'waiting'.  `)
        if(this.props.ordersStore.getOrderResultById(this.props.order.idorder).length===0) updateOrderStatus(order, 'waiting')
      })
    } 
  }
  handleConfirmPayment = () =>{
    if (window.confirm('Are you sure to Confirm the payment (Bound, Part or Full Payment)?')) {
      confirmOrderPayment(this.props.order)
    } 
  }
  handleNewOneMoreOrder = () =>{
    let newOrder = Object.assign({}, this.props.order)
    delete newOrder['idorder']
    newOrder['paymentImg'] = []
    delete newOrder['currentProcessorRole']
    delete newOrder['currentProcessor_iduser']
    delete newOrder['currency_cost']
    delete newOrder['cost']
    delete newOrder['autoProcess']
    this.setState({oneMoreOrder: newOrder, mode: 'onemore'})
  }

  handlePaymentImgDislpay(file){
    let fileClass = typeof(file.fileclass) === 'string'? JSON.parse(file.fileclass) : file.fileclass

    this.setState({
      activeFile: file,
      fileClass: fileClass,
      fileDisplayOpen: true,
    })
  }
  handlePaymentImgDisplayCLose = () => {
    this.setState({
      activeFile: {},
      fileClass: {},
      fileDisplayOpen: false,
    })
  }
  generatePaymentImg(filesObj){
    let files = []
    let filesArray = filesObj
    if(typeof(filesArray) == 'string') filesArray = JSON.parse(filesObj)
    filesArray = filesArray||[]
    // console.log(filesArray)
    for(let i=0; i<filesArray.length; i++){
      let file = filesArray[i]
      // const size = file.size/1024 >= 1024? Math.round(file.size/(1024*1024)*100)/100 + ' MB': Math.round(file.size/1024*100)/100+' KB'
      let fileClass = typeof(file.fileclass) === 'string'? JSON.parse(file.fileclass) : file.fileclass

      files.push(
        <GridListTile key={i}>
          <img src={this.props.helperStore.ROOT_API_URL + '/img/' +file.filename} alt={file.filename} />
          <GridListTileBar
              title={fileClass.paymentCurrency + ': ' + fileClass.paymentMount}
              subtitle={<span>{fileClass.paymentType}</span>}
              actionIcon={
                <IconButton className={this.props.classes.icon} onClick={() => this.handlePaymentImgDislpay(file)}>
                  <SettingsOverscan />
                </IconButton>
              }
            />
        </GridListTile>)
    }
    return files
  }
  

  generateCustomerInfo(customer){
    let customerInfo = []
    customerInfo.push(<div className={this.props.classes.float} key="1">History Order No.: {customer.code}</div>)
    customerInfo.push(<div className={this.props.classes.float} key="2">Name: {customer.customerName}</div>)
    customerInfo.push(<div className={this.props.classes.float} key="3">Email: {customer.customerEmail}</div>)
    customerInfo.push(<div className={this.props.classes.float} key="4">Country: {customer.country}</div>)
    customerInfo.push(<div className={this.props.classes.float} key="5">University: {customer.university}</div>)
    customerInfo.push(<div className={this.props.classes.float} key="6">UniversityUrl: <a href={customer.universityUrl}>{customer.universityUrl}</a></div>)
    customerInfo.push(<div className={this.props.classes.float} key="7">Degree/Level: {customer.level}</div>)
    customerInfo.push(<div className={this.props.classes.float} key="8">English Level: {customer.englishLevel}</div>)
    return customerInfo
  }

  getRoleLabelByValue(role){
    for(let ele of this.props.helperStore.roles){
      if(ele.value === role) return ele.label
    }
  }

  handleSendResult(userEmail, userName, orderName, orderResult) {
    let order = this.props.order
    if(order.paymentStatus === 'full paid' && order.paymentVerify === 'verified'){
      notifyCustomerInEmail(userEmail, userName, orderName, orderResult.fileUrl, 'description.txt', orderResult.description)
      .then(res => {
        // console.log(res)
        if(res.status === 200){
          alert(`Order result has been sent to ${userName} in ${userEmail}.`)
          addOrderComment(order, this.props.authStore.currentUser.iduser, `${this.props.authStore.currentUser.name}(${this.props.authStore.currentUser.iduser}) send order result to ${userName} in ${userEmail}. `)
        }else{
          alert(`Send Result Error!!!  please check ${userName}\'s email ${userEmail}, and contact with ${userName} in other way to send result. `)
        }
      })
    }else if(order.paymentStatus !== 'full paid'){
      alert('The order does not FULL PAID, please contact sale to update the payment as full paid. ')
    }else if(order.paymentVerify !== 'verified'){
      alert('The full payment does NOT verified!, please verify (admin or account) the payment first! ')
    }

    
  }

  render(){
    let { classes, order, match, ordersStore, helperStore, userStore, authStore, customerStore, courseStore} = this.props;
    let { value , checked, mode, anchorEl} = this.state;
    checked = ordersStore.checked(this.props.match.params.idorder||this.props.order.idorder)
    ordersStore.isLoading ? <CircularProgress /> : ''
    if(!order) order = ordersStore.getOrderById.get(match.params.idorder)

    const UserOrderAllStatus =  ordersStore.getOrderResultById(order.idorder) || []
    const userOrder = UserOrderAllStatus.filter(ele=>ele.confirm==1)[0]||{}
    let assistant = userOrder.assigned_iduser ? userStore.getUserById(userOrder.assigned_iduser) : {}
    let comments = ordersStore.getOrderCommentsById(order.idorder)
    let invitations = ordersStore.getInvitations(order.idorder)
    let customer = customerStore.getCustomerById(order.idcustomer)
    let courseDetail = order.idcourse ? courseStore.getCourseById(order.idcourse)||{} : {}
    courseDetail = courseDetail.status === 'valid'? courseDetail : {}


    let timezone = momenttz.tz.guess()
    let isoCreateDate = moment(order.dateTime, moment.ISO_8601).tz(timezone)
    let localCreatedate = isoCreateDate.format("YYYY-MM-DD HH:mm")
    let isoDeadline = moment(order.deadline, moment.ISO_8601).tz(timezone)
    let localDeadline = isoDeadline.format("YYYY-MM-DD HH:mm")
    let isoRealDeadline = moment(order.realDeadline, moment.ISO_8601).tz(timezone)
    let localRealDeadline = isoRealDeadline.format("YYYY-MM-DD HH:mm")

    let duration = moment.duration(isoDeadline.diff(moment()));
    let hours = duration.asHours();
    let bgcolor = classes.normal
    if( hours <= 48){
      bgcolor = classes.bgwarning
    }
    if(moment()>isoDeadline){
      bgcolor = classes.bgerror
    }
    if(order.status === 'complete' || order.status === 'error'){
      bgcolor = classes.normal
    }
    if(mode == 'edit'){
      return <NewOrder order={order} formName="Modify Order" cancleFunction={this.handleCancleEdit} />
    }
    if(mode == 'onemore'){
      return <NewOrder order={this.state.oneMoreOrder} formName="Copy to Create Order" cancleFunction={this.handleCancleEdit} />
    }

    const allUsers = userStore.Users

    let filtedUsers = []
    if(this.state.search){
      for(let ele of allUsers){
        if(JSON.stringify(ele).toLowerCase().indexOf(this.state.search.toLowerCase())>=0){
          filtedUsers.push(ele)
        }
      }
    }else{
      filtedUsers = allUsers
    }
    const open = Boolean(anchorEl);

  

    return(
      <div className={[classes.root, bgcolor].join(' ')} key={order.idorder}>
        <Grid container item xs={12} justify={'space-between'}>
          <Grid item >
            <Typography variant="subheading" gutterBottom >
              {order.idorder + '. ' }&sect;{ order.name } &nbsp; 
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'qa', 'account', 'translater', 'exchanger']} oneperm>
                <span> 
                  <span className={[classes.label,classes.Success].join(' ')}>
                    {"Price: " + (helperStore.getCurrency(order.currency_sale) ? helperStore.getCurrency(order.currency_sale).currency : 'Null Currency') + ':' + order.salePrice }
                  </span>
                  <span className={[classes.label, order.paymentStatus==='full paid'?classes.Success:classes.Important].join(' ')}>
                    { order.paymentStatus }</span>
                  <span className={[classes.label, order.paymentVerify==='verifying'?classes.Important:classes.Success].join(' ')}>
                    {order.paymentVerify}
                  </span> 
                </span>
              </PermissibleRender>
              &nbsp; 
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'qa', 'account', 'translater', 'exchanger']} oneperm>
                <span className={[classes.label, order.counselorPayment?classes.Success:classes.Important].join(' ')}>
                  Counseor {order.currency_cost ? helperStore.getCurrency(order.currency_cost) ? helperStore.getCurrency(order.currency_cost).currency + ':' + order.cost : '':''} {order.counselorPayment?'paid':'unpaid'}
                </span>
              </PermissibleRender>
              <span className={classes.errorText}>[{hours <=0 ? 'Expired' : `${~~duration.days()}day ${duration.hours()}hour ${duration.minutes()}minute Due`}]</span>
            </Typography>
            <Typography vcolor="textSecondary" variant="caption"  gutterBottom>
              Create at {localCreatedate} //
              {" Deadline: " + localDeadline} // 
              {authStore.currentUser.role.indexOf('counsellor')>=0 ? '':
               " RealDeadline: " + localRealDeadline + ' // '
              }
              Created By {order.user_iduser +':'+ userStore.getUserById(order.user_iduser).name} // 
              Status: {order.currentProcessorRole + ' ' + userStore.getUserById(order.currentProcessor_iduser||1).name + ' ' + order.status}
            </Typography>
          </Grid>
          {/* <Grid item lg={1} sm={1}>
            <Typography variant="subheading" className={classes.errorText} gutterBottom >
                {`${duration.days()} days ${duration.hours()} hours ${duration.minutes()} left`}
            </Typography>
          </Grid> */}
          <Grid item>
            <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'saleman', 'qa', 'account', 'translater', 'exchanger']} oneperm>
              <FormControlLabel control={
                  <Switch 
                    checked={order.autoProcess===0? false: true}
                    onChange={this.handleSwitch(order)}
                    value={order.autoProcess}
                  />}
                  label="Auto" 
                />
              <IconButton>
              <Edit
                onClick={this.handleOrderEdit}
              /> 
              </IconButton>
              <Checkbox
                onChange={this.handleCheck}
                checked={checked}
              />
            </PermissibleRender>
            <IconButton
                className={classnames(classes.expand, {
                  [classes.expandOpen]: this.state.expanded,
                })}
                onClick={this.handleExpandClick}
                aria-expanded={this.state.expanded}
                aria-label="Show more"
              >
                <ExpandMoreIcon />
            </IconButton>
          </Grid>
        </Grid>
        
        <Grid container item xs={12}>
          <Collapse in={this.state.expanded} timeout="auto" className={classes.fullWidth}>
            <Grid item xs={12}>
            { courseDetail.idcource ? 
              <p>
                Course Link: <Link className={classes.list} to={'/courses/'+courseDetail.idcource}>{`${courseDetail.idcource}. ${courseDetail.courseCode||''} : ${courseDetail.name||''} - ${courseDetail.courseType||'' } - ${ courseDetail.university||''} - ${courseDetail.country||''} `}</Link>
              </p> : 'No Course Linked to This Order'
            }
            </Grid>
            <Grid item xs={12}>
              <pre className={classes.span} style={{whiteSpace: 'pre-wrap', wordBreak: 'keep-all'}}>{order.description}</pre>
            </Grid>
           
            <Grid container item xs={12}>
              {authStore.currentUser.role.indexOf('counsellor')>=0 ? '':
                <GridList cellHeight={180}>
                  <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
                    <ListSubheader component="div">Payment Images:</ListSubheader>
                  </GridListTile>
                  {this.generatePaymentImg(order.paymentImg)}
                </GridList>
              }
              <Grid item xs={12} >
                  <Typography key={0} variant="subheading"  gutterBottom> Supported Documents/Materials: &nbsp;
                  <span className={[classes.label , classes.Success].join(' ')}><a href='#' onClick={() => downloadAllFiles(order.name, order.fileUrl, 'Customer Words.txt', order.description)} >Download All</a></span></Typography>
                  <GenerateFileLinkView files={order.fileUrl} />
              </Grid>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'saleman']} oneperm>
                <Grid item xs={12}>
                  <Typography  variant="subheading"  gutterBottom>Customer Information:</Typography>
                  <div className={classes.span}>
                    {this.generateCustomerInfo(customer)}
                  </div>
                </Grid>
              </PermissibleRender>
            </Grid>
          </Collapse> 
        </Grid>  
        { this.state.editResult ? <EditOrderResult order={order} orderResult={{}} cancle={this.handelCancelResultUploadView} /> : ''} 
        <OrderResult order={order} editResult={this.state.editResult} />

        <Grid item xs={12} ><Divider/></Grid>

        <Grid container item xs={12} alignItems={"center"}>
          <Grid item lg={6} sm={12}>
            {order.emerg === 1? <span className={[this.props.classes.label, this.props.classes.Extremely].join(' ')} key={'-1'} >Emergent</span>: ''}
            <GenerateLabelView labels={order.labels} />
          </Grid>
          <Grid container item lg={6} sm={12} justify={'flex-end'} alignItems={"center"}>
            <Grid item>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'saleman']} oneperm>
                {
                  userOrder.assigned_iduser ? 
                  <span className={[classes.label , classes.Normal].join(' ')} > 
                  { 'Assistant: ' + userStore.getUserById(userOrder.assigned_iduser).name}
                  </span>
                  : 
                  UserOrderAllStatus.map(ele => 
                    <span key={ele.assigned_iduser} className={[classes.label , classes.Normal].join(' ')} > 
                      { userStore.getUserById(ele.assigned_iduser).name + '(confirming)' }
                    </span>
                  )
                }
                { invitations.map((invitation,index) => 
                  <span className={[classes.label , classes.Normal].join(' ')} key={index}> 
                  {this.getRoleLabelByValue( userStore.getUserById(invitation.user_iduser).role.split('|')[0]) + ': ' + userStore.getUserById(invitation.user_iduser).name}
                  </span>
                )}
              </PermissibleRender>
            </Grid>
            <Grid item>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'saleman', 'counsellor']} oneperm>
                {
                  order.status==='verifying' ?
                  <Button  size="small"   className={classes.button}  
                    onClick={this.handleAcceptorder}>
                    Accept Order
                  </Button>
                  : ''
                }
              </PermissibleRender>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['qa', 'admin']} oneperm>
                {
                  order.status==='verifying' ?
                  <Button  size="small"   className={classes.button}  
                    onClick={() => this.props.history.push(`/quality/${order.idorder}`)}>
                     Quality Process
                  </Button>
                  : ''
                }
              </PermissibleRender>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['counsellor']} oneperm>
                {
                  order.status==='waiting writer' || order.status==='processing' ?
                  <Button  size="small"   className={classes.button}  
                    onClick={this.handleRejectOrder}>
                    Reject Order
                  </Button>
                  : ''
                }
              </PermissibleRender>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>
                {
                  order.paymentVerify==='verifying' ?
                  <Button  size="small"   className={classes.button}  
                    onClick={this.handleConfirmPayment}>
                    Confirm Payment
                  </Button>:''
                }
              </PermissibleRender>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'qa']} oneperm>
                <Button  onClick={this.handleAssign} value="qa" size="small" className={classes.button}>
                  Assign to QA
                </Button>
              </PermissibleRender>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'saleman']} oneperm>
                {
                  order.status==='complete'||order.status==='verifying' ?
                  <Button  size="small"   className={classes.button}  
                    onClick={() => this.handleSendResult(assistant.email, assistant.name, order.name, userOrder)}>
                    Send Result
                  </Button>
                  : ''
                }
              </PermissibleRender>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'saleman']} oneperm>
                {
                  <Button  size="small" className={classes.button}  
                    onClick={this.handleNewOneMoreOrder}>
                    Copy Order
                  </Button>
                }
              </PermissibleRender>
              
              {  
                order.status !== 'confirming' && order.status !== 'waiting' && order.status !== 'waiting writer' ?
                <Button  size="small" className={classes.button}  onClick={this.handleResultUploadView}>
                  Upload Result
                </Button>
                : 
                ''
              }
              <Button size="small"  onClick={this.handleShowComment}  >
                {this.state.showComment?"Hide Comments":"Show Comments"}
              </Button>
            </Grid>
          </Grid>
        </Grid>

        {this.state.showComment ? 
        <div>
          <Comments comments={comments} idorder={order.idorder}/>
        </div>:''}

        <Dialog
          fullWidth={true}
          maxWidth={'md'}
          open={this.state.fileDisplayOpen}
          onClose={this.handlePaymentImgDisplayCLose}
        >
          <DialogTitle id="max-width-dialog-title"> {this.state.fileClass.paymentType + ' ' + this.state.fileClass.paymentCurrency + ': ' + this.state.fileClass.paymentMount}</DialogTitle>
          <DialogContent>
            <img className={classes.image} src={this.props.helperStore.ROOT_API_URL + '/img/' +this.state.activeFile.filename} alt={this.state.activeFile.filename} />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handlePaymentImgDisplayCLose} color="primary">
              Close
            </Button>
          </DialogActions>
        </Dialog>

        <Popover
          id="simple-popper"
          open={open}
          anchorEl={anchorEl}
          onClose={this.handleAssignClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          <List>
            <ListItem>
              <Grid item className={classes.search}>
                <TextField 
                  id='search'
                  placeholder="Search QA" 
                  className={classes.textField} 
                  onChange={this.handleSearchQa}
                  value={this.state.search}
                  />
              </Grid>
            </ListItem>
            {/* {console.log(assignToRole)} */}
            {filtedUsers.map(user => 
                (''+user.role).split('|').indexOf(this.state.assignToRole) >=0 ?
                  <ListItem button key={user.iduser} onClick={() => this.handleAssignUser(user)}>
                    {/* <Checkbox
                      checked={this.state.checkedUser.indexOf(user) !== -1}
                      tabIndex={-1}
                      disableRipple
                      onClick={() => this.handleCheckUser(user)}
                    /> */}
                    <ListItemText primary={`${user.name} - ${user.role}`} secondary={user.labels ? user.labels.replace('|', ',') : ''} />
                  </ListItem>
                : ''
              )}
              {/* <Divider />
              <ListItem>
                <Grid>
                <TextField
                  id="currency_cost"
                  required
                  select
                  label = "Currency"
                  value={this.state.currency_cost||""}
                  onChange={this.handleInputChange}
                  error ={this.state.currency_costValidate ? true : false  }
                  helperText={this.state.currency_costValidate}
                  className={classes.textField}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  SelectProps={{
                    native: true,
                    MenuProps: {
                      className: classes.menu,
                    },
                  }}
                >
                  <option value="" disabled>
                    Select Currency
                  </option>
                  {helperStore.currencyArray.map(option => (
                    <option key={option.idcurrency} value={option.idcurrency}>
                      {option.currency}
                    </option>
                  ))}
                </TextField>   
                <TextField
                  id="cost"
                  required
                  onChange={this.handleInputChange}
                  value={this.state.cost}
                  label="Price to Writer"
                  error ={this.state.costValidate ? true : false  }
                  helperText={this.state.costValidate}
                  className={classes.textField}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />  
              </Grid>
            </ListItem> */}
            <Divider />
          </List>
{/*             
            <Button  size='small' variant="outlined" onClick={() => this.handleAssignUser()} className={classes.button}>
              Assign
            </Button> */}
        </Popover>
      </div>
      );
    }
} 

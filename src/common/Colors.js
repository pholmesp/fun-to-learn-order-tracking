const success = '#5cb85c'
const info = '#a7ddf4'
const warning = '#f0ad4e'
const error = '#ea1e63'
const gray = '#444444'
const primary='#428bca'

const bginfo = '#d9edf7'
const bgwarning = '#f7ecb5'
const bgerror = '#ffcccc'
const bggray = '#e5e5e5'
const bglightyellow ='#FFFCEB'
const bgyellow = '#fffacd'
const bgsuccess = '#bde2bd'

const lightBlue = '#b3e5fc'
const bgLightBlue = '#EFF9FE'
const cyan = '#e0f7fa'

export {success, info, warning, error, gray,
        bginfo, bgwarning, bgerror, bggray, bglightyellow, bgyellow, bgsuccess,
        lightBlue, bgLightBlue, cyan}
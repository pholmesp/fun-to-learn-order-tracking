import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import moment from 'moment';
import momenttz from 'moment-timezone'

import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LinearProgress from '@material-ui/core/LinearProgress';
import IconButton from '@material-ui/core/IconButton';
import ArrowBack from '@material-ui/icons/ArrowBack';

import Button from '@material-ui/core/Button';
import blue from '@material-ui/core/colors/blue';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import Typography from '@material-ui/core/Typography';

import ListSubheader from '@material-ui/core/ListSubheader';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

import Cancel from '@material-ui/icons/Cancel'

import './react-datetime.css'
import {success,gray, info, warning, error, bginfo, bgwarning, bgerror} from './Colors'
import loading from './loading'
import { AutoCompleteCourse, AutoCompleteCustomer } from './AutoComplete'
import { GenerateLabelEdit } from '../util/generateLabel';
import { GenerateFileLinkEdit } from '../util/generateFileLink';

import { Spin, message } from 'antd'
import agent from "../agent";

message.config({
  top: 100,
  duration: 4,
  maxCount: 3,
});
const level = [
  "Extremely",
  "Important",
  "Normal",
  "Note"
]

const styles = theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing.unit *2
  },
  button: {
    margin: theme.spacing.unit,
  },
  padding: {
    padding: theme.spacing.unit * 2
  },
  input: {
    display: 'none',
  },
  marginTop: {
    marginTop: theme.spacing.unit * 2
  },
  listitemBackColor: {
    backgroundColor: blue[100],

  }, 
  Extremely: {
    backgroundColor: error,
  },
  Important: {
    backgroundColor: warning,
  },
  Normal: {
    backgroundColor: info,
  }, 
  bgerror: {
    backgroundColor: bgerror,
  },
  margin: {
    marginTop: theme.spacing.unit * 4,
    margin: theme.spacing.unit,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit * 2,
    // width: theme.breakpoints.values.sm,
  },
  label:{
    padding: '3px',
    paddingLeft: '5px',
    paddingRight: '5px',
    marginRight: '5px',
    marginTop: '4px',
    borderRadius: 3,
    fontSize: '12px',
    color: gray, 
    textDecoration: 'None',
  },
  list:{
    color: gray,
    padding: '3px',
    fontSize: '14px',
    textDecoration: 'underline',
  },
  grayText: {
    color: gray,
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
    [theme.breakpoints.up('sm')]: {
      marginRight: -8,
    },
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  img: {

    padding: 5,
    paddingBottom: 8,
    width: 280,
  },
  span: {
    fontSize: '14px',
    color: gray,
    paddingLeft: '5px'
  },
  fullWidth: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit,
    border: 2,
    width: theme.breakpoints.values.lg,
  }, 
  // img: {
  //   padding: 5,
  //   paddingBottom: 8,
  //   width: 280,
  // },
  // span: {
  //   fontSize: '14px',
  //   color: gray,
  //   paddingLeft: '5px'
  // },
  badge: {
    top: 5,
    right: -15,
    cursor: 'pointer',
  },
  marginRight:{
    marginRight: theme.spacing.unit*3,
  },
  fileList: {
    margin: 5,
    padingBottom: 8,
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
  float:{
    // float: 'left',
    fontSize: '14px',
    color: gray,
    padding: '3px',
    // marginRight: '10px'
  }, 
  largeSpace: {
    lineHeight: 1.5
  }

});


@withStyles(styles)
@inject('helperStore', 'ordersStore', 'authStore', 'courseStore', 'customerStore')
@withRouter
@observer
export default class NewOrder extends Component{
  state = {
    currency: 1,
    order: this.props.order||{"courseType":"",  "topic":"", "assignmentType":"", "idcustomer":"", "salePrice":"", "paymentImg":[], "currency_sale":"", "description":"",  "deadline":"", "realDeadline":"", "labels":[],  "fileUrl":[], "words":"", "referenceStyle":"", "expection":"", "emerg":0},
    label: {"level": "Normal", "content": ""},
    orderValidate: {"wechat":"", "courseType":"", "assignmentType":"", "idcustomer":"", "salePrice":"", "paymentImg":"", "currency_sale":"",  "deadline":"", "realDeadline":""},
    timeZone: momenttz.tz.guess(),
    paymentArray: this.props.order?typeof(this.props.order.paymentImg)==='string'?JSON.parse(this.props.order.paymentImg):this.props.order.paymentImg:[],
    paymentImgFiles: [],
    paymentObj: {paymentType: '', paymentCurrency: '', paymentMount: '', paymentImg: ''},
    paymentObjValidate: {paymentType: '', paymentCurrency: '', paymentMount: '', paymentImg: ''},
    customer: this.props.order? this.props.customerStore.getCustomerById(this.props.order.idcustomer) : undefined,
    errorInfo: [], 
    imageAdding: "",
    fileAdding: "",
    courseInput: false,
  }

  handleCurrencyChange = (event) => {
    this.setState({
      currency: event.target.value,
    });
  };
  handleTimeZoneChange = (event) => {
    this.setState({
      timeZone: event.target.value,
    });
  };


  handleCustomerInfoChange = (customer) => {
    if(customer){
      let updatingOrder = this.state.order
      updatingOrder['idcustomer'] = customer.value
      this.setState({
        order: updatingOrder,
        customer: this.props.customerStore.getCustomerById(customer.value) 
      })
    }else{
      let updatingOrder = this.state.order
      updatingOrder['idcustomer'] = ""
      this.setState({
        order: updatingOrder,
        customer: undefined
      })
    }
  }

  handleInputChange = (event) =>{
    console.log("chage: "+event.target.id+event.target.value)
    let updatingOrderValidation = this.state.orderValidate
    if(event.target.id in [...Object.keys(updatingOrderValidation)]){
      if((''+this.state.order[event.target.id]).trim().length===0){
        updatingOrderValidation[event.target.id] = event.target.id + ' is required'
      }else{
        updatingOrderValidation[event.target.id] = ""
      }
    }
    let updatingOrder = this.state.order
    updatingOrder[event.target.id] = event.target.value
    this.setState({
      order: updatingOrder,
      orderValidate: updatingOrderValidation
    })
  }
  handleLabelInputChange = (event) => {
    let updatingLabel = this.state.label
    updatingLabel[event.target.id] = event.target.value
    this.setState({
      label: updatingLabel
    })
  }
  handleAddLabel = (event) => {
    let updatingOrder = this.state.order
    let updatingLabels = this.state.order.labels
    if(typeof(updatingLabels) === 'string'){
      updatingLabels = JSON.parse(updatingLabels)
    }
    updatingLabels = updatingLabels.concat(this.state.label)
    updatingOrder["labels"] = updatingLabels
    this.setState({
      order: updatingOrder, 
      label: {"level": "Normal", "content": ""}
    })
  }
  handleUpdateLabel = newLabels => {
    let orginalOrder = this.state.order
    orginalOrder['labels'] = newLabels
    this.setState({
      order: orginalOrder,
    })
}
 

  handlePaymentInput = (event) => {
    let orginalPaymentObj = this.state.paymentObj
    let orginalPaymentObjValidate = this.state.paymentObjValidate

    let validateInfo = ""
    console.log(event.target.value, event.currentTarget.value)
    if(event.target.value.trim().length===0){
      validateInfo =  event.target.id + ' is required'
    }else{
      validateInfo = "" 
    }
    orginalPaymentObj[event.target.id] = event.target.value,
    orginalPaymentObjValidate[event.target.id] = validateInfo
    this.setState({
      paymentObj: orginalPaymentObj,
      paymentObjValidate: orginalPaymentObjValidate
    })
  }
  paymentInputValidate(){
    let updatingOrderValidate = this.state.paymentObjValidate
    let validateStatus = false
    for(let key in this.state.paymentObj){
      console.log(key)
      if(typeof(this.state.paymentObj[key]) === 'string'){
        if(this.state.paymentObj[key].trim().length===0){
          console.log(this.state.paymentObj[key])
          updatingOrderValidate[key] = key + ' is required'
          validateStatus = true
        }
      }else{
        if(!this.state.paymentObj[key]){
          console.log(this.state.paymentObj[key])
          updatingOrderValidate[key] = key + ' is required'
          validateStatus = true
        }
      }
     
    }
    this.setState({
      paymentObjValidate:  updatingOrderValidate
    })
    return validateStatus
  }

  handleAddPaymentImg = (event) => {
    const files = Array.from(event.target.files)
    let orginalPaymentObj = this.state.paymentObj
    let orginalPaymentObjValidate = this.state.paymentObjValidate
    orginalPaymentObj['paymentImg'] = files,
    orginalPaymentObjValidate['paymentImg'] = ""
    this.setState({
      paymentImgFiles: files, 
      paymentObj: orginalPaymentObj,
      paymentObjValidate: orginalPaymentObjValidate,
    })
  }

  handleAddPayment = (event) =>{
    if(!this.paymentInputValidate()){
      this.setState({imageAdding:"uploading..."})
      const files = Array.from(this.state.paymentImgFiles)
      const formData = new FormData()
      files.forEach((file) => {
        formData.append('file', file)
        formData.append('fileclass', JSON.stringify(this.state.paymentObj))
      })
      this.props.helperStore.uploadFiles(formData).then(res =>{
        if(res.statusCode === 200){
          let orginalOrder = this.state.order
          let paymentImg = this.state.paymentArray
          let orginalOrderValidate = this.state.orderValidate
          if(typeof(paymentImg) == 'string') paymentImg = JSON.parse(paymentImg)
          paymentImg = paymentImg||[]
          paymentImg = paymentImg.concat(res)
          orginalOrderValidate['paymentImg'] = ""
          orginalOrder['paymentImg'] = paymentImg
          let paymentStatus = ''
          if(this.state.paymentObj.paymentType === 'Bound Payment') paymentStatus = 'bound paid'
          if(this.state.paymentObj.paymentType === 'Part Payment') paymentStatus = 'part paid'
          if(this.state.paymentObj.paymentType === 'Full Payment') paymentStatus = 'full paid'
          orginalOrder['paymentStatus'] = paymentStatus
          orginalOrder['paymentVerify'] = 'verifying'
          this.setState({
            paymentArray: paymentImg, 
            order: orginalOrder,
            orderValidate: orginalOrderValidate,
            imageAdding: "",
            paymentObj: {paymentType: '', paymentCurrency: '', paymentMount: '', paymentImg: ''}
          })
        }else{
          this.setState({imageAdding: "upload payment image error: "+res})
        }
        
      }).finally(res=>{
        this.setState({imageAdding: ""})
      })
    }
   
  }

  handlePaymentImgDelete(filename){
      let orginalOrder = this.state.order
      let paymentImg = orginalOrder['paymentImg']
      if(typeof(paymentImg) == 'string') paymentImg = JSON.parse(paymentImg)
      paymentImg = paymentImg||[]
      let newPaymentImg = this.deleteArrayEle(paymentImg, filename)
      orginalOrder['paymentImg'] = newPaymentImg
      this.setState({
        order: orginalOrder,
        paymentArray: newPaymentImg
      })
  }

  deleteArrayEle(array, filename){
    let newArray = []
    for(let ele of array){
      if(filename!==ele.filename) newArray.push(ele)
    }
    return newArray
  }

  handleFileclassChange = (event) => {
    this.setState({
      fileclass: event.target.value
    })
  }
  handleFilesUpload = (event) => {
    this.setState({fileAdding:"uploading..."})
    const files = Array.from(event.target.files)
    const formData = new FormData()
    files.forEach((file) => {
      formData.append('file', file)
    })
    formData.append('fileclass', this.state.fileclass)
    this.props.helperStore.uploadFiles(formData).then(res =>{
      if(res.statusCode === 200){
        console.log(res)
        let orginalOrder = this.state.order
        let fileUrl = this.state.order.fileUrl
        if(typeof(fileUrl) == 'string') fileUrl = JSON.parse(fileUrl)
        fileUrl = fileUrl||[]
        fileUrl = fileUrl.concat(res)
        orginalOrder['fileUrl'] = fileUrl
        this.setState({
          order: orginalOrder,
          fileAdding: ""
        })
      }else{
        this.setState({fileAdding: "upload files error: "+res})
      }
    }).finally(res=>{
        this.setState({fileAdding: ""})
      })
  }

  handleUpdateFileUrls = newFileUrls => {
    let orginalOrder = this.state.order
    orginalOrder['fileUrl'] = newFileUrls
    this.setState({order: orginalOrder})
}

  validateField(){
    let updatingOrderValidate = this.state.orderValidate
    let validateStatus = false
    let errorInfo = []
    for(let key in updatingOrderValidate){
      // console.log(this.state.order[key], key)
      if((''+this.state.order[key]).trim().length===0){
        updatingOrderValidate[key] = key + ' is required'
        validateStatus = true
        // console.log(key, updatingOrderValidate[key])
        errorInfo.push(key + ' is required')
      }
    }
    this.setState({
      orderValidate: updatingOrderValidate,
      errorInfo: errorInfo
    })
    return validateStatus
  }
  utcDatetime(dt, tz){
    return moment.tz(dt,tz).format()
  }

  handleCreateOrder = () => {
    if (!this.validateField()) {

      this.props.ordersStore.submitProcess()
      let order = Object.assign({}, this.state.order)
      if (typeof (order.labels) == 'object') order.labels = JSON.stringify(order.labels)
      if (typeof (order.paymentImg) == 'object') order.paymentImg = JSON.stringify(order.paymentImg)
      if (typeof (order.fileUrl) == 'object') order.fileUrl = JSON.stringify(order.fileUrl)
      if (!order.idorder) order.user_iduser = this.props.authStore.currentUser.iduser || 1
      if (!order.idorder) order.autoProcess = 0
      if (!order.idorder) order.status = 'confirming'
      // let paymentStatus = ''
      // for(let pay of this.state.paymentArray){
      //   let type = JSON.parse(pay.fileclass)
      //   if(type.paymentType == 'Bound Payment') paymentStatus = 'bound paid'
      //   if(type.paymentType == 'Part Payment') paymentStatus = 'part paid'
      //   if(type.paymentType == 'Full Payment') paymentStatus = 'full paid'
      // }
      // if(!order.idorder) order.paymentStatus=paymentStatus

      // if(!order.idorder) order.paymentVerify='verifying'
      let topic = order.assignmentType === 'Other' ? order.topic : order.assignmentType + ':' + order.topic
      let words = order.words ? ' -' + order.words + ' words' : ''
      let courseCode = order.idcourse ? ' -' + this.props.courseStore.getCourseById(order.idcourse).courseCode || '' : ''
      let courseName = order.idcourse ? ' -' + this.props.courseStore.getCourseById(order.idcourse).name : ''
      let utcDeadline = moment.tz(order.deadline, 'Asia/Shanghai').format('MMM Do')
      let customerCode = this.state.customer.code;
      let wechatName;
      if (!order.idorder) {
        customerCode = parseInt(this.state.customer['code'], 10) + 1
      } else {
        customerCode = parseInt(order.name.substring(this.state.customer.customerName.length).split('-')[0])
      }
      if(this.props.authStore.currentUser.role.indexOf('admin')<0){
         wechatName = JSON.parse(this.props.authStore.currentUser.wechatName)[order['wechat']].split('(')[0]
      }
      if(this.props.authStore.currentUser.role.indexOf('admin')>=0){
         agent.Account.getWechatInfo(order['wechat']).then(res=>{wechatName = res.wechatName})
      }


      order.name = `${this.state.customer.customerName}${customerCode}${words}-${topic}-${order.courseType}${courseCode}${courseName}-${wechatName}-${utcDeadline}`

      if (!order.idorder) order.currentProcessorRole = 'admin'
      if (!order.idorder) order.currentProcessor_iduser = 1


      if (!order.idorder) order.dateTime = moment.tz().format('YYYY-MM-DDTHH:mm:ss')
      order.deadline = this.utcDatetime(order.deadline, this.state.timeZone)
      order.realDeadline = this.utcDatetime(order.realDeadline, this.state.timeZone)

      if (this.props.idPackage) {
        order['orderPackage_idorderPackage'] = this.props.idPackage
        delete order['idorderPackage']
      }

      console.log(order)


      if (this.props.formName && this.props.formName === "Modify Order") {
        this.props.ordersStore.createOrder(order)
            .then(res => {

          // console.log(res.statusCode, JSON.stringify(res))
          if (res.statusCode == 200) {
            if (!order.idorder) {
              let customer = this.props.customerStore.getCustomerById(this.state.order.idcustomer)
              customer['code'] = parseInt(customer['code'], 10) + 1
              this.props.customerStore.updateCustomer(customer)
            }
            if (this.props.cancleFunction) this.props.cancleFunction()
            if (this.props.goBack) this.props.goBack()
            this.props.history.push('/')
            // this.props.history.goBack()

            message.success('Successfully modified the order.')
          } else {
            // this.setState({
            //   error: JSON.stringify(res)
            // })
            alert('Operation failed, please refresh and try again.')
          }
        }).catch(error => {
          alert('Operation failed, please refresh and try again.')
        })
      } else {

        // let idWechat = order['wechat'].split('(')[1].replace(')', "")
        // agent.Account.getIdWeChat(idWechat).then(res => {
        //   if (res.statusCode === 200) {
        //     order['wechat'] = res[0].id


            this.props.ordersStore.createOrder(order).then(res => {
               console.log(res.statusCode, JSON.stringify(res))
              if (res.statusCode == 200) {
                if (!order.idorder) {
                  let customer = this.props.customerStore.getCustomerById(this.state.order.idcustomer)
                  customer['code'] = parseInt(customer['code'], 10) + 1
                  this.props.customerStore.updateCustomer(customer)
                }

                if (this.props.cancleFunction) this.props.cancleFunction()
                if (this.props.goBack) this.props.goBack()
                this.props.history.push('/')
                // this.props.history.goBack()
                message.success('Successfully created a new order.')
              } else {
                // this.setState({
                //   error: JSON.stringify(res)
                // })
                alert('Operation failed, please refresh and try again.')
              }
            }).catch(error => {
              alert('Operation failed, please refresh and try again.')
            })
      }
    }
    else{
      message.error("Invalid input, Please check again!")
    }
  }








  generatePaymentImg(filesObj){
    let files = []
    let filesArray = filesObj
    if(typeof(filesArray) == 'string') filesArray = JSON.parse(filesObj)
    filesArray = filesArray||[]
    for(let i=0; i<filesArray.length; i++){
      let file = filesArray[i]
      // const size = file.size/1024 >= 1024? Math.round(file.size/(1024*1024)*100)/100 + ' MB': Math.round(file.size/1024*100)/100+' KB'
      // console.log(file)
      let fileClass = typeof(file.fileclass) === 'string'? JSON.parse(file.fileclass) : file.fileclass
      files.push(
        <GridListTile key={i}>
          <img src={this.props.helperStore.ROOT_API_URL + '/img/' +encodeURIComponent(file.filename)} alt={file.filename} />
          <GridListTileBar
              title={fileClass.paymentCurrency + ': ' + fileClass.paymentMount}
              subtitle={<span>{fileClass.paymentType}</span>}
              actionIcon={
                <IconButton className={this.props.classes.icon} onClick={() => this.handlePaymentImgDelete(file.filename)}>
                  <Cancel />
                </IconButton>
              }
            />
        </GridListTile>)
    }
    return files
  }

  generateCustomerInfo(customer){
    let customerInfo = []
    customerInfo.push(<div className={this.props.classes.float} key="1">History Order No.: {customer.code}</div>)
    customerInfo.push(<div className={this.props.classes.float} key="2">Name: {customer.customerName}</div>)
    customerInfo.push(<div className={this.props.classes.float} key="3">Email: {customer.customerEmail}</div>)
    customerInfo.push(<div className={this.props.classes.float} key="4">Country: {customer.country}</div>)
    customerInfo.push(<div className={this.props.classes.float} key="5">University: {customer.university}</div>)
    customerInfo.push(<div className={this.props.classes.float} key="6">UniversityUrl: <a href={customer.universityUrl}>{customer.universityUrl}</a></div>)
    customerInfo.push(<div className={this.props.classes.float} key="7">Degree/Level: {customer.level}</div>)
    customerInfo.push(<div className={this.props.classes.float} key="8">English Level: {customer.englishLevel}</div>)
    return customerInfo
  }
  generateErrorInfo(errorInfo){
    let info = []
    for(let ele of errorInfo){
      info.push(<div className={this.props.classes.float} key={ele}>{ele}</div>)
    }
    return info
  }

  handleSwitch = (event) => {
    let updatingOrder = this.state.order
    updatingOrder['emerg'] = updatingOrder['emerg']==0? 1 : 0
    
    this.setState({order:updatingOrder})
  }
  handleSwitchCourseInput = () => {
    this.setState({courseInput: !this.state.courseInput})
  }
  handleManualCourseInputChange = (event) =>{
    this.setState({manualCourseName: event.target.value})
    
  }
  handleManualCourseInputSubmit = () => {
    let courseObj = {}
    courseObj['name'] = this.state.manualCourseName
    courseObj['status'] = 'private'
    courseObj['createBy'] = this.props.authStore.currentUser.iduser
    courseObj['courseType'] = this.state.order.courseType
    this.props.courseStore.createCourse(courseObj).then(res =>{
      if(res.statusCode === 200){
        this.props.courseStore.loadCourses().then(rr => {
          let updatingOrder = this.state.order
          updatingOrder['idcourse'] = res.insertId
          this.setState({
            order: updatingOrder,
          })
        })
      }else{
        alert('Add Course name error, please refresh and try again.')
      }
    })
  }

  render(){
    const { classes, helperStore, ordersStore, history, customerStore, authStore, courseStore} = this.props;
    const { order, orderValidate, customer, fileAdding, imageAdding } = this.state
    if(ordersStore.isLoading) return loading
    let courseName = order.idcourse ? courseStore.getCourseById(order.idcourse).name : ''
    return(
        <div>
          <Spin spinning={ordersStore.creating}>
          <h3>
            <IconButton>
              <ArrowBack onClick={() => this.props.goBack ? this.props.goBack() : history.goBack()} />
            </IconButton>
            &nbsp; { this.props.formName || "Create a New Order"}
          </h3>
          {
            this.state.errorInfo.length > 0 ? 
            <Grid className={[classes.bgerror, classes.padding].join(' ')}>{this.generateErrorInfo(this.state.errorInfo)}</Grid>
            : ''
          }
          <form className={classes.container}>
            <div className={classes.root}>
            <h5>1. Order Info</h5>
              <TextField
                id="wechat"
                required
                select
                label = "Sale Wechat"
                value={order.wechat || ""}
                onChange={this.handleInputChange}
                error ={(orderValidate.wechat||'').length === 0 ? false : true  }
                helperText={orderValidate.wechat}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
                SelectProps={{
                  native: true,
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
              >
              <option value="" disabled >
                Sale Wechat 
              </option>
                {authStore.currentUser.wechatName? Object.keys(JSON.parse(authStore.currentUser.wechatName)).map(key => (
                  <option key={key} value={key}>
                    {JSON.parse(authStore.currentUser.wechatName)[key]}
                  </option>
                )):''}
              </TextField>
              <TextField
                id="courseType"
                required
                select
                label = "Course Type"
                value={order.courseType||""}
                onChange={this.handleInputChange}
                error ={(orderValidate.courseType||'').length === 0 ? false : true  }
                helperText={orderValidate.courseType}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
                SelectProps={{
                  native: true,
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
              >
              <option value="" disabled >
                Course Type
              </option>
              {helperStore.courseType.map(option => (
                  <option key={option} value={option}>
                    {option}
                  </option>
                ))}
              </TextField> 
              <Grid item sm={12} md={6} lg={4}>
                {
                  !this.state.courseInput ? 
                  <AutoCompleteCourse placeholderText="Type to search course ..."  defaultValue={{label: courseName, value: order.idcourse}} options={courseStore.Courses.filter(ele => ele.status==='valid')} onChangeCallback={this.handleInputChange}/>
                  :
                  order.idcourse ? 
                  <div className={[classes.marginTop, classes.marginLeft].join(' ')}>Course Name: {this.state.manualCourseName}</div>
                  :
                  <div>
                    <TextField
                      id="manualCourseName"
                      value={this.state.manualCourseName}
                      onChange={this.handleManualCourseInputChange}
                      label="Course Name"
                      className={classes.textField}
                      InputLabelProps={{
                        shrink: true,
                    }}
                    />
                    <Button color="secondary" size='small' className={classes.margin} onClick={this.handleManualCourseInputSubmit}>
                      Save Course Name
                    </Button>
                  </div>
                }
              </Grid>
              <Grid item sm={6} md={3} lg={2}>
                {
                  !this.state.courseInput ? 
                  <Button color="primary"  size='small'  onClick={this.handleSwitchCourseInput}>
                    Not find course? Input Manually
                  </Button>
                  :''
                  // <Button color="primary"  size='small'  onClick={this.handleSwitchCourseInput}>
                  //   Go Back to Course Search
                  // </Button>
                }
                
              </Grid>
              <TextField
                id="assignmentType"
                required
                select
                value={order.assignmentType}
                onChange={this.handleInputChange}
                label="Assignment Type"
                error ={(orderValidate.assignmentType||'').length === 0? false : true  }
                helperText={orderValidate.assignmentType}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
                SelectProps={{
                  native: true,
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
              >
                <option value="" disabled selected>
                  Assignment Type
                </option>
              {helperStore.assignmentType.map(option => (
                <option key={option} value={option}>
                  {option}
                </option>
              ))}
              </TextField>
              {
                order.assignmentType==='Other' ? 
                <TextField
                  id="topic"
                  value={order.topic}
                  onChange={this.handleInputChange}
                  label="Input Assignment Type"
                  className={classes.textField}
                  InputLabelProps={{
                    shrink: true,
                }}
                /> : ''
              }
              
              <TextField
                id="words"
                value={order.words}
                onChange={this.handleInputChange}
                label="Words Number"
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
              }}
              />
              <TextField
                id="referenceStyle"
                select
                value={order.referenceStyle||""}
                onChange={this.handleInputChange}
                label="Reference Style"
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
                SelectProps={{
                  native: true,
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
              >
                <option value="" disabled>
                  Reference Type
                </option>
              {helperStore.referenceType.map(option => (
                <option key={option} value={option}>
                  {option}
                </option>
              ))}
              </TextField>
              <TextField
                id="expection"
                value={order.expection}
                onChange={this.handleInputChange}
                label="Expectation"
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
              }}
              />
              <FormControlLabel className={classes.marginTop} control={
                  <Switch 
                    checked={order.emerg===0? false: true}
                    onChange={this.handleSwitch}
                    value={order.emerg}
                  />}
                  label="Emergent" 
                />
            </div>

            <div className={[classes.root, classes.marginTop].join(' ')}>
            <div>
              <h5>2. Timezone Deadline and Notes/Labels</h5>
              <TextField
                  id="timeZone"
                  select
                  required
                  defaultValue={this.state.timeZone}
                  onChange={this.handleTimeZoneChange}
                  label="Time Zone"
                  error ={this.state.timeZone.length === 0 ? true : false  }
                  className={classes.textField}
                  SelectProps={{
                    native: true,
                    MenuProps: {
                      className: classes.menu,
                    },
                  }}>
                  <option key={momenttz.tz.guess()} value={momenttz.tz.guess()}>{momenttz.tz.guess()}</option>
                  {
                    helperStore.timezoneArray.map(t => (
                      <option key={t.idtimezone} value={t.timezone}>{t.timezone + ' - ' + t.offset}</option>
                    ))
                  }
              </TextField>  
              <TextField
                  id="deadline"
                  required
                  // value={order.deadline}
                  onChange={this.handleInputChange}
                  label="Deadline"
                  type="datetime-local"
                  defaultValue={ moment(order.deadline, moment.ISO_8601).tz(this.state.timeZone).format("YYYY-MM-DDTHH:mm") || moment().add(7, 'days').format("YYYY-MM-DD") + "T23:59"}
                  error ={orderValidate.deadline.length === 0 ? false : true  }
                  helperText={orderValidate.deadline}
                  InputLabelProps={{
                    shrink: true,
                  }}
                  className={classes.textField}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                  {console.log(moment(order.realDeadline, moment.ISO_8601).tz(this.state.timeZone).format("YYYY-MM-DDTHH:mm"))}

                <TextField
                  id="realDeadline"
                  required
                  // value={order.realDeadline}
                  onChange={this.handleInputChange}
                  label="Real Deadline"
                  type="datetime-local"
                  defaultValue={moment(order.realDeadline, moment.ISO_8601).tz(this.state.timeZone).format("YYYY-MM-DDTHH:mm") || moment().add(14, 'days').format("YYYY-MM-DD") + "T23:59:59"}
                  error ={orderValidate.realDeadline.length === 0 ? false : true  }
                  helperText={orderValidate.realDeadline}
                  className={classes.textField}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
                </div>
                <hr/>
                <div>
                  <TextField
                    id="level"
                    select
                    onChange={this.handleLabelInputChange}
                    label="Note Level"
                    className={classes.textField}
                    InputLabelProps={{
                      shrink: true,
                    }}
                    SelectProps={{
                      native: true,
                      MenuProps: {
                        className: classes.menu,
                      },
                    }}>
                    <option value="" disabled selected>
                      Select Levels
                    </option>
                    {
                      [...level].map(t => (
                        <option key={t} value={t}>{t}</option>
                      ))
                    }
                  </TextField>  
                  <TextField
                    id="content"
                    onChange={this.handleLabelInputChange}
                    label="Add Labels or Notes"
                    className={classes.textField}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  <Button size="small" onClick={this.handleAddLabel} variant="contained" color="primary" className={classes.margin}>Add Label</Button>
              </div>
              <div>
                  Lables: 
                  <GenerateLabelEdit labels={order.labels} updateLabel = {this.handleUpdateLabel}/>
              </div>
            </div>

            <div className={[classes.root, classes.marginTop].join(' ')}>
              <h5>3. Customer Info</h5>
              {customer ? '' : <div className={classes.bgerror}>Customer Information required. </div>}
              <div>
                <AutoCompleteCustomer placeholderText="Type to search customer ..."  defaultValue={
                  customer ? {label: `${customer.code}-${customer.customerName}-${customer.customerEmail}-${customer.university}-${customer.wechatName}`, value: customer.idcustomer} : {label: '', value: ''}}
                  options={ authStore.currentUser.role.indexOf('admin')>=0 ? customerStore.Customers : customerStore.getSalerCustomer(authStore.currentUser.iduser)}
                  onChangeCallback={this.handleCustomerInfoChange}
                  />
              </div>
              {customer ? this.generateCustomerInfo(customer): ''}
            </div>

            <div className={[classes.root, classes.marginTop].join(' ')}>
             <h5>4. Payment Information</h5>
             <div>
             <TextField
                id="currency_sale"
                required
                select
                label = "Currency"
                value={order.currency_sale||""}
                onChange={this.handleInputChange}
                error ={orderValidate.currency_sale.length === 0 ? false : true  }
                helperText={orderValidate.currency_sale}
                className={classes.textField}
                InputLabelProps={{
                   shrink: true,
                }}
                SelectProps={{
                  native: true,
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
              >
               <option value="" disabled>
                Select Currency
              </option>
                {helperStore.currencyArray.map(option => (
                  <option key={option.idcurrency} value={option.idcurrency}>
                    {option.currency}
                  </option>
                ))}
              </TextField>   
              <TextField
                id="salePrice"
                required
                onChange={this.handleInputChange}
                value={order.salePrice}
                label="Sale Price"
                error ={this.state.orderValidate.salePrice.length === 0 ? false : true  }
                helperText={this.state.orderValidate.salePrice}
                className={classes.textField}
                InputLabelProps={{
                   shrink: true,
                }}
              />  
              
             </div>
             <hr/>
             <div>
              {orderValidate.paymentImg.length == 0 ? '' : <div className={classes.bgerror}>Initial, Part or Full payment required.</div>}
              <TextField
                id="paymentType"
                required
                select
                label = "Payment Type"
                onChange={this.handlePaymentInput}
                value={this.state.paymentObj.paymentType||''}
                error ={(this.state.paymentObjValidate.paymentCurrency||'').length === 0 ? false : true  }
                helperText={this.state.paymentObjValidate.paymentCurrency}
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
                SelectProps={{
                  native: true,
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
              >
              <option value="" disabled selected>
                Payment Type
              </option>
                {helperStore.paymentType.map(option => (
                  <option key={option} value={option}>
                    {option}
                  </option>
                ))}
              </TextField>  
              <TextField
                id="paymentCurrency"
                required
                select
                label = "Currency"
                value={this.state.paymentObj.paymentCurrency||""}
                onChange={this.handlePaymentInput}
                error ={(this.state.paymentObjValidate.paymentCurrency||'').length === 0 ? false : true  }
                helperText={this.state.paymentObjValidate.paymentCurrency}
                className={classes.textField}
                InputLabelProps={{
                   shrink: true,
                }}
                SelectProps={{
                  native: true,
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
              >
                <option value="" disabled>
                  Currency
                </option>
                {helperStore.currencyArray.map(option => (
                  <option key={option.idcurrency} value={option.currency}>
                    {option.currency}
                  </option>
                ))}
              </TextField>   
              <TextField
                id="paymentMount"
                required
                value={this.state.paymentObj.paymentMount}
                onChange={this.handlePaymentInput}
                label="Payment Amount"
                error ={(this.state.paymentObjValidate.paymentMount||'').length === 0 ? false : true  }
                helperText={this.state.paymentObjValidate.paymentMount}
                className={classes.textField}
                InputLabelProps={{
                   shrink: true,
                }}
              />  
              <TextField
                  id="paymentImg"
                  required
                  label="Upload Payment Image (Max 20MB)"
                  type="file"
                  accept='image/*'
                  onChange = {this.handleAddPaymentImg}
                  error ={(this.state.paymentObjValidate.paymentImg||'').length === 0 ? false : true  }
                  helperText={this.state.paymentObjValidate.paymentImg}
                  className={classes.textField}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              <Button size="small" onClick={this.handleAddPayment} variant="contained" color="primary" className={classes.margin}>Add Payment</Button>
              {imageAdding.length>0 ? imageAdding.length == 12 ?
                <div className={classes.marginTop}><LinearProgress color="secondary" />{imageAdding}</div>
                : 
                <div className={[classes.bgerror, classes.marginTop].join(' ')}>{imageAdding}</div>
                : ''
              }
              <div>
                <Grid container>
                  <GridList cellHeight={180}>
                      <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
                        <ListSubheader component="div">Payment Images:</ListSubheader>
                      </GridListTile>
                      {this.generatePaymentImg(order.paymentImg)}
                    </GridList>
                </Grid>
                </div>
              </div>
            </div>


            <div className={[classes.root, classes.marginTop].join(' ')}>
              <h5>5. Documents/Materials </h5>
              <div>
                  <TextField
                      id="fileclass"
                      select
                      label = "File Class"
                      onChange={this.handleFileclassChange}
                      className={classes.textField}
                      InputLabelProps={{
                        shrink: true,
                      }}
                      SelectProps={{
                        native: true,
                        MenuProps: {
                          className: classes.menu,
                        },
                      }}
                    >
                    <option value="" disabled selected>
                      Document Type
                    </option>
                      {helperStore.fileClass.map(option => (
                        <option key={option} value={option}>
                          {option}
                        </option>
                      ))}
                    </TextField>  
                  <TextField
                    id="attachement"
                    label="Choose Documents (Max 20MB)"
                    type="file"
                    accept="image/*"
                    onChange = {this.handleFilesUpload}
                    inputProps={{ multiple: true }}
                    className={classes.textField}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
                  </div>
                <div>
                {fileAdding.length>0 ? fileAdding.length == 12 ?
                  <div className={classes.marginTop}><LinearProgress color="secondary" />{fileAdding}</div>
                  : 
                  <div className={[classes.bgerror, classes.marginTop].join(' ')}>{fileAdding}</div>
                  : ''
                }
                <Grid className={classes.fileList}>
                  Materials:
                  <GenerateFileLinkEdit files={order.fileUrl} updateFileUrls = {this.handleUpdateFileUrls}/>
                </Grid>
              </div>
            </div>
           
          <div className={[classes.root, classes.marginTop].join(' ')}>
            <h5>6. Customer Note (Other Import Information Customer Told)</h5>
            <TextField
              id="description"
              fullWidth
              value={order.description||`
Course Reference Number:
Customer Saying: 

Schoole system login URL:
Login username/password: 
(We won't share or otherwise engage in unauthorized use of your inforamtion)

Type of references and the edition of reference style:  

Other Special Requirements: 
Readings or other materials that should be included int he essay: 
Required Structure for the assignment and the approximate word count for each part: 
              `}
              onChange={this.handleInputChange}
              label="Description"
              multiline
              fullWidth
              className={[classes.textField, classes.largeSpace, order.description? '':classes.grayText].join(' ')}
              rows={15}
            >
            
            </TextField>
            
          </div>
          <div className={[classes.fileList].join(' ')}>
            {this.props.cancleFunction ? <Button  variant="contained" onClick={this.props.cancleFunction} color="primary" className={classes.button}>Cancel</Button> : ""}
            <Button  variant="contained"
                     disabled = {ordersStore.submitOrderLoading}
                     onClick={this.handleCreateOrder}
                     color="secondary"
                     className={classes.button}
            >{this.props.formName ||"New Order"}</Button>
          </div>
            {ordersStore.submitOrderLoading ?
             <Loading />: null
            }
        </form>
        </Spin>
      </div>
    );
  }
} 

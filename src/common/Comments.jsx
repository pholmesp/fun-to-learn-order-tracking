import React, {Component} from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import moment from 'moment';
import momenttz from 'moment-timezone'

import {bggray, bgerror, gray, error, bglightyellow, bgyellow} from '../common/Colors'
import loading from '../util/loading'

const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: bglightyellow,
    borderBottom: '1px solid #e8e8e8',
  },
  bgpapper:{
    backgroundColor: theme.palette.background.paper,
  },
  button: {
    margin: theme.spacing.unit,
  },
  bggray:{
    backgroundColor: bgyellow,
    padding: theme.spacing.unit,
    fontSize: '16px'
  },
  bgerror:{
    backgroundColor: bgerror,
  },
  padding: {
    padding: theme.spacing.unit,
  },
  span:{
    fontSize: '12px',
    fontColor: gray,
  }, 
  spanred:{
    fontSize: '12px',
    fontColor: error,
    backgroundColor: bgerror,
  },
  comments:{
    backgroundColor: '#f4f4f4',
    marginBottom: theme.spacing.unit,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit * 2,
    // width: theme.breakpoints.values.sm,
  },
});

@withStyles(styles)
@inject('ordersStore')
@withRouter
@observer
export default class Comments extends Component{
  state = {
    mode: 'view',
  }

  handleCancleEdit = () => {
    this.setState({mode:"view"})
  }
  handleNewComment = () => {
    this.setState({mode:"new"})
  }
  
  render(){
    const {classes, comments, idorder} = this.props
    if(!comments) return ''
    return(
      <Grid container spacing={8} className={classes.comments}>
          {comments.map(comment => <Comment comment={comment} key={comment.idcommentsToOrder} />)}
          <Grid container item className={classes.root}>
            {this.state.mode === 'new'? 
              <AddComment idorder={idorder} cancleFunction={this.handleCancleEdit}/> 
              : 
              <Button color="secondary"  variant="outlined" className={classes.button} onClick={this.handleNewComment}>Add Comment</Button> }
          </Grid>
      </Grid>
    )
  }
}

@withStyles(styles)
@inject('helperStore', 'ordersStore', 'authStore', 'userStore')
@withRouter
@observer
class Comment extends Component{
 
  state = {
    mode: 'view'
  }
  handleCommentEdit = () => {
    this.setState({mode:"edit"})
  }
  handleCancleEdit = () => {
    this.setState({mode:"view"})
  }

  handleMakeVisible = () =>{
    let comment = Object.assign({}, this.props.comment)
    comment['status'] = 1
    this.props.ordersStore.updateOrderComment(comment).then(res=>{
      console.log(res)
    })
  }
  hendleDelete = () => {
    this.props.ordersStore.deleteOrderComment(this.props.comment).then(res=>{
      console.log(res)
    })
  }

  generateFileLink(filesObj){
    let files = []
    let filesArray = filesObj
    if (typeof(filesObj) === 'string'){
      filesArray = JSON.parse(filesObj)
    }
    filesArray = filesArray||[]
    for(let i=0; i<filesArray.length; i++){
      let file = filesArray[i]
      const size = file.size/1024 >= 1024? Math.round(file.size/(1024*1024)*100)/100 + ' MB': Math.round(file.size/1024*100)/100+' KB'
      files.push(<li key={i}><a href={this.props.helperStore.ROOT_API_URL + '/img/' + file.filename} key={i}>{file.originalname} - <span className={this.props.classes.span}>{size}</span></a></li>)
    }
    return files
  }
  render(){
    const {classes, comment, userStore, authStore} = this.props

    if(this.state.mode === 'edit') return(<AddComment comment={comment} cancleFunction={this.handleCancleEdit}/>)

    let user = userStore.getUserById(comment.commentBy)
    let currentUser = authStore.currentUser
    let isoCreateDate = moment(comment.dateTime, moment.ISO_8601).tz(momenttz.tz.guess())
    let localCreatedate = isoCreateDate.format("YYYY-MM-DD HH:mm:ss")
    console.log(currentUser, user)

    const displayComment = <Grid container item xs={12} className={classes.root} >
        <Grid item className={classes.padding} xs={2}>
          <Typography>
            {user.name} ({user.role.replace('|', ',')}) <br/> <span className={classes.span}>{localCreatedate}</span> &nbsp; &nbsp; &nbsp; &nbsp; 
            {currentUser.role.indexOf('admin') >=0 && comment.status===0 ? <span className={classes.spanred}> - Hided for other users</span>: ''}
            </Typography>
          
        </Grid>
        <Grid item className={classes.padding} xs={8}>
          <Typography  className={classes.padding}>
            {comment.content}
          </Typography>
          { (comment.fileUrl||'').length>3 ?
            <Typography className={classes.padding}>
              Attachements:
              {this.generateFileLink(comment.fileUrl)}
            </Typography>
            : ''
          }
        </Grid>
        {currentUser.role.indexOf('admin') >=0 || currentUser.iduser===comment.commentBy ? 
          <Grid container item xs={2} justify="flex-end">
              {comment.status===0 && currentUser.role.indexOf('admin') >=0 ? <Button size="small" color="secondary" onClick={this.handleMakeVisible} >Make Visible</Button> : ''}
              <Button size="small" color="secondary"  onClick={this.handleCommentEdit}>
                Edit
              </Button>
              <Button size="small" color="secondary" onClick={this.hendleDelete}>
                Delete
              </Button>
          </Grid>: ''}
      </Grid>

    if(comment.status===0){
      if(currentUser.role.indexOf('admin') >=0 ) return (displayComment)
      if(currentUser.iduser===comment.commentBy) return (displayComment)
      return ('')
    }else{
      return (displayComment)
    } 
  }     
}

@withStyles(styles)
@inject('helperStore', 'ordersStore', 'authStore')
@withRouter
@observer
class AddComment extends Component{

  state = {
    comment: this.props.comment || {content:"", order_idorder: this.props.idorder, commentBy: this.props.authStore.currentUser.iduser, status: 0, fileUrl:[]},
    contentValidation: ""
  }

  handleInputChange = (event) =>{
    console.log(event.target)
    let contentValidation = this.state.contentValidation
    if(event.target.value.trim().length===0){
      contentValidation = "Comment can not be empty"
    }else{
      contentValidation = "" 
    }
    let commentUpdate = this.state.comment
    commentUpdate[event.target.id] = event.target.value
    this.setState({
      comment: commentUpdate,
      contentValidation: contentValidation
    })
  }

  handleFilesUpload = (event) => {
    const files = Array.from(event.target.files)
    const formData = new FormData()
    files.forEach((file) => {
      formData.append('file', file)
    })
    this.props.helperStore.uploadFiles(formData).then(res =>{
      let orginalComment = this.state.comment
      orginalComment['fileUrl'] = res
      this.setState({comment: orginalComment})
    })
  }

  validateField(){
      // console.log(this.state.order[key], key)
      let validateStatus = false
      if(''+this.state.comment.content.trim().length===0){
        this.setState({
          contentValidation: "Comment can not be empty"
        })
        validateStatus = true
      }
    return validateStatus
  }
  utcDatetime(dt, tz){
    return moment.tz(dt,tz).format()
  }

  handleCreateOrder = () => {
    if(!this.validateField()){
      let comment = Object.assign({}, this.state.comment)
      if(typeof(comment.fileUrl)=='object') comment.fileUrl = JSON.stringify(comment.fileUrl)
      comment['dateTime']=moment.tz().format('YYYY-MM-DDTHH:mm:ss')
  
      this.props.ordersStore.updateOrderComment(comment).then(res =>{
        console.log(res.statusCode, JSON.stringify(res))
        if(res.statusCode == 200){
          this.props.cancleFunction()
        }else{
          this.setState({
            error: JSON.stringify(res)
          })
        }
      })
    }
  }

  generateFileLink(filesObj){
    let files = []
    let filesArray = filesObj
    if (typeof(filesObj) === 'string'){
      filesArray = JSON.parse(filesObj)
    }
    filesArray = filesArray||[]
    for(let i=0; i<filesArray.length; i++){
      let file = filesArray[i]
      const size = file.size/1024 >= 1024? Math.round(file.size/(1024*1024)*100)/100 + ' MB': Math.round(file.size/1024*100)/100+' KB'
      files.push(<li key={i}><a href={this.props.helperStore.ROOT_API_URL + '/img/' + file.filename} key={i}>{file.originalname} - <span className={this.props.classes.span}>{size}</span></a></li>)
    }
    return files
  }
  render(){
    const {classes, ordersStore} = this.props
    if(ordersStore.isLoading) return(loading)

    return(
      <Grid container className={[classes.root, classes.bgpapper].join(' ')} justify="center">
        <Grid item xs={12} className={classes.bgerror}>{this.state.error}</Grid>
        <Grid item xs={12}>
          <TextField
            id="content"
            value={this.state.comment.content}
            onChange={this.handleInputChange}
            label="Comment"
            multiline
            fullWidth
            className={classes.textField}
            rows={10}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <TextField
            id="attachement"
            label="Support Documents - Select Multipal supported files"
            type="file"
            onChange = {this.handleFilesUpload}
            fullWidth
            inputProps={{ multiple: true }}
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
          />
          </Grid>
        <Grid item xs={12} className={classes.fileList}>
          {this.generateFileLink(this.state.comment.fileUrl)}
        </Grid>
        <Grid item xs={4}>
          <Button  size="small" variant="contained" className={classes.button} onClick={this.props.cancleFunction} color="primary">Cancel</Button> 
          <Button  size="small" variant="contained" className={classes.button} onClick={this.handleCreateOrder} color="secondary">{this.props.formName ||"Add Comment"}</Button>
        </Grid>
      </Grid>
    )
  }     
}

import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import FolderOpen from '@material-ui/icons/FolderOpen';
import ArrowBack from '@material-ui/icons/ArrowBack';
import moment from 'moment'


const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit * 2,
    // width: theme.breakpoints.values.sm,
  },
  tabsRoot: {
    width: '100%',
    borderBottom: '1px solid #e8e8e8',
  },
  paddingTop: {
    paddingTop: theme.spacing.unit*2,
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  label: {
    padding: '4px',
    paddingLeft: '8px',
    paddingRight: '8px',
    marginRight: '8px',
    marginTop: '5px',
    borderRadius: 6,
    fontSize: '12px',
    textDecoration: 'None',
  },
});

@withStyles(styles)
@inject('priceRequestStore', 'helperStore', 'userStore', 'authStore')
@withRouter
@observer
export default class PriceRequestNew extends Component {
  state = {
    request: this.props.request||{"name":"", "description":"",  "major":""},
    requestValidate:  {"name":"",  "major":""},
  };

  handleInputChange = (event) => {
    console.log(event.target)
    let updatingOrderValidation = this.state.requestValidate
    if(event.target.value.trim().length===0){
      updatingOrderValidation[event.target.id] = event.target.id + ' is required'
    }else{
      updatingOrderValidation[event.target.id] = "" 
    }
    let updatingOrder = this.state.request
    updatingOrder[event.target.id] = event.target.value
    this.setState({
      request: updatingOrder,
      requestValidate: updatingOrderValidation
    })
  }

  handleCancel = () => {
    if(this.props.cancelFuntion) this.props.cancelFuntion()
    else this.props.history.goBack()
  }

  handleFilesUpload = (event) => {
    const files = Array.from(event.target.files)
    const formData = new FormData()
    files.forEach((file) => {
      formData.append('file', file)
    })
    this.props.helperStore.uploadFiles(formData).then(res =>{
      let orginalOrder = this.state.request
      orginalOrder['fileUrl'] = res
      this.setState({request: orginalOrder})
    })
  }

  validateField(){
    let updatingrequestValidate = this.state.requestValidate
    let validateStatus = false
    for(let key in updatingrequestValidate){
      console.log(this.state.request[key], key)
      if((''+this.state.request[key]).trim().length===0){
        updatingrequestValidate[key] = key + ' is required'
        validateStatus = true
      }
    }
    this.setState({
      requestValidate: updatingrequestValidate
    })
    console.log()
    return validateStatus
  }
  utcDatetime(dt, tz){
    // console.log(dt, tz)
    return moment.tz(dt,tz).format()
  }

  handleCreatePriceRequest = () => {
    if(!this.validateField()){
      let order = Object.assign({},this.state.request)
      if(typeof(order.fileUrl)=='object') order.fileUrl = JSON.stringify(order.fileUrl)
      if(!order.idpriceRequest) order.idUser = this.props.authStore.currentUser.iduser
      if(!order.idpriceRequest) order.status='requesting'  //confirming - requesting - complete
      if(!order.idpriceRequest) order.dateTime=moment.tz().format('YYYY-MM-DDTHH:mm:ss')
      this.props.priceRequestStore.updatePriceRequest(order).then(res =>{
        if(res.statusCode == 200){
          if(this.props.cancleFunction) this.props.cancleFunction()
          // this.props.history.push(`/packages/${res.insertId}`)
          this.props.history.goBack()
        }else{
          this.setState({
            error: JSON.stringify(res)
          })
        }
      })
      this.setState({mode:'view'})
    }
  }

  generateFileLink(filesObj){
    let files = []
    let filesArray = filesObj
    if (typeof(filesObj) === 'string'){
      filesArray = JSON.parse(filesObj)
    }
    filesArray = filesArray||[]
    for(let i=0; i<filesArray.length; i++){
      let file = filesArray[i]
      const size = file.size/1024 >= 1024? Math.round(file.size/(1024*1024)*100)/100 + ' MB': Math.round(file.size/1024*100)/100+' KB'
      files.push(<li key={i}><a href={this.props.helperStore.ROOT_API_URL + '/img/' + file.filename} key={i}>{file.originalname} - <span className={this.props.classes.span}>{size}</span></a></li>)
    }
    return files
  }

  render(){
    const { classes, helperStore } = this.props;
    const { request } = this.state;

    return(
      <div className={classes.root}>
        <h3>
          <IconButton>
              <ArrowBack onClick={() => this.handleViewChange('view')} />
          </IconButton>
          &nbsp;
          <FolderOpen/> { this.props.formName || "Create a New Price Quote Request"}
        </h3>
        <Grid className={classes.bgerror}>{this.state.error}</Grid>
        <form className={classes.container}>
          <TextField
            id="name"
            required
            fullWidth
            value={request.name}
            onChange={this.handleInputChange}
            label="Price Request Name"
            error ={this.state.requestValidate.name.length === 0 ? false : true }
            helperText={this.state.requestValidate.name}
            className={classes.textField}
            fullWidth
            InputLabelProps={{
              shrink: true,
            }}
          />
          {/* <div>
            <TextField
              id="currency"
              required
              select
              value={request.currency}
              label = "Currency"
              onChange={this.handleInputChange}
              error ={this.state.requestValidate.currency.length === 0 ? false : true }
              helperText={this.state.requestValidate.currency}
              className={classes.textField}
              InputLabelProps={{
                shrink: true,
              }}
              SelectProps={{
                native: true,
                MenuProps: {
                  className: classes.menu,
                },
              }}
            >
              {helperStore.currencyArray.map(option => (
                <option key={option.idcurrency} value={option.idcurrency}>
                  {option.currency}
                </option>
              ))}
            </TextField>   
            <TextField
              id="finnalPrice"
              required
              value={request.salePrice}
              onChange={this.handleInputChange}
              label="Sale Price"
              error ={this.state.requestValidate.finallPrice.length === 0 ? false : true }
              helperText={this.state.requestValidate.finnalPrice}
              className={classes.textField}
              InputLabelProps={{
                shrink: true,
              }}
            />             
          </div> */}
            <TextField
            id="major"
            required
            onChange={this.handleInputChange}
            defaultValue={request.major}
            label="Customer Email"
            error ={this.state.requestValidate.major.length === 0 ? false : true }
            helperText={this.state.requestValidate.major}
            className={classes.textField}
            fullWidth
            InputLabelProps={{
              shrink: true,
            }}
          />

          <TextField
            id="fileUrl"
            label="Support Documents - Select Multipal supported files"
            type="file"
            onChange = {this.handleFilesUpload}
            fullWidth
            inputProps={{ multiple: true }}
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <Grid xs={12} className={classes.fileList}>
            {this.generateFileLink(request.fileUrl)}
          </Grid>

          <TextField
            id="description"
            value={request.description}
            onChange={this.handleInputChange}
            label="Description"
            multiline
            fullWidth
            className={classes.textField}
            rows={10}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <Button  variant="contained" onClick={ this.handleCancel } className={classes.button} color="primary">Cancel</Button>
          <Button  variant="contained" onClick={this.handleCreatePriceRequest} className={classes.button} color="secondary">{this.props.formName ||"Confirm"}</Button>
      </form>
    </div>
    );
  }
} 


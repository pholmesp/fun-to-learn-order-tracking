import React, {Component} from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import {Modal, Input} from "antd";
import IdleTimer from "react-idle-timer";
import Button from "@material-ui/core/Button";



import TextField from '@material-ui/core/TextField';

import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


export default class AFK extends Component {
    constructor(props){
        super(props)
        this.state={
            value: "",

        }
    }
    handleAFKInput = (event)=>{
        this.setState({
            value: event.target.value
        })
    }

    render() {
        const { visible, handleSubmit, error, text } = this.props;
        return (
            <div>

                <Dialog open={visible}
                        aria-labelledby="form-dialog-title"
                         style={{backgroundColor: "black"}}

                >
                    <DialogTitle id="form-dialog-title">Password</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Seems AFK? To continue using the system, please type in your password.
                        </DialogContentText>
                        <form autocomplete="off">
                        <TextField
                            error={error}
                            autoFocus
                            margin="dense"
                            id="password"
                            label="Password"
                            type="password"
                            helperText={text}
                            value={this.state.value}
                            onChange={this.handleAFKInput}
                            fullWidth
                        />

                        </form>
                    </DialogContent>
                    <DialogActions>

                        <Button onClick={()=>handleSubmit(this.state.value)} color="primary">
                            Submit
                        </Button>
                    </DialogActions>
                </Dialog>
                {/*<Modal*/}
                {/*    maskStyle={{ backgroundColor: 'black'}}*/}

                {/*    visible={visible}*/}
                {/*    title="Title"*/}
                {/*    onOk={this.handleOk}*/}
                {/*    footer={[*/}
                {/*        <Button key="submit" type="primary"  onClick={this.handleOk}>*/}
                {/*            Submit*/}
                {/*        </Button>,*/}
                {/*    ]}*/}
                {/*>*/}
                {/*    <p>Some contents...</p>*/}
                {/*    <p>Some contents...</p>*/}
                {/*    <p>Some contents...</p>*/}
                {/*    <p>Some contents...</p>*/}
                {/*    <p>Some contents...</p>*/}
                {/*</Modal>*/}
            </div>
        );
    }
}
import Orders from './Orders'
import NewOrder from './NewOrder'
import OrderDetail from './OrderDetail'

import Courses from './Courses'
import CourseDetail from './CourseDetail'
import NewCourse from './NewCourse'

import NewPackage from './NewPackage'
import Packages from './Packages'
import PackageDetail from './PackageDetail'

import AssignFuncBar from './AssignFuncBar'
import SaleFuncBar from './SaleFuncBar'

import Statistic from './Statistic'

export {
    Orders, NewOrder, OrderDetail, Courses, NewCourse, CourseDetail, NewPackage, Packages, PackageDetail, AssignFuncBar, SaleFuncBar, Statistic
}
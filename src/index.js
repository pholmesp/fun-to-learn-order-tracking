import React from "react";
import ReactDOM from "react-dom";
import { HashRouter } from 'react-router-dom';
import articlesStore from './stores/articlesStore';
import commentsStore from './stores/commentsStore';
import authStore from './stores/authStore';
import commonStore from './stores/commonStore';
import editorStore from './stores/editorStore';
import userStore from './stores/userStore';
import ordersStore from './stores/ordersStore';
import courseStore from './stores/courseStore';
import helperStore from './stores/helperStore';
import customerStore from './stores/customerStore';
import priceRequestStore from './stores/priceRequestStore';
import exchangeRequestStore from './stores/exchangeRequestStore';
import exchangeReserveStore from './stores/exchangeReserveStore';
import statisticStore from './stores/statisticStore';
import qualityStore from './stores/qualityStore';
import orderResultStore from './stores/orderResultStore'
import utilStore from './stores/utilStore'

import "./index.css";
import App from "./App";
import Dashboard from "./Dashboard";
import registerServiceWorker from "./registerServiceWorker";

import { Provider } from "mobx-react";

const stores = {
  // articlesStore,
  ordersStore,
  courseStore,
  // commentsStore,
  authStore,
  priceRequestStore,
  customerStore,
  userStore,
  helperStore,
  statisticStore,
  exchangeRequestStore,
  exchangeReserveStore,
  qualityStore,
  orderResultStore,
  utilStore
};

let init = () => {
  for(let key in stores){
    stores[key].init()
  }
}

const Root = (
  <Provider  { ...stores}>
    <HashRouter>
      <div>
        {init()}
        <App />
      </div>
    </HashRouter>
  </Provider>
);

ReactDOM.render(Root, document.getElementById("root"));
registerServiceWorker();

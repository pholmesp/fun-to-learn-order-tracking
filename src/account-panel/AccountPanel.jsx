import React, { Component } from 'react';
import { NavLink } from "react-router-dom";
import {Icon} from 'antd';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Collapse from '@material-ui/core/Collapse'
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

import cyan from '@material-ui/core/colors/cyan';

import AssignmentInd from '@material-ui/icons/AssignmentInd';
import PeopleIcon from '@material-ui/icons/People';
import NaturePeople from '@material-ui/icons/NaturePeople'

const styles = theme => ({
  active: {
    backgroundColor: cyan[50],
  },
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing.unit * 3,
  },
});

@withStyles(styles)
export default class AccountPanel extends Component{
  state = {
    open: true,
  };

  handleClick = () => {
    this.setState(state => ({ open: !state.open }));
  };
  render(){
    const { classes } = this.props;
      return(
        <List>
          <ListItem button onClick={this.handleClick}>
            <ListItemIcon>
              <PeopleIcon />
            </ListItemIcon>
            <ListItemText inset primary="Account" />
            {this.state.open ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
      
          <Collapse in={this.state.open} timeout="auto" unmountOnExit>
            <List component="div">
                <ListItem button className={classes.nested} component={NavLink} to="/account" exact activeClassName={classes.active}>
                  <ListItemIcon>
                    <AssignmentInd/>
                  </ListItemIcon>
                  <ListItemText primary="Account" />
                </ListItem>
              <ListItem button className={classes.nested} component={NavLink} to="/SalesWechat" exact activeClassName={classes.active}>
                <ListItemIcon>
                  <Icon type="wechat" style={{ fontSize: '24px' }}/>
                </ListItemIcon>
                <ListItemText primary="SalesWechat" />
              </ListItem>
              <ListItem button className={classes.nested} component={NavLink} to="/UniversityManagement" exact activeClassName={classes.active}>
                <ListItemIcon>
                  <Icon type="bank" style={{ fontSize: '24px' }}/>
                </ListItemIcon>
                <ListItemText primary="University" />
              </ListItem>
                <ListItem button className={classes.nested} component={NavLink} to="/customer" exact activeClassName={classes.active}>
                  <ListItemIcon>
                    <NaturePeople />
                  </ListItemIcon>
                  <ListItemText primary="Customer" />
                </ListItem>
            </List>
          </Collapse>
          <Divider />
        </List>
    );
  }
} 

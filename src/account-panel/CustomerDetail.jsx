import React, { Component } from 'react';
import { NavLink } from "react-router-dom";
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';

import ArrowBack from '@material-ui/icons/ArrowBack';
import { gray } from '@material-ui/core/colors';
import {PermissibleRender} from '@brainhubeu/react-permissible';
import { bgsuccess, lightBlue } from '../common/Colors'
import OrdersFiltered from '../common/OrdersFiltered';
import { GenerateSimpleLabel } from '../util/generateLabel';


  
  const styles = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      overflowX: 'auto',
      backgroundColor: theme.palette.background.paper,
    },
    table: {
      minWidth: 700,
    },
    row: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
      },
    },
    button: {
      margin: theme.spacing.unit,
    },
    rightIcon: {
      marginLeft: theme.spacing.unit,
    },
    input: {
      display: 'none',
    },
    textField: {
      marginLeft: 8,
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    search:{
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
      width: 400,
    }
  });
  
@withStyles(styles)
@inject('customerStore', 'userStore', 'helperStore', 'authStore', 'ordersStore')
@withRouter
@observer
export default class CustomerDetail extends Component {
  state = {
    currentStatus: 'America',
    country: this.props.helperStore.countries.filter(country => country !=='India').sort(),
    search: '', 
    page: 0, 
    rowsPerPage: 15,
  }
  handleChange = (event, currentStatus) => {
    this.setState({ currentStatus: currentStatus });
  };
  handleInputChange = event => {
    const {id, value} = event.target
    this.setState({[id]: value})
}
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  render() {
    let customers =  []
    const { classes, customerStore, userStore, authStore, ordersStore } = this.props;
    const { currentStatus, page, rowsPerPage} = this.state
    if(authStore.currentUser.role.indexOf('admin')>=0 || authStore.currentUser.role.indexOf('services')>=0) customers =customerStore.Customers
    else customers = customerStore.getSalerCustomer(authStore.currentUser.iduser)
    
    let customer = customers.filter(ele => ele.idcustomer == this.props.match.params.idcustomer)[0]||{}
    // console.log(this.props.match.params.idcustomer, customer, customers)
    let customerOrders = ordersStore.getOrdersByCustomerId(this.props.match.params.idcustomer)

    return (
        <div className={classes.root}>
          <Grid container justify={'space-between'} >
            {/* <Grid item className={classes.search}>
              <TextField 
                id="search"
                placeholder="Search Orders" 
                className={classes.textField} 
                onChange={this.handleInputChange}
                value={this.state.search}
              />
              <IconButton className={classes.iconButton} aria-label="Search">
                <SearchIcon />
              </IconButton>
            </Grid> */}
            <Grid item>
              <Button variant="outlined" color="secondary" size="small" onClick={this.props.history.goBack}>
                <ArrowBack /> &nbsp; Go Back 
              </Button>
            </Grid>
            <Grid item>
              <Button variant="contained" color="primary" className={classes.button} component={NavLink} to="/customer/edit" exact activeClassName={classes.active}>
                Add Customer
                <Icon className={classes.rightIcon}>add_circle</Icon>
              </Button>
            </Grid>
          </Grid>

          <Table className={classes.table} size="small">
            <TableHead>
              <TableRow>
                <TableCell padding='dense'># </TableCell>
                <TableCell padding='dense'>Name</TableCell>
                <TableCell padding='dense'>History No.</TableCell>
                <TableCell padding='dense'>Country</TableCell>
                <TableCell padding='dense'>University/URL</TableCell>
                <TableCell padding='dense'>Level</TableCell>
                <TableCell padding='dense'>English Level</TableCell>
                <TableCell padding='dense'>Email</TableCell>
                <TableCell padding='dense'>SaleWechat</TableCell>
                <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm> 
                  <TableCell padding='dense'>Customer Wechat</TableCell>
                </PermissibleRender>
                <TableCell padding='dense'>Labels</TableCell>
                <TableCell padding='dense'>Operate</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow className={classes.row} key={customer.idcustomer}>
                <TableCell padding='dense'>{customer.idcustomer}</TableCell>
                <TableCell padding='dense'>{customer.customerName}</TableCell>
                <TableCell padding='dense'>{customer.code}</TableCell>
                <TableCell padding='dense'>{customer.country}</TableCell>
                <TableCell padding='dense'>{customer.university}<p>{customer.universityUrl}</p></TableCell>
                <TableCell padding='dense'>{customer.level}</TableCell>
                <TableCell padding='dense'>{customer.englishLevel}</TableCell>
                <TableCell padding='dense'>{customer.customerEmail}</TableCell>
                <TableCell padding='dense'>{customer.wechatName}</TableCell>
                <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm> 
                  <TableCell padding='dense'>{customer.customerWechat}</TableCell>
                </PermissibleRender>
                <TableCell padding='dense'> <GenerateSimpleLabel labels={customer.labels} /> </TableCell>
                <TableCell padding='dense'><Button  onClick={()=>{customerStore.getSaleInfo(customer.wechat)}} className={classes.button} component={NavLink} to={"/customer/edit/"+customer.idcustomer} exact activeClassName={classes.active}>
                  Edit
                </Button>
                </TableCell>
              </TableRow>
              <TableRow >
                <TableCell colSpan={authStore.currentUser.role.indexOf('admin')>=0? 12 : 11}>
                  <h5>Customer Note:</h5>
                  <pre style={{whiteSpace: 'pre-wrap'}}>{customer.description}</pre>
                </TableCell>
              </TableRow>

            </TableBody>
          </Table>

          
          <OrdersFiltered orders={customerOrders} />
        </div>
      )
    }
  }
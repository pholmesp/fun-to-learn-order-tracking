import React from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider'
import Typography from '@material-ui/core/Typography'
import {PermissibleRender} from '@brainhubeu/react-permissible';
import agent from '../agent'

import moment from 'moment';
import {GenerateSimpleLabelEdit} from '../util/generateLabel'
import {AutoCompleteUniversity} from '../common/AutoComplete'
import loading from "../common/loading";

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit,

  },
  dense: {
    marginTop: 16,
  },
  menu: {
    width: 200,
  },
  button: {
    margin: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  margin: {
    marginTop: theme.spacing.unit * 3,
    margin: theme.spacing.unit,
  },
  marginRight:{
    marginRight: theme.spacing.unit*3,
  },
  input: {
    display: 'flex',
    padding: 0,
  },
  placeholder: {
    position: 'absolute',
    left: 2,
    fontSize: 16,
  },
});


const customerLevels = [
    { label: 'High' }, 
    { label: 'Medium' },
    { label: 'Low' },
];

@withStyles(styles)
@inject('customerStore', 'helperStore', 'authStore', 'userStore')
@withRouter
@observer

export default class EditCustomer extends React.Component {

    state = {
        isCreating: this.props.match.params.idcustomer ? false : true,
        // wechat: "",
        labels: "",
        customer: this.props.match.params.idcustomer ? this.props.customerStore.getCustomerById(this.props.match.params.idcustomer) 
                                                : { code: "", country: "", creator_iduser:"", customerEmail: "", customerLevel: "", customerName: "", dateTime: "", description: "", englishLevel: "", labels: "", level: "", university: "", universityUrl: "", wechat: ""}, 
        customerVerify: { country: "",  customerEmail: "", customerName: "",  wechat: ""}
    }
    
    zeroPad(num, places) {
        var zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    }

    handleChange = event => {
        const {id, value} = event.target
        this.setState(prevState => {
            return {
                customer: {
                    ...prevState.customer,
                    [id]: value
                }
            }
        })
        // Generate code
        // if(id=='customerName'){
        //     let code = value.substring(0,3).toLowerCase()+this.zeroPad(Math.round(Math.random()*999),3)
  
        //     while(this.props.customerStore.getCustomerByCode(code).code) code = value.substring(0,3).toLowerCase()+this.zeroPad(Math.round(Math.random()*999),3)
        //     this.setState(prevState => {
        //         return {
        //             customer: {
        //                 ...prevState.customer,
        //                 code: code
        //             }
        //         }
        //     })
        // }
    }

    handleSelectInputChange = (selectObj) => {
      let updatingCustomer = this.state.customer
      if(selectObj){
        updatingCustomer['university'] = selectObj.value
        this.setState({
          customer: updatingCustomer,
        })
      }else{
        updatingCustomer['university'] = ""
        this.setState({
          customer: updatingCustomer,
        })
      }
       
    }
      

    handleSelectChange = event => {
        const {name, value} = event.target;
        this.props.helperStore.getCountryUni(value.toLowerCase())
        this.setState(prevState => {
            return {
                customer: {
                    ...prevState.customer,
                    [name]: value
                }
            }
        })
      }

    handleInputChange = event => {
        const {id, value} = event.target
        this.setState({[id]: value})
    }

    handleAddLabel(labelName) {
        let updatingCustomer = this.state.customer
        let updatingLabels = this.state.customer[labelName]
        if(typeof(updatingLabels) == 'string') updatingLabels = updatingLabels.split('|')
        console.log(updatingLabels, typeof(updatingLabels))
        updatingLabels = updatingLabels || []
        if(!this.state[labelName]) {
            this.setState({
                [labelName+'Validate']: labelName+" can not be empty"
            })
            return 
        }
        updatingLabels.push(this.state[labelName])
        console.log(updatingLabels)
        updatingCustomer[labelName] = updatingLabels.join('|')
        this.setState({
            customer: updatingCustomer, 
            [labelName]: "", 
            [labelName+'Validate']: "",
        })
    }

    handleLabelDelete = newLabels =>{
        let updatingCustomer = this.state.customer
        updatingCustomer['labels'] = newLabels
        this.setState({
            customer: updatingCustomer
        })
    }

    validateField(){
      let customerVerify = this.state.customerVerify
      let validateStatus = false
      for(let key in customerVerify){
        if((''+this.state.customer[key]).trim().length===0){
          customerVerify[key] = key + ' is required'
          validateStatus = true
        }else{
          customerVerify[key] = ""
        }
      }
      this.setState({
        customerVerify: customerVerify,
      })
      return validateStatus
    }

    handleSaveButtonClick = () => {
      console.log(this.validateField())
      if(this.validateField()){
        return 
      }
      if(!this.props.match.params.idcustomer){
        for(let ele of this.props.customerStore.Customers){
          if(ele.customerName.replace(/ /g, '') === this.state.customer.customerName.replace(/ /g, '')){
            alert("Customer name " + this.state.customer.customerName + ' already exist. ')
            return
          }
          if(ele.customerEmail.replace(/ /g, '') === this.state.customer.customerEmail.replace(/ /g, '')){
            alert("Customer email " + this.state.customer.customerEmail + ' already exist. ')
            return
          }
        }
      }
      let updateCustomer = Object.assign({}, this.state.customer)
      let createTime = moment().format('YYYY-MM-DDTHH:mm:ss')
      updateCustomer['creator_iduser'] ? '' : updateCustomer['creator_iduser'] = this.props.authStore.currentUser.iduser
      updateCustomer['dateTime'] ? '': updateCustomer['dateTime'] = createTime
      console.log(this.state.customer)
        if(this.state.customer.wechat){
            // let idWechat = this.state.customer.wechat.split('(')[1].replace(')', "")
            // agent.Account.getIdWeChat(idWechat).then(res => {

                // if(res.statusCode ===200){
                //     console.log(res[0].id)
                //     updateCustomer['wechat'] = res[0].id
                    delete updateCustomer['wechatName']
                    console.log(updateCustomer['wechat'])
                    if(!this.props.match.params.idcustomer) updateCustomer['code'] = 0
                    if(typeof(updateCustomer.labels) === 'object' && updateCustomer.labels) updateCustomer['labels'] = updateCustomer.labels.join('|')
                    console.log(updateCustomer['wechat'])
                    this.props.customerStore.updateCustomer(updateCustomer).then(res => {
                        if(res.statusCode === 200){
                            this.props.history.goBack()
                        }
                    })
                // }
            // })
        }

        else alert ("Please select a Wechat !")



    }


  render() {
    const { classes, authStore, userStore, helperStore, customerStore} = this.props
    let customer = this.state.customer
    const university = customer.country ? helperStore.countryuni :  helperStore.universities
      if(customerStore.isLoading) return loading
// console.log(customer)
// console.log(authStore.currentUser)
    return (
      <form className={classes.container} noValidate autoComplete="off">
        <Grid container>
            
            <TextField
                id="customerName"
                label="Name"   
                required
                className={classes.textField}
                value={customer.customerName}
                onChange={this.handleChange}
                error={this.state.customerVerify.customerName.length === 0? false : true}
                helperText={this.state.customerVerify.customerName}
                margin="normal"
                // variant="outlined"
            />
            <TextField
                id="customerEmail"
                label="Email"
                required
                className={classes.textField}
                value={customer.customerEmail}
                onChange={this.handleChange}
                error={this.state.customerVerify.customerEmail.length === 0? false : true}
                helperText={this.state.customerVerify.customerEmail}
                margin="normal"
            />
        </Grid>
        <Grid container>
            <TextField
                id="customerLevel"
                select
                label="Customer Level"
                className={classes.textField}
                value={customer.customerLevel||""}
                onChange={this.handleChange}
                InputLabelProps={{
                    shrink: true,
                }}
                SelectProps={{
                    native: true,
                    MenuProps: {className: classes.menu},
                }}
                margin="normal"
                >
                <option value="" disabled>
                Customer Level
                </option>
                {customerLevels.map(option => (
                    <option key={option.label} value={option.label}>
                        {option.label}
                    </option>
                ))}
            </TextField>
            <TextField
                id="level"
                select
                label="Degree Level"
                className={classes.textField}
                value={customer.level||""}
                onChange={this.handleChange}
                InputLabelProps={{
                    shrink: true,
                }}
                SelectProps={{
                    native: true,
                    MenuProps: {className: classes.menu},
                }}
                margin="normal"
                >
                <option value="" disabled>
                Customer Degree
                </option>
                {helperStore.degreeLevels.map(option => (
                    <option key={option} value={option}>
                        {option}
                    </option>
                ))}
            </TextField>
            <TextField
                id="englishLevel"
                select
                label="English Level"
                className={classes.textField}
                value={customer.englishLevel||""}
                onChange={this.handleChange}
                InputLabelProps={{
                    shrink: true,
                }}
                SelectProps={{
                    native: true,
                    MenuProps: {className: classes.menu},
                }}
                margin="normal"
                >
                <option value="" disabled>
                Customer English
                </option>
                {helperStore.englishLevels.map(option => (
                    <option key={option} value={option}>
                        {option}
                    </option>
                ))}
            </TextField>
            <TextField
                id="wechat"
                label="Wechat"
                className={classes.textField}
                required
                select
                value={customer.wechat||""}
                onChange={this.handleChange}
                error={this.state.customerVerify.wechat.length === 0? false : true}
                helperText={this.state.customerVerify.wechat}
                margin="normal"
                InputLabelProps={{
                    shrink: true,
                }}
                SelectProps={{
                    native: true,
                    MenuProps: {
                    className: classes.menu,
                    },
                }}
            >
                <option value="" disabled>
                    Select Wechat
                </option>
                { customer.creator_iduser ?
                    Object.keys(JSON.parse(authStore.currentUser.wechatName)).map(key => (
                        <option key={key} value={key}>
                            {JSON.parse(authStore.currentUser.wechatName)[key]}
                        </option>
                    ))
                :
                Object.keys(JSON.parse(authStore.currentUser.wechatName)).map(key => (
                    <option key={key} value={key}>
                    {JSON.parse(authStore.currentUser.wechatName)[key]}
                    </option>
                    ))
            }
            </TextField>

            <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>      
              <TextField
                  id="customerWechat"
                  label="Customer Wechat"
                  className={classes.textField}
                  value={customer.customerWechat}
                  onChange={this.handleChange}
                  margin="normal"
                  InputLabelProps={{
                    shrink: true,
                }}
              />
            </PermissibleRender>
        </Grid>
        <Grid container>
            <Grid item lg={2} sm={12}>
              <TextField
                  name="country"
                  select
                  fullWidth
                  label="Country"
                  className={classes.textField}
                  value={customer.country}
                  onChange={this.handleSelectChange}
                  error={this.state.customerVerify.country.length === 0? false : true}
                  helperText={this.state.customerVerify.country}
                  InputLabelProps={{
                      shrink: true,
                  }}
                  SelectProps={{
                      native: true,
                      MenuProps: {
                        className: classes.menu,
                      },
                    }}
                  margin="normal"
              >
                  <option value="" disabled selected>
                      Select Country
                  </option>
                  {this.props.helperStore.countries.map(element => (
                      <option key={element} value={element}>
                          {element}
                      </option>
                  ))}
              </TextField>
            </Grid>
            <Grid item lg={4} sm={12}>
              <AutoCompleteUniversity placeholderText="Type to search university  ..."  defaultValue={{label: customer.university, value: customer.university}} options={university} onChangeCallback={this.handleSelectInputChange}/>
            </Grid>
            <Grid item lg={3} sm={12}>
                <TextField
                    id="universityUrl"
                    label="University URL"
                    fullWidth
                    className={classes.textField}
                    value={customer.universityUrl}
                    onChange={this.handleChange}
                    error ={false }
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
            </Grid>
        </Grid>
        <Grid container>
            <TextField
                id="labels"
                label="Labels"
                style={{ margin: 8 }}
                value={this.state.labels}
                onChange={this.handleInputChange}
                error={this.state.labelsValidate?true:false}
                helperText={this.state.labelsValidate}
                margin="normal"
                InputLabelProps={{
                    shrink: true,
                }}
            />
            <Button size="small" onClick={() =>this.handleAddLabel('labels')} color="primary" className={classes.margin}>Add Label</Button>
            <Grid item xs={12} className={classes.marginRight}>
                <Typography variant="caption" gutterBottom>
                &nbsp; &nbsp; Labels: <GenerateSimpleLabelEdit labels={customer.labels} updateLabel={this.handleLabelDelete}/> 
                {/* {this.generateLabels(customer.labels, 'labels')} */}
                </Typography>
            </Grid>
        </Grid>
        <Grid container>
            
        </Grid>
        <Grid container>
            <TextField
                id="description"
                label="Description"
                multiline
                fullWidth
                rows="6"
                value={customer.description}
                helperText="Please write some notes here."
                className={classes.textField}
                onChange={this.handleChange}
                margin="normal"
                variant="outlined"
            />
        </Grid>
        <br/>
        <Grid container><Divider /></Grid>
        <br/>
        <Grid container>
            <Button 
                variant="contained" 
                color="secondary" 
                className={classes.button} 
                onClick={() => this.props.history.goBack()}
                >
                Cancel
            </Button>
            <Button 
                variant="contained" 
                color="primary"
                className={classes.button} 
                onClick={this.handleSaveButtonClick}
                >
                Save
            </Button>
        </Grid>
      </form>
    );
  }
}

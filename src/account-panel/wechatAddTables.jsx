import React, { Component } from 'react';
import {Button, Modal, Form, Input, Radio, notification, Alert} from 'antd';
import agent from '../agent';
import {inject} from "mobx-react";


const CollectionCreateForm = Form.create({ name: 'form_in_modal' })(
    // eslint-disable-next-line

    class extends React.Component {

        render() {
            const { visible, onCancel, onCreate, form, errorVisible, onclose } = this.props;
            const { getFieldDecorator } = form;
            return (
                <Modal
                    visible={visible}
                    title="Add a new wechat"
                    okText="Create"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <Form.Item label="Wechat Name">
                            {getFieldDecorator('name', {
                                rules: [{ required: true, message: 'Please input the wechat name!' }],
                            })(<Input />)}
                        </Form.Item>
                        <Form.Item label="Wechat Account">
                            {getFieldDecorator('account', {
                                rules: [{ required: true, message: 'Please input the wechat account!' }],
                            })(<Input type="textarea" />)}
                        </Form.Item>
                    </Form>
                    {errorVisible ? (
                    <Alert
                        message="Error"
                        description="Wechat account already exists"
                        type="error"
                        closable
                        onClose={onclose}
                    />): null}

                </Modal>
            );
        }
    },
);
const openNotificationWithIcon = (type) => {
    notification.config({
        placement: 'bottomRight',
        bottom: 50,
        duration: 5,
    });

        notification[type]({
            message: 'Successfully Added New Wechat',
        });


};
@inject('userStore')
export default class EditableAddFormTable extends React.Component {
    state = {
        visible: false,
        errorVisible: false,
    };

    showModal = () => {
        this.setState({ visible: true });
    };

    handleCancel = () => {
        this.setState({ visible: false });
    };

    handleCreate = () => {
        const { form } = this.formRef.props;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
           // console.log(values)
        // if (this.props.userStore.AllSalesWechatName.wechatId.indexOf(values.account) >= 0){
        //     console.log("no")
        // }
            for (let val of this.props.userStore.AllSalesWechatName){
                if (val.wechatId.indexOf(values.account)>=0){
                    this.setState({errorVisible: true})
                    return;
                }
            }
            return agent.Account.addNewWechat(values.name, values.account)
                .then((res)=>{
                    console.log(res)
                    if (res.statusCode ===200){
                        this.props.userStore.AllWechat();
                        form.resetFields();
                        this.setState({ visible: false });
                        openNotificationWithIcon('success')
                    }
                })



    })
    };
    onClose = () => {
        this.setState({errorVisible: false})
    };


    saveFormRef = formRef => {
        this.formRef = formRef;
    };

    render() {
        return (
            <div>
                <Button type="primary" onClick={this.showModal}>
                    Add New Wechat
                </Button>
                <CollectionCreateForm
                    wrappedComponentRef={this.saveFormRef}
                    visible={this.state.visible}
                    onCancel={this.handleCancel}
                    onCreate={this.handleCreate}
                    errorVisible={this.state.errorVisible}
                    onclose={this.onClose}
                />
            </div>
        );
    }
}

import React from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { NavLink } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import Badge from '@material-ui/core/Badge';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Typography from '@material-ui/core/Typography'

import {Spin} from 'antd'

import moment from 'moment';
import { bgyellow, gray, bgsuccess } from '../common/Colors'

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  menu: {
    width: 200,
  },
  label:{
    padding: '3px',
    paddingLeft: '5px',
    paddingRight: '5px',
    marginRight: '5px',
    marginTop: '4px',
    borderRadius: 3,
    fontSize: '14px',
    color: gray, 
    textDecoration: 'None',
    backgroundColor: bgsuccess,
  },
  labelbgsuccess:{
      backgroundColor: bgsuccess,
  },
  button: {
    margin: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  margin: {
    marginTop: theme.spacing.unit * 3,
    margin: theme.spacing.unit,
  },
  badge: {
    top: 5,
    right: -15,
    cursor: 'pointer',
  },
  marginRight:{
    marginRight: theme.spacing.unit*3,
  },
});

@withStyles(styles)
@inject('userStore', 'helperStore')
@withRouter
@observer

export default class EditAccount extends React.Component {
    state = {
        isCreating: null,
        user: this.props.match.params.iduser ? this.props.userStore.getUserById(this.props.match.params.iduser) 
                                                : { phone: "", iduser: null, password:"",status: "", country: "",  email: "", createTime: "", name: "", logo: "",note:""},
        addingUser: false,
    }

  handleChange = event => {
      const {id, value} = event.target
      this.setState(prevState => {
          return {
            user: {
                ...prevState.user,
                [id]: value
            }
          }
      })
  }

  handleSelectChange = event => {
    const {name, value} = event.target
    this.setState(prevState => {
        return {
          user: {
              ...prevState.user,
              [name]: value
          }
        }
    })
  }

  handleInputChange = event => {
    const {id, value} = event.target
      if( id === 'note'){
          this.state.user.note = value
      }
    else this.setState({[id]: value})

  }

  handleAddLabel(labelName) {
    let updatingUser = this.state.user
    let updatingLabels = this.state.user[labelName]
    if(typeof(updatingLabels) == 'string') updatingLabels = updatingLabels.split('|')
    console.log(updatingLabels, typeof(updatingLabels))
    updatingLabels = updatingLabels ||[]
    if(!this.state[labelName]) {
        this.setState({
            [labelName+'Validate']: labelName+" can not be empty"
        })
        return 
    }
    updatingLabels.push(this.state[labelName])
    updatingUser[labelName] = updatingLabels
    this.setState({
      user: updatingUser, 
      [labelName]: "",
      [labelName+'Validate']: "",
    })
  }

  handleLabelDelete(labelName, delWord){
      let updatingUser = this.state.user
      let updatingArray = this.state.user[labelName]
      if(typeof(updatingArray) == 'string') updatingArray = updatingArray.split('|')||[]
      updatingArray = updatingArray || []
      updatingArray = this.deleteArrayEle(updatingArray, delWord)
      updatingUser[labelName] = updatingArray
      console.log(updatingArray)
      this.setState({
          user: updatingUser
      })
  }

  deleteArrayEle(array, delWord){
    let newArray = []
    for(let ele of array){
      if(delWord!==ele) newArray.push(ele)
    }
    return newArray
  }

  generateLabels(labelsObj, labelName){
        //console.log(this.state)
    let labels = []
    let labelArray = labelsObj

    if(typeof(labelsObj) == 'string') labelArray = labelsObj.split('|')||[]
    labelArray = labelArray || []
    for(let i=0; i<labelArray.length; i++){
      let labelobj = labelArray[i]
      labels.push(<Badge badgeContent={'x'} color="secondary" classes={{badge: this.props.classes.badge}} className={this.props.classes.marginRight} 
                    onClick={()=> this.handleLabelDelete(labelName, labelobj)}>
                    <span className={[this.props.classes.label, this.props.classes.labelbgsuccess].join(' ')} key={i} >
                    {labelobj}
                    </span>
                </Badge>)
    }
    return labels
  }

  handleSaveButtonClick = () => {
      for(let ele of this.props.userStore.Users){
        if(ele.iduser !== this.state.user.iduser && ele.email === this.state.user.email){
          window.alert("This user email "+this.state.user.email+" has been used, please try a different one. ")
          return;
        }
      }
      this.setState({
        addingUser: true
      })
      let userobj = Object.assign({}, this.state.user)
      if(!userobj['createTime']) userobj['createTime'] = moment().format('YYYY-MM-DDTHH:mm:ss')
      if(typeof(userobj.labels) === 'object' && userobj.labels) userobj['labels'] = userobj.labels.join('|')
      if(typeof(userobj.wechat) === 'object' && userobj.wechat) userobj['wechat'] = userobj.wechat.join('|')
      if(typeof(userobj.role) === 'object' && userobj.role) userobj['role'] = userobj.role.join('|')

      if(userobj.pw){
          userobj['password'] = userobj.pw
          delete userobj.pw
      }
      //console.log(JSON.stringify(userobj))

      this.props.userStore.updateUser(userobj).then(res => {
        this.setState({
          addingUser: false
        })
        this.props.history.push('/account')
      })
  }


  render() {
    //console.log(this.props)
    const { classes, helperStore } = this.props
    let user = this.state.user

    return (
      <form className={classes.container} noValidate autoComplete="off">
        <Spin spinning={this.state.addingUser}>
        <Grid container>
            <TextField
                name="country"
                select
                label="Country"
                className={classes.textField}
                value={user.country}
                onChange={this.handleSelectChange}
                InputLabelProps={{
                    shrink: true,
                }}
                SelectProps={{
                    native: true,
                    MenuProps: {
                      className: classes.menu,
                    },
                  }}
                margin="normal"
            >
                <option value="" disabled selected>
                    Select Country
                </option>
                {this.props.helperStore.countries.map(element => (
                    <option key={element} value={element}>
                        {element}
                    </option>
                ))}
            </TextField>
            <TextField
                id="name"
                label="Name"
                className={classes.textField}
                value={user.name}
                onChange={this.handleChange}
                margin="normal"
                
            />
            <TextField
                id="pw"
                label="Password"
                className={classes.textField}
                value={user.pw}
                onChange={this.handleChange}
                margin="normal"
                
            />
            
           
        
            <TextField
                id="phone"
                label="Phone"
                className={classes.textField}
                value={user.phone}
                onChange={this.handleChange}
                margin="normal"
                
            />
            <TextField
                id="email"
                label="Email"
                className={classes.textField}
                value={user.email}
                onChange={this.handleChange}
                margin="normal"
                
            />

        
            <TextField
                id="status"
                label="Status"
                select
                className={classes.textField}
                value={user.status}
                onChange={this.handleChange}
                margin="normal"
                InputLabelProps={{
                    shrink: true,
                }}
                SelectProps={{
                    native: true,
                    MenuProps: {
                      className: classes.menu,
                    },
                  }}
            >
                <option value="" disabled selected>
                    Select Status
                </option>
                <option value="valid">
                    Valid
                </option>
                <option value="unvalid">
                    Unvalid
                </option>
            </TextField>
        </Grid>
        <Grid container>
            <TextField
                id="role"
                label="Role(s)"
                select
                className={classes.textField}
                onChange={this.handleInputChange}
                value={this.state.role||""}
                error={this.state.roleValidate?true:false}
                helperText={this.state.roleValidate}
                margin="normal"  
                InputLabelProps={{
                    shrink: true,
                }}
                SelectProps={{
                    native: true,
                    MenuProps: {
                      className: classes.menu,
                    },
                  }}
            >
                <option value="" disabled>
                    Select Role
                </option>
                {this.props.helperStore.roles.map(element => (
                    <option key={element.value} value={element.value}>
                        {element.label}
                    </option>
                ))}
            </TextField>
            <Button size="small" onClick={() =>this.handleAddLabel('role')}  color="primary" className={classes.margin}>Add Role</Button>

            <Grid item xs={12} className={classes.marginRight}>  
                <Typography variant="caption" gutterBottom>
                &nbsp; &nbsp; Role(s):  {this.generateLabels(user.role, 'role')}
                </Typography>
            </Grid>
        </Grid>
        <Grid container>
          {
            (''+this.state.user['role']).indexOf('counsellor')>=0 ? 
            <TextField
              name="major"
              select
              label="Major"
              className={classes.textField}
              value={user.major||""}
              onChange={this.handleSelectChange}
              InputLabelProps={{
                  shrink: true,
              }}
              SelectProps={{
                  native: true,
                  MenuProps: {
                    className: classes.menu,
                  },
                }}
              margin="normal"
            >
              <option value="" disabled selected>
                Select Major
              </option>
              {helperStore.courseType.map(option => (
                  <option key={option} value={option}>
                    {option}
                  </option>
                ))}
            </TextField>
            :''
          }
        </Grid>
        <Grid container>
            <TextField
                id="labels"
                label="Labels"
                style={{ margin: 8 }}
                onChange={this.handleInputChange}
                value={this.state.labels||""}
                error={this.state.labelsValidate?true:false}
                helperText={this.state.labelsValidate}
                margin="normal"
                InputLabelProps={{
                    shrink: true,
                }}
            />
            <Button size="small" onClick={() =>this.handleAddLabel('labels')} color="primary" className={classes.margin}>Add Label</Button>

            <Grid item xs={12} className={classes.marginRight}>
                <Typography variant="caption" gutterBottom>
                &nbsp; &nbsp; Labels:  {this.generateLabels(user.labels, 'labels')}
                </Typography>
            </Grid>
        </Grid>
            <Grid container>
                <TextField
                    id="note"
                    label="Bank Info"
                    multiline
                    fullWidth
                    rows="3"
                    value={this.state.user.note || ""}
                    helperText="Please write Bank Account Information."
                    className={classes.textField}
                    onChange={this.handleInputChange}
                    margin="normal"
                    variant="outlined"
                />
            </Grid>
        <Grid container><Divider /></Grid>
        <Grid>
            <Button 
                variant="contained" 
                color="secondary" 
                size="small"
                className={classes.button} 
                onClick={() => this.props.history.goBack()}
                component={NavLink} to="/account" exact activeClassName={classes.active}>
                Cancel
            </Button>
            {/* <Button 
                variant="contained" 
                color="secondary" 
                size="small"
                className={classes.button} 
                onClick={() => this.props.history.goBack()}
                component={NavLink} to="/account" exact activeClassName={classes.active}>
                Delete
            </Button> */}
            <Button 
                variant="contained" 
                color="primary"
                size="small"
                className={classes.button} 
                onClick={this.handleSaveButtonClick}
                // component={NavLink} to="/account" exact activeClassName={classes.active}
                >
                Save
            </Button>
        </Grid>
        </Spin>
      </form>
    );
  }
}

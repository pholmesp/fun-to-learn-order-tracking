import React, { Component } from 'react';
import { NavLink } from "react-router-dom";
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search'
import TablePagination from "@material-ui/core/TablePagination";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import {bgerror, gray, bgsuccess, lightBlue} from '../common/Colors'

  
  const styles = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      overflowX: 'auto',
      backgroundColor: theme.palette.background.paper,
    },
    table: {
      minWidth: 700,
    },
    tabsRoot: {
      width: '100%',
      borderBottom: '1px solid #e8e8e8',
      paddingTop: theme.spacing.unit,
    },
    row: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
      },
    },
    button: {
      margin: theme.spacing.unit,
    },
    rightIcon: {
      marginLeft: theme.spacing.unit,
    },
    input: {
      display: 'none',
    },
    bgerror: {
      backgroundColor: bgerror,
    },
    label:{
      padding: '3px',
      paddingLeft: '5px',
      paddingRight: '5px',
      marginRight: '5px',
      marginTop: '4px',
      marginLeft: '3px',
      borderRadius: 3,
      fontSize: '12px',
      textDecoration: 'None',
      backgroundColor: lightBlue,
    },   
    textField: {
      marginLeft: 8,
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    search:{
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
      width: 400,
    }
  });
  
  @withStyles(styles)
  @inject('userStore', 'helperStore')
  @withRouter
  @observer
  
  export default class Account extends Component {
    state = {
      currentStatus: 'admin',
      roles: this.props.helperStore.roles,
      search: '', 
      page: 0, 
      rowsPerPage: 15,
      view:false,
      key:''
    }
    generateLabels(labelsObj, labelName){
      let labels = []
      let labelArray = labelsObj
  
      if(typeof(labelsObj) == 'string') labelArray = labelsObj.split('|')||[]
      labelArray = labelArray || []
      for(let i=0; i<labelArray.length; i++){
        let labelobj = labelArray[i]
        labels.push(<span className={[this.props.classes.label, this.props.classes.labelbgsuccess].join(' ')} key={i} >
                      {labelobj}
                    </span>
                  )
      }
      return labels
    }
    handleChange = (event, currentStatus) => {
      this.setState({ currentStatus: currentStatus });
    };
    handleChangePage = (event, page) => {
      this.setState({ page });
    };
    handleInputChange = event => {
      const {id, value} = event.target
      this.setState({[id]: value})
  }
  
    handleChangeRowsPerPage = event => {
      this.setState({ rowsPerPage: event.target.value });
    };

    view=(key)=>{
      this.setState({view:true,key:key})
    }
    Hide=(key)=>{
      this.setState({view:false,key:key})
    }
    render() {
      const { classes, userStore} = this.props;
      let users = userStore.Users || []
      const { roles, currentStatus, search, page, rowsPerPage} = this.state


      let filtedUser = []
      if(search){
        for(let ele of users){
          if(JSON.stringify(ele).toLowerCase().indexOf(search.toLowerCase())>=0){
            filtedUser.push(ele)
          }
        }
      }else{
        filtedUser = users
      }

      return (
        <div className={classes.root}>
          <Grid container justify={'space-between'} >
            <Grid item className={classes.search}>
              <TextField 
                id="search"
                placeholder="Search Orders" 
                className={classes.textField} 
                onChange={this.handleInputChange}
                value={search}
              />
              <IconButton className={classes.iconButton} aria-label="Search">
                <SearchIcon />
              </IconButton>
            </Grid>
            <Grid item>
              <Button variant="contained" color="primary" className={classes.button} component={NavLink} to="/account/edit" exact activeClassName={classes.active}>
                Add User
                <Icon className={classes.rightIcon}>add_circle</Icon>
              </Button>
            </Grid>
          </Grid>
          <Grid>
          <Tabs 
            value={currentStatus} 
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            className={classes.tabsRoot}
            >
            { roles.map((item, key) => <Tab key={key} value={item.value} label={
              <div>{item.label} 
                <span className={this.props.classes.label}>
                  {filtedUser.filter(c => c.role.split('|').indexOf(item.value) >= 0).length}
                </span>
              </div>
            } /> )}
          </Tabs>
          </Grid>
          <Table className={classes.table} size="small">
            <TableHead>
              <TableRow>
                <TableCell padding='dense'>#</TableCell>
                <TableCell padding='dense'>Name</TableCell>
                <TableCell padding='dense'>Role(s)</TableCell>
                <TableCell padding='dense'>Country</TableCell>
                <TableCell padding='dense'>Phone Number</TableCell>
                <TableCell padding='dense'>Email</TableCell>
                <TableCell padding='dense'>Labels</TableCell>
                <TableCell padding='dense'>Wechat(s)</TableCell>
                <TableCell padding='dense'>Bank Info</TableCell>
                <TableCell padding='dense'>Major</TableCell>
                <TableCell padding='dense'>Status</TableCell>
                <TableCell padding='dense'>Operate</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {filtedUser.filter(c => c.role.split('|').indexOf(currentStatus) >= 0).slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(user => {
                return (
                  <TableRow className={user.status=='valid'? classes.row : classes.bgerror} key={user.iduser} hover={user.status=='valid'? true : false}>
                    <TableCell padding='dense'>{user.iduser}</TableCell>
                    <TableCell padding='dense'>{user.name}</TableCell>
                    <TableCell padding='dense'>{user.role}</TableCell>
                    <TableCell padding='dense'>{user.country}</TableCell>
                    <TableCell padding='dense'>{user.phone}</TableCell>
                    <TableCell padding='dense'>{user.email}</TableCell>
                    <TableCell padding='dense'>{this.generateLabels(user.labels)}</TableCell>
                    <TableCell padding='dense'>{this.generateLabels(user.wechat)}</TableCell>
                    {this.state.key===user.iduser && this.state.view?

                        <TableCell padding='dense'>
                          {user.note}
                          <Button className={classes.button} onClick={()=>{this.Hide(user.iduser)}}> Hide </Button>
                        </TableCell>
                        :
                        <TableCell padding='dense'>
                          <Button className={classes.button} onClick={()=>{this.view(user.iduser)}}> View </Button>
                        </TableCell>
                    }

                    <TableCell padding='dense'>{user.major}</TableCell>
                    <TableCell padding='dense'>{user.status}</TableCell>
                    <TableCell padding='dense'><Button  className={classes.button} component={NavLink} to={"/account/edit/"+user.iduser} exact activeClassName={classes.active}>
                      Edit
                    </Button>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
          <TablePagination
            rowsPerPageOptions={[15, 30, 50]}
            component="div"
            count={filtedUser.filter(c => c.role.split('|').indexOf(currentStatus) >= 0).length}
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              'aria-label': 'Previous Page',
            }}
            nextIconButtonProps={{
              'aria-label': 'Next Page',
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
        </div>
      )
    }
  }

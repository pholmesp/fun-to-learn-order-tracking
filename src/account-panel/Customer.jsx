import React, { Component } from 'react';
import { NavLink } from "react-router-dom";
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search'
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { gray } from '@material-ui/core/colors';

import {PermissibleRender} from '@brainhubeu/react-permissible';
import TablePagination from "@material-ui/core/TablePagination";

import { bgsuccess, lightBlue } from '../common/Colors'
import { GenerateSimpleLabel } from '../util/generateLabel';

  
  const styles = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      overflowX: 'auto',
      backgroundColor: theme.palette.background.paper,

    },
    table: {
      minWidth: 700,
    },
    tabsRoot: {
      width: '100%',
      borderBottom: '1px solid #e8e8e8',
      paddingTop: theme.spacing.unit,
    },
    row: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.background.default,
      },
    },
    button: {
      margin: theme.spacing.unit,
    },
    rightIcon: {
      marginLeft: theme.spacing.unit,
    },
    input: {
      display: 'none',
    },
    span: {
      fontSize: '14px',
      color: gray,
      paddingLeft: '5px'
    },
    textField: {
      marginLeft: 8,
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    label:{
      padding: '3px',
      paddingLeft: '5px',
      paddingRight: '5px',
      marginRight: '5px',
      marginTop: '4px',
      marginLeft: '3px',
      borderRadius: 3,
      fontSize: '12px',
      textDecoration: 'None',
      backgroundColor: lightBlue,
    }, 
    search:{
      padding: '2px 4px',
      display: 'flex',
      alignItems: 'center',
      width: 400,
    }
  });
  
@withStyles(styles)
@inject('customerStore', 'userStore', 'helperStore', 'authStore')
@withRouter
@observer
export default class Customer extends Component {
  state = {
    currentStatus: 'America',
    country: this.props.helperStore.countries.filter(country => country !=='India').sort(),
    search: '', 
    page: 0, 
    rowsPerPage: 15,
  }
  handleChange = (event, currentStatus) => {
    this.setState({ 
      currentStatus: currentStatus,
      page: 0,
     });
  };
  handleInputChange = event => {
    const {id, value} = event.target
    this.setState({[id]: value})
}
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };

  render() {
    let customers =  []
    let filteredCustomers = []
    const { classes, customerStore, userStore, authStore } = this.props;
    const { country, currentStatus, page, rowsPerPage} = this.state
    if(authStore.currentUser.role.indexOf('admin')>=0 || authStore.currentUser.role.indexOf('services')>=0) customers =customerStore.Customers
    else customers = customerStore.getSalerCustomer(authStore.currentUser.iduser);
    // console.log("salecustomers:"+customers)

    if(this.state.search){
      for(let ele of customers){
        if(JSON.stringify(ele).toLowerCase().indexOf(this.state.search.toLowerCase())>=0){
          filteredCustomers.push(ele)
        }
      }
    }else{
      filteredCustomers = customers
    }


    return (
        <div className={classes.root}>
          <Grid container justify={'space-between'} >
            <Grid item className={classes.search}>
              <TextField 
                id="search"
                placeholder="Search Orders" 
                className={classes.textField} 
                onChange={this.handleInputChange}
                value={this.state.search}
              />
              <IconButton className={classes.iconButton} aria-label="Search">
                <SearchIcon />
              </IconButton>
            </Grid>
            <Grid item>
              <Button variant="contained" color="primary" className={classes.button} component={NavLink} to="/customer/edit" exact activeClassName={classes.active}>
                Add Customer
                <Icon className={classes.rightIcon}>add_circle</Icon>
              </Button>
            </Grid>
          </Grid>
          <Grid>
          <Tabs 
            value={currentStatus} 
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            className={classes.tabsRoot}
            >
            { country.map((item, key) => <Tab key={key} value={item} label={
              <div>{item} 
                <span className={this.props.classes.label}>
                  {filteredCustomers.filter(c => c.country === item).length}
                </span>
              </div>
            } /> )}
          </Tabs>
          </Grid>
          <Table className={classes.table} size="small">
            <TableHead>
              <TableRow>
                <TableCell padding='dense'># </TableCell>
                <TableCell padding='dense'>Name</TableCell>
                <TableCell padding='dense'>History No.</TableCell>
                <TableCell padding='dense'>Country</TableCell>
                <TableCell padding='dense'>University/URL</TableCell>
                <TableCell padding='dense'>Level</TableCell>
                <TableCell padding='dense'>English Level</TableCell>
                <TableCell padding='dense'>Email</TableCell>
                <TableCell padding='dense'>SaleWechat</TableCell>
                <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm> 
                  <TableCell padding='dense'>Customer Wechat</TableCell>
                </PermissibleRender>
                <TableCell padding='dense'>Labels</TableCell>
                <TableCell padding='dense'>Operate</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {filteredCustomers.filter(c => c.country === currentStatus).slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(customer => {
                return (
                  <TableRow className={classes.row} key={customer.idcustomer}>
                    <TableCell padding='dense'>{customer.idcustomer}</TableCell>
                    <TableCell padding='dense'>{customer.customerName}</TableCell>
                    <TableCell padding='dense'>{customer.code}</TableCell>
                    <TableCell padding='dense'>{customer.country}</TableCell>
                    <TableCell padding='dense'>{customer.university}<p>{customer.universityUrl}</p></TableCell>
                    <TableCell padding='dense'>{customer.level}</TableCell>
                    <TableCell padding='dense'>{customer.englishLevel}</TableCell>
                    <TableCell padding='dense'>{customer.customerEmail}</TableCell>
                    <TableCell padding='dense'>
                        {/*{customer.creator_iduser}-{userStore.getUserById(customer.creator_iduser).name}: */}
                        {customer.wechatName}</TableCell>
                    <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm> 
                      <TableCell padding='dense'>{customer.customerWechat}</TableCell>
                    </PermissibleRender>
                    <TableCell padding='dense'><GenerateSimpleLabel labels={customer.labels}/> </TableCell>
                    <TableCell padding='dense'><Button  className={classes.button} component={NavLink} to={"/customer/view/"+customer.idcustomer} exact activeClassName={classes.active}>
                      View Detail
                    </Button>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
          <TablePagination
            rowsPerPageOptions={[15, 30, 50]}
            component="div"
            count={filteredCustomers.filter(c => c.country === currentStatus).length}
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              'aria-label': 'Previous Page',
            }}
            nextIconButtonProps={{
              'aria-label': 'Next Page',
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
        </div>
      )
    }
    
  }
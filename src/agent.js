import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';
import commonStore from './stores/commonStore';
import authStore from './stores/authStore';
import config from './config'
import { request } from 'https';

const superagent = superagentPromise(_superagent, global.Promise);

// const API_ROOT = 'https://conduit.productionready.io/api';
// const API_ROOT = 'http://localhost:3000/api';
const API_ROOT = config.ROOT_API_URL
const QAPI_ROOT = config.ROOT_QAPI_URL

const handleErrors = err => {
  if (err && err.response && err.response.status === 401) {
    authStore.logout();
  }
  // return err;
};

const responseBody = res => {
  // console.log(res)
  if(res.body) res.body.statusCode=res.statusCode
  return res.body
};

const tokenPlugin = req => {
  if (commonStore.token) {
    req.set('authorization', `Token ${commonStore.token}`);
  }
};

//QAPI request
const requests = {
  del: url =>
    superagent
      .del(`${QAPI_ROOT}${url}`)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
  get: url =>
    superagent
      .get(`${QAPI_ROOT}${url}`)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
  put: (url, body) =>
    superagent
      .put(`${QAPI_ROOT}${url}`, body)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
  post: (url, body) =>
    superagent
      .post(`${QAPI_ROOT}${url}`, body)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
};

//Basic API request
const brequests = {
  del: url =>
    superagent
      .del(`${API_ROOT}${url}`)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
  get: url =>
    superagent
      .get(`${API_ROOT}${url}`)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
  put: (url, body) =>
    superagent
      .put(`${API_ROOT}${url}`, body)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
  post: (url, body) =>
    superagent
      .post(`${API_ROOT}${url}`, body)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
};


const Auth = {
  current: () =>
    brequests.get('/currentUser'),
  login: (email, password) =>
    brequests.post('/login', { email: email, password:password }),
  // register: (username, email, password) =>
  //   brequests.post('/users', { user: { username, email, password } }),
  editUser: user =>
    brequests.put('/editUser', { user }),
  logout: () => brequests.logout('/logout'), 
};

const Orders = {
  all: () =>
    requests.get('/orders'),
  delete: idorder =>
    requests.get(`/deleteOrder/${idorder}`),
  update: obj => superagent
    .post(`${API_ROOT}/updateOrder`, obj)  ////obj likes {key=key, value=value, idorder=idorder}
    .use(tokenPlugin)
    .end(handleErrors)
    .then(responseBody),
  create: order =>
    requests.post('/addOrder', order), 
  getCompletedOrdersByPage: (start, limit) => requests.get(`/getCompletedOrdersByPage/${limit}/${start}`),
  getCompletedOrdersTotal: () => requests.get(`/getCompletedOrdersTotal`),
  getCompletedOrdersMonths: () => requests.get(`/getCompletedOrdersMonths`),
  getOrdersMonths: () => requests.get(`/getOrdersMonths`),
  getHistoryOrdersByMonth: (month) => requests.get(`/getHistoryOrdersByMonth/${month}`),
  getSaleCompletedOrdersMonths: (iduser) => requests.get(`/getSaleCompletedOrdersMonths/${iduser}`),
    getSaleCompletedOrdersWechatMonths: (iduser) => brequests.post('/getAllSaleMonthByWechat', { iduser:iduser }),
  getSaleHistoryOrdersByMonth: (iduser, month) => brequests.post('/getAllSaleHistoryOrdersByWechatMonth', { iduser:iduser, month:month }),
  getOrdersBySaleDeadline: (iduser, deadline) => requests.get(`/getOrdersBySaleDeadline/${iduser}/${deadline}`),
  getOrdersBySaleCreateTime: (iduser, createTime) => requests.get(`/getOrdersBySaleCreateTime/${iduser}/${createTime}`),
  getOrdersByCounsellorDeadline: (iduser, createTime) => requests.get(`/getOrdersByCounsellorDeadline/${iduser}/${createTime}`),
  updateComment: (comment) => requests.post('/addOrderComments', comment),
  getComments: (idorder) => requests.get(`/getOrderCommentsByOrderId/${idorder}`),
  deleteComment: (idcomment) => requests.get(`/deleteCommentsToOrder/${idcomment}`),

  updateInvitation: (invitation) => requests.post('/addInvitation', invitation), 
  getInvitations: (idorder) => requests.get(`/getInvitationsByOrderId/${idorder}`),
  getInvitationsByIduser: (iduser, role) => requests.get(`/getInvitationsByIduser/${iduser}/${role}`),
  getOrdersByIduser: (iduser) =>requests.get(`/getOrdersByIduser/${iduser}`),
    getSaleOrdersByWechat: (iduser) =>brequests.post('/getAllSaleOrdersByWechat', { iduser:iduser }),
  getOrdersByIdCustomer: (idcustomer) => requests.get(`/getOrdersByIdCustomer/${idcustomer}`), 
  getOrderResultsByIduser: (iduser) => requests.get(`/getOrderResultsByIduser/${iduser}`), 
  getOrderResultByOrderId: (idorder) => requests.get(`/getOrderResultByOrderId/${idorder}`),
  deleteOrderResultByOrderId: (idorder) => requests.get(`/deleteOrderResultByOrderId/${idorder}`),
  deleteOrderResultByOrderIdUserId: (idorder, iduser) => requests.get(`/deleteOrderResultByOrderIdUserId/${idorder}/${iduser}`),
  getStatistic: () => requests.get(`/getStatistic`),
  getMyStatistic: (iduser) => requests.get(`/getMyStatistic/${iduser}`),
  getPackageOrders: () => requests.get(`/packageOrders`),
  getOrderByID: (id) => requests.get(`/getOrderByID/${id}`)
};
const Package = {
  update: obj => superagent
      .post(`${API_ROOT}/updatePackage`, obj)  ////obj likes {key=key, value=value, idorder=idorder}
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
  updateComment: (comment) => requests.post('/addPackageComments', comment),
};

const Course = {
  all: () =>
    requests.get('/getCource'),
  del: idcourse =>
    requests.get(`/deleteCource/${idcourse}`),
  // update: obj => superagent
  //   .post(`${API_ROOT}/updateOrder`, obj)  ////obj likes {key=key, value=value, idorder=idorder}
  //   .use(tokenPlugin)
  //   .end(handleErrors)
  //   .then(responseBody),
  create: cource =>
    requests.post('/addCource', cource)
};

const SignedUserOrders = {
  getUserOrders: (iduser) =>
    requests.get(`/getUserOrders/${iduser}`),
  getUserOrdersDetail: (iduser) =>
    requests.get(`/getUserOrdersDetail/${iduser}`),
  del: idorder =>
    requests.get(`/userOrders/${idorder}`),
  updateUserOrder: userOrder =>
    requests.post('/updateUserOrder', userOrder),
};

const OrderPackage = {
  getAll: () =>
    requests.get('/getOrderPackage'),
  del: idOrderPackage =>
    requests.get(`/deleteOrderPackage/${idOrderPackage}`),
  update: orderPackage =>
    requests.post('/updateOrderPackage', orderPackage),
  create: orderPackage =>
    requests.post('/updateOrderPackage', orderPackage), 
  getOrderPackageByUser: iduser => requests.get(`/getOrderPackageByUser/${iduser}`), 
  updatePackageStatus: (idorderPackage, status) => requests.get(`/updatePackageStatus/${status}/${idorderPackage}`),
  //updateThePackageCost: (currency_cost,cost,idorderPackage)=> requests.post(`/updateThePackageCost`, {currency_cost:currency_cost, cost:cost, idorderPackage:idorderPackage}),
  updateThePackageCost: (cost,idorderPackage)=> requests.get(`/updateThePackageCost/${cost}/${idorderPackage}`),
  updatePackageOrderCurrencySale: (currency_sale,idorder)=>requests.get(`/updatePackageOrderCurrencySale/${currency_sale}/${idorder}`),

};

const Users = {
  create: user =>
    requests.post(`/addUser`, user),
  disable: iduser =>
    requests.get(`/disableUser/${iduser}`),
  update: user =>
    requests.post(`/addUser`, user),
  select: () =>
    requests.get(`/allUsers`), 
  allUsers: () =>
    requests.post(`/allUsers`),
  updateUser: user => 
    requests.post('/updateUser', user)
};
const Customers = {
  all: () => requests.get(`/getCustomer`),
  delete: (idcustomer) => requests.get(`/deleteCustomer/${idcustomer}`),
  update: (customer) => requests.post(`/updateCustomer`, customer),
  getSaleCustomersByWechat: (iduser)=> brequests.post('/getSaleCustomersByWechat',{iduser:iduser})
}

const Exchange = {
  selectRequest: () => requests.get(`/allExchangeRequest`),
  deleteRequest: (idexchangeRequest) => requests.get(`/deleteExchangeRequest/${idexchangeRequest}`),
  updateRequest: (exchangeRequest) => requests.post(`/updateExchangeRequest`, exchangeRequest),
  selectReserveByUser: (iduser) => requests.get(`/getExchangeReserveByUserId/${iduser}`),
  selectReserve: () => requests.get(`/allExchangeReserve`),
  deleteReserve: (idexchangeReserve) => requests.get(`/deleteExchangeReserve/${idexchangeReserve}`),
  updateReserve: (exchangeReserve) => requests.post(`/updateExchangeReserve`, exchangeReserve),
}

const Price = {
  allPriceRequest: () => requests.get(`/allPriceRequest`),
  getQuoteByIdpriceRequest: (idpriceRequest) => requests.get(`/getPriceQuoteByIdpriceRequest/${idpriceRequest}`),
    //getHistoryOrdersByIdCourse: (IdCourse) => requests.get(`/getHistoryOrdersByIdCourse/${IdCourse}`),
    getHistoryOrdersByIdCourse: (course) =>
        brequests.post('/searchbycourse', { course:course }),
  getUserPriceQuote: (iduser) => requests.get(`/getPriceQuoteByIduser/${iduser}`),
  updateRequest: (priceRequest) => requests.post(`/updatePriceRequest`, priceRequest),
  updateQuote: (priceQuote) => requests.post(`/updatePriceQuote`, priceQuote)
}
const Statistic = {
  getOrderStatisticByCountryMonth: (country, month) => requests.get(`/getOrderStatisticByCountryMonth/${country}/${month}`),
  getNewCustomerStatisticByCountryMonth: (country, month) => requests.get(`/getNewCustomerStatisticByCountryMonth/${country}/${month}`),
  getCustomerStatisticByCountryMonth: (country, month) => requests.get(`/getCustomerStatisticByCountryMonth/${country}/${month}`),
  getOrderStatisticByMonthGroupBySale: (month) => requests.get(`/getOrderStatisticByMonthGroupBySale/${month}`),
  getOrderStatisticByDeadlineGroupBySale: (month) => requests.get(`/getOrderStatisticByDeadlineGroupBySale/${month}`),
  getOrderStatisticByDeadlineGroupByCounsellor: (month) => requests.get(`/getOrderStatisticByDeadlineGroupByCounsellor/${month}`),
  getOrderStatisticByMonthGroupByHour: (day) => requests.get(`/getOrderStatisticByMonthGroupByHour/${day}`),
  getOrderStatisticByMonthGroupByDay: (month) => requests.get(`/getOrderStatisticByMonthGroupByDay/${month}`),  

}

const Quality = {
  getCheckQuestions: () => requests.get(`/getCheckQuestions`),
  getOrderResultList: (idorder) => requests.get(`/getOrderResultList/${idorder}`),
  sumitOrderResult: (orderResult) => requests.post(`/sumitOrderResult`, orderResult),
  deleteOrderResult: (idorderResult) => requests.get(`/deleteOrderResult/${idorderResult}`),
  email: (orderId, mode) => brequests.post(`/QaMail`, {orderId:orderId, mode: mode}),


}
const Helper = {
  uploadFiles: formData => superagent
    .post(`${API_ROOT}/uploadFiles/`, formData)
    .use(tokenPlugin)
    .end(handleErrors)
    .then(responseBody),
  getCurrency: ()  =>  requests.get(`/getCurrency`), 
  getTimezone: () => requests.get(`/getTimezone`), 
  getCourse: () => requests.get(`/getCource`),
  getAllUni: () => requests.get(`/getAllUni`),
  getCountryUni: (country) => requests.get(`/getCountryUni/${country}`),
  getCountry: () => requests.get(`/getCountry`),
  updateUniName: (name, id) => requests.get(`/updateUniName/${name}/${id}`),
  deleteUni: (id) => requests.get(`/deleteUni/${id}`),
  addUni: (country, name) => requests.get(`/addUni/${country}/${name}`),
}
const Councellor = {
  emailAdmin: (orderId, orderName, counsellorID, counsellorName) => brequests.post(`/mailAdmin`, {orderId:orderId, orderName: orderName, counsellorID:counsellorID, counsellorName:counsellorName}),
  emailSale: (orderId, orderName, counsellorID, counsellorName, mode) => brequests.post(`/mailSale`, {orderId:orderId, orderName: orderName, counsellorID:counsellorID, counsellorName:counsellorName, mode: mode}),

}

const Account ={
  getAllSalesWechat:()=> requests.get(`/getAllSalesWechat`),
  getAllWechat:()=> requests.get(`/getAllWechat`),
  removeWeChat: (id) => requests.get(`/removeWechat/${id}`),
  addWeChat:(idUser, id)=> requests.get(`/addWechat/${idUser}/${id}`),
  getIdWeChat: (wechatId)=>requests.get(`/getIdWechat/${wechatId}`),
  getSaleWeChat: (iduser)=>requests.get(`/getSaleWechat/${iduser}`),
  updateWechat: (wechatName, id) => requests.get(`/updateSaleWechat/${wechatName}/${id}`),
  deleteWechat: (id) => requests.get(`/deleteWechat/${id}`),
  addNewWechat:(name, id)=> requests.get(`/addNewWechat/${name}/${id}`),
  getWechatInfo:(id)=> requests.get(`/getWechatInfo/${id}`),
  updateUserNote:(note, iduser)=> requests.get(`/updateUserNote/${note}/${iduser}`),
  updateUserPhone: (phone, iduser)=> requests.get(`/updateUserPhone/${phone}/${iduser}`),
  refreshUser: (email) => requests.get(`/refreshUser/${email}`)
}

export default {
  Orders,
  Course,
  SignedUserOrders,
  OrderPackage,
  Auth,
  Users,
  Customers,
  Price,
  Statistic,
  Quality,
  Helper,
  Exchange,
  Councellor,
  Account,
  Package,
}

import React, { Component } from 'react';
import { NavLink } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import Collapse from '@material-ui/core/Collapse'
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Public from '@material-ui/icons/Public';
import NaturePeople from '@material-ui/icons/NaturePeople';
import LibraryBooks from '@material-ui/icons/LibraryBooks';
import cyan from '@material-ui/core/colors/cyan';

@withStyles( theme =>({
  active: {
    backgroundColor: cyan[100],
  },
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing.unit * 3,
  },
}))
export default class ServicesPanel extends Component{
  state = {
    open: true,
  };

  handleClick = () => {
    this.setState(state => ({ open: !state.open }));
  };
  render(){
    const { classes } = this.props;
    return(
      <List>
        <ListItem button onClick={this.handleClick}>
          <ListItemIcon>
            <Public />
          </ListItemIcon>
          <ListItemText inset primary="Market" />
          {this.state.open ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
    
        <Collapse in={this.state.open} timeout="auto" unmountOnExit>
          <Divider />
          <List component="div"  >
            <ListItem button  className={classes.nested} component={NavLink} to="/courses" exact activeClassName={classes.active}>
              <ListItemIcon>
                <LibraryBooks />
              </ListItemIcon>
              <ListItemText primary="Courses" />
            </ListItem>
            {/* <ListItem button className={classes.nested} component={NavLink} to="/customer" exact activeClassName={classes.active}>
              <ListItemIcon>
                <NaturePeople />
              </ListItemIcon>
              <ListItemText primary="Customer" />
            </ListItem> */}
          </List>
        </Collapse>
        <Divider />
      </List>
    );
  }
} 

import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import blue from '@material-ui/core/colors/blue';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Divider from '@material-ui/core/Divider'
import moment from 'moment';

import {Map} from 'immutable'

import agent from '../agent'

import {
  ComposedChart, Bar, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,  ResponsiveContainer
} from 'recharts';


const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  paddingTop: {
    paddingTop: theme.spacing.unit,
    backgroundColor: 'None',
  },

  padding: {
    padding: `0 ${theme.spacing.unit * 2}px`,
  },
  tabsRoot: {
    width: '100%',
    borderBottom: '1px solid #e8e8e8',
    paddingTop: theme.spacing.unit,
  },
  button: {
    margin: theme.spacing.unit,
  },
  textField: {
    marginLeft: 8,
    flex: 1,
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  listitemBackColor: {
    backgroundColor: blue[100],
  },
  iconButton: {
    padding: 10,
  },
  search:{
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
  }
});

const COLORS = ['#a8e6cf', '#dcedc1', '#ffd3b6', '#ffaaa5', '#ff8b94']

@withStyles(styles)
@inject('ordersStore', 'helperStore', 'userStore', 'statisticStore')
@withRouter
@observer
export default class OrderStatisticLineChartMonth extends Component{
  state = {
    currentStatus: '',
    months: [],
    todayStatistic: '',
    orderStatistic: new Map(),
  };

  componentDidMount(){
    let currentStatus = [...this.props.ordersStore.OrdersMonths.values()][0] ? [...this.props.ordersStore.OrdersMonths.values()][0].months : ''
    let months = [...this.props.ordersStore.OrdersMonths.values()]

    this.setState({
        currentStatus: currentStatus,
        months: months,
    })
    if(currentStatus !== '') this.getOrderStatisticByMonthGroupByDay(currentStatus)
    this.getOrderStatisticGroupByHour(moment().format('YYYY-MM-DD'))
  }

  getOrderStatisticGroupByHour(day){
    agent.Statistic.getOrderStatisticByMonthGroupByHour(day).then(res =>{
      // console.log(res)
      if(res.statusCode === 200){
        delete res['statusCode']
        this.setState({
          todayStatistic: res
        })
      }
      
    })
  }

  getOrderStatisticByMonthGroupByDay(month){
    agent.Statistic.getOrderStatisticByMonthGroupByDay(month).then(res => {
      // console.log(res)

      if(res.statusCode === 200){       
          delete res['statusCode']
          let newOrderStatistic = this.state.orderStatistic.set(month, res)
          this.setState({orderStatistic: newOrderStatistic})
      }
    })
  }

  handleChange = (event, currentStatus) => {
    this.setState({ currentStatus: currentStatus });
    this.getOrderStatisticByMonthGroupByDay(currentStatus)
  };
  handleInputChange = event => {
    const {id, value} = event.target
    this.setState({[id]: value})
  }
  
  render(){
    const { classes, statisticStore, userStore } = this.props;
    const { currentStatus, months, orderStatistic, todayStatistic } = this.state;

    if(currentStatus === '') return <p></p>
    if(todayStatistic === '') return <p></p>
    if(!orderStatistic) return <p></p>

    const monthDays = moment(currentStatus, 'YYYY-MM').daysInMonth()
    const month = moment(currentStatus, 'YYYY-MM').format('MM')
    const xLabels = new Array(monthDays).fill(0).map((_,i) => i>=9 ? `${month}-${i+1}` : `${month}-0${i+1}`);
    const xLabelsHours = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24]

    let daysData = orderStatistic.get(currentStatus)||[]
    let accumulate = 0
    let daysRangeStatistic = []
    for(let h of xLabelsHours){
      for(let ele of todayStatistic){
        if(ele.hours === ''+h){
          accumulate += ele.orderNumber
          ele['Total today'] = accumulate
          daysRangeStatistic.push(ele)
        }else{
          let empty = {}
          empty['orderNumber'] = 0
          empty['Total today'] = accumulate
          empty['hours'] = ''+h
          daysRangeStatistic.push(empty)
        }
      }
    }


    let daysAccumulate = 0
    let monthRangeStatistic = []
    for(let h of xLabels){
      for(let ele of daysData){
        let day = ele.day.substring(5,ele.day.length)
        if(day === ''+h){
          daysAccumulate += ele.orderNumber
          ele['Total month'] = daysAccumulate
          monthRangeStatistic.push(ele)
        }else{
          let empty = {}
          empty['orderNumber'] = 0
          empty['Total month'] = daysAccumulate
          empty['day'] = ''+h
          monthRangeStatistic.push(empty)
        }
      }
    }

    return(
      <Grid container className={classes.root}>
        <Tabs 
            value={currentStatus} 
            onChange={this.handleChange}
            indicatorColor="primary"
            textColor="primary"
            className={classes.tabsRoot}
            >
            { months.map((item, key) => <Tab key={key} value={item.months} label={item.months} /> )}
          </Tabs>
        <Grid container className={classes.paddingTop}>
          {
            currentStatus === moment().format('YYYY-MM') ? 
            <Grid container>
              Todays' Order Trends in Hours:
            <ResponsiveContainer width={'100%'} height={300}>
              <ComposedChart
                data={daysRangeStatistic}
                margin={{
                  top: 5, right: 30, left: 20, bottom: 5,
                }}
              >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="hours" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar type="monotone" dataKey="orderNumber" fill="#8884d8"  />
                <Line type="monotone" dataKey="Total today" stroke="#82ca9d" activeDot={{ r: 8 }}/>
              </ComposedChart>
            </ResponsiveContainer>
          </Grid>
            : ''
          }
          <Divider />
          <Grid container>
            Month's Order Number Trends:
            <ResponsiveContainer width={'100%'} height={300}>
              <ComposedChart
                data={monthRangeStatistic}
                margin={{
                  top: 5, right: 30, left: 20, bottom: 5,
                }}
              >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="day" />
                <YAxis padding={{ top: 30}}/>
                <Tooltip />
                <Legend />
                <Bar type="monotone" dataKey="orderNumber" fill="#8884d8"  />
                <Line type="monotone" dataKey="Total month" stroke="#82ca9d" activeDot={{ r: 8 }} />
              </ComposedChart>
            </ResponsiveContainer>
          </Grid>

        </Grid>
      </Grid>
    );
  }
} 

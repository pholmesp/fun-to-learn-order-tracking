import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import blue from '@material-ui/core/colors/blue';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import TableHead from '@material-ui/core/TableHead';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import moment from 'moment';

import HeatMap from "react-heatmap-grid";
import {Map} from 'immutable'
import agent from '../agent'
import { error, bgLightBlue} from '../common/Colors'
import {
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer,PieChart, Pie,
} from 'recharts';
const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({
  cx, cy, midAngle, innerRadius, outerRadius, percent, index,
}) => {
   const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
      {`${(percent * 100).toFixed(0)}%`}
    </text>
  );
};
const data = [
  {
    name: 'Page A', uv: 4000, pv: 2400, amt: 2400,
  },
  {
    name: 'Page B', uv: 3000, pv: 1398, amt: 2210,
  },
  {
    name: 'Page C', uv: 2000, pv: 9800, amt: 2290,
  },
  {
    name: 'Page D', uv: 2780, pv: 3908, amt: 2000,
  },
  {
    name: 'Page E', uv: 1890, pv: 4800, amt: 2181,
  },
  {
    name: 'Page F', uv: 2390, pv: 3800, amt: 2500,
  },
  {
    name: 'Page G', uv: 3490, pv: 4300, amt: 2100,
  },
];


const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  paddingTop: {
    paddingTop: theme.spacing.unit,
    backgroundColor: 'None',
  },
  paddingLeft: {
    paddingLeft: theme.spacing.unit,
  },
  underline: {
    textDecoration: 'underline',
    fontWeight: 'bold',
    color: error,
  },
  panel:{
    marginRight: theme.spacing.unit * 2
  },
  padding: {
    padding: `0 ${theme.spacing.unit * 2}px`,
  },
  tabsRoot: {
    width: '100%',
    borderBottom: '1px solid #e8e8e8',
    paddingTop: theme.spacing.unit,
  },
  h4:{
    width: '100%',
    borderBottom: '1px solid #e8e8e8',
    paddingTop: theme.spacing.unit,
  },
  button: {
    margin: theme.spacing.unit,
  },
  textField: {
    marginLeft: 8,
    flex: 1,
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  listitemBackColor: {
    backgroundColor: blue[100],
  },
  iconButton: {
    padding: 10,
  },
  search:{
    padding: '2px 4px',
    display: 'flex',
    alignItems: 'center',
    width: 400,
  }
});

const COLORS = ['#a8e6cf', '#dcedc1', '#ffd3b6', '#ffaaa5', '#ff8b94']

@withStyles(styles)
@inject('ordersStore', 'helperStore', 'userStore', 'authStore', 'statisticStore')
@withRouter
@observer
export default class SaleKPI extends Component{
  state = {
    SaleOrdersDetail: this.props.ordersStore.SaleOrdersDetail,
    currentStatus: '',
    months: [],
    ordersByMonth: new Map(),
    splitedOrders: new Map(),
    orderStatisticByDay: new Map(),
  };

  componentDidMount(){
    let currentStatus = [...this.props.ordersStore.OrdersMonths.values()][0] ? [...this.props.ordersStore.OrdersMonths.values()][0].months : ''
    let months = [...this.props.ordersStore.OrdersMonths.values()]
    this.setState({
      currentStatus: currentStatus,
      months: months,
    })
    let ordersByMonth = this.sortOrdersByMonth()
    // console.log(currentStatus, [...ordersByMonth.values()])
    this.parseOrderByPayment(ordersByMonth.get(currentStatus)||[], currentStatus)
  }

  sortOrdersByMonth(){
    const SaleOrdersDetailArray = this.state.SaleOrdersDetail
    // console.log(SaleOrdersDetailArray)
    let ordersByMonth = new Map()
    for(let ele of SaleOrdersDetailArray){
      let datetime = ele.dateTime.substring(0, 7)
      let orderInMap = ordersByMonth.get(datetime)
      if(orderInMap){
        orderInMap.push(ele)
        ordersByMonth = ordersByMonth.set(datetime, orderInMap)
      }else{
        ordersByMonth = ordersByMonth.set(datetime, [ele])
      }
    }
    this.setState({
      ordersByMonth: ordersByMonth
    })
    return ordersByMonth
  }

  parseOrderByPayment(orders, month){
    if(!orders) return 
    let orderStatisticByDay = new Map()
    let splitedOrders = []
    for(let order of orders){
      let datetime = order.dateTime.substring(5, 10)
      let payments = JSON.parse(order.paymentImg)||[]
      let subOrder = order
      for(let p of payments){
        let payment = JSON.parse(p.fileclass)
        let currency = payment.paymentCurrency 
        let amount = payment.paymentMount*1
        let KPIAmount = amount * this.props.helperStore.getCurrencyByName(currency).rateToRMB
        subOrder.shortDatetime = datetime
        subOrder.currency = currency
        subOrder.amount = amount 
        subOrder.KPIAmount = KPIAmount
        subOrder.KPI = amount/1000.0
        splitedOrders.push(subOrder)

        let daysStatistic = orderStatisticByDay.get(datetime)
        if(daysStatistic){
          let currencyStatistic = daysStatistic.get(currency)
          if(currencyStatistic){
            currencyStatistic += amount
            daysStatistic = daysStatistic.set(currency, currencyStatistic)
          }else{
            let currencyStatistic = 0
            daysStatistic = daysStatistic.set(currency, currencyStatistic)
          }
          let KPIStatistic = daysStatistic.get('KPIAmount')
          if(KPIStatistic){
            KPIStatistic += KPIAmount
            daysStatistic = daysStatistic.set('KPIAmount', KPIStatistic)
          }else{
            let KPIStatistic = 0
            daysStatistic = daysStatistic.set('KPIAmount', KPIStatistic)
          }
          orderStatisticByDay = orderStatisticByDay.set(datetime, daysStatistic)
        }else{
          //add init set here
          daysStatistic = new Map()
          daysStatistic = daysStatistic.set(currency, amount)
          daysStatistic = daysStatistic.set('KPIAmount', KPIAmount)
          orderStatisticByDay = orderStatisticByDay.set(datetime, daysStatistic)
        }
      }
    }
    let newSplitedOrders = this.state.splitedOrders.set(month, splitedOrders)
    let newOrderStatisticByDay = this.state.orderStatisticByDay.set(month, orderStatisticByDay)
    this.setState({
      orderStatisticByDay: newOrderStatisticByDay,
      splitedOrders: newSplitedOrders
    })
  }

  handleChange = (event, currentStatus) => {
    this.setState({ currentStatus: currentStatus });
    if(!this.state.orderStatisticByDay.get(currentStatus))
      this.parseOrderByPayment(this.state.ordersByMonth.get(currentStatus), currentStatus)
  };

  generateMonthRevenue(orders){
    if(!orders) return 
    let sortedOrders = orders.sort((a,b)=> a.shortDatetime>b.shortDatetime? 1 : -1)
    let totalRMB = 0
    let monthSpliteOrder= []
    let monthKPI = []
    for(let o of sortedOrders){
      totalRMB += o.KPIAmount
      monthSpliteOrder.push(
        <TableRow hover>
          <TableCell padding='dense'>{o.shortDatetime}</TableCell>
          <TableCell padding='dense'>{o.currency}: {o.amount}</TableCell>
          <TableCell padding='dense'>{o.idorder} {o.name}</TableCell>
        </TableRow>
      )
      monthKPI.push(
        <TableRow hover>
          <TableCell padding='dense'>{o.shortDatetime}</TableCell>
          <TableCell padding='dense'>{parseFloat(Math.round(o.KPIAmount/10) / 100).toFixed(2) } K</TableCell>
          <TableCell padding='dense'>{o.idorder} {o.name}</TableCell>
        </TableRow>
      )
    }
    return {totalRMB, monthSpliteOrder, monthKPI}
  }

  generateDaysRevenue(statistic){
    let days = [...statistic.keys()].sort((a,b)=> a>b? -1 : 1)
    let currencies = this.props.helperStore.currencyArray.filter(ele=>ele.status==='valid').map(c => c.currency).sort((a,b)=>a>b?1:-1)
    let daysStatistic = []
    let daysKPIStatistic = []

    let latestDay = statistic.get(days[0])||new Map()
    let latestDayStatistic =  
      <div>{days[0]} 
        {currencies.map(key=>{ 
            return(
                <span className={this.props.classes.paddingLeft}>{key}:<span className={this.props.classes.underline}>{Math.round(latestDay.get(key)||0) } </span></span>
            )
        })}
      </div>
    let latestDayKPIStatistic = 
        <div>
          <span >{days[0]}: <span className={this.props.classes.underline}>{Math.round((latestDay.get('KPIAmount')||0)/10) / 100} K</span></span>
        </div>
    days.splice(0,1)
    let daysStatisticHead =
        <TableRow>
          <TableCell padding='dense'>Date</TableCell>
          {
            currencies.map(key=>{
              // if(s.get(key))
                return (
                  <TableCell padding='dense'>{key.replace(' ','')}</TableCell>
                )
              }
            )
          }
        </TableRow>
    
    for(let d of days){
      console.log(d, statistic.get(d))
      const s = statistic.get(d)||new Map()
      daysStatistic.push(
        <TableRow hover>
          <TableCell padding='dense'>{d}</TableCell>
          {
            currencies.map(key=>{
              // if(s.get(key))
                return (
                  <TableCell padding='dense'>{s.get(key)||0}</TableCell>
                )
              }
            )
          }
        </TableRow>
      )
      daysKPIStatistic.push(
        <TableRow hover>
          <TableCell padding='dense'>{d}</TableCell>
          <TableCell padding='dense'>{Math.round((s.get('KPIAmount')||0)/10) / 100} K</TableCell>
        </TableRow>
      )
    }
    return {latestDayStatistic, daysStatisticHead, daysStatistic, latestDayKPIStatistic, daysKPIStatistic}
  }

  generateMonthRevenueByCurrency(statistic){
    let monthRevenueByCurrency = []
    let currencies = this.props.helperStore.currencyArray.map(c => c.currency)
    let revenueByCurrencyMap = new Map()
    for(let currency of currencies){
      revenueByCurrencyMap = revenueByCurrencyMap.set(currency, 0)
    }
    for(let [day, daysRevenue] of statistic.entries()){
      for(let [currency, amount] of daysRevenue.entries()){
        if(currency==='KPIAmount') continue 
        let currencyAccumulate = revenueByCurrencyMap.get(currency)
        if(currencyAccumulate){
          currencyAccumulate = currencyAccumulate + amount||0
          revenueByCurrencyMap = revenueByCurrencyMap.set(currency, currencyAccumulate)
        }else{
          revenueByCurrencyMap = revenueByCurrencyMap.set(currency,  amount||0)
        }
      }
    }
    console.log(currencies, revenueByCurrencyMap)
    for(let [key, value] of revenueByCurrencyMap.entries()){
      monthRevenueByCurrency.push({currency: key, value: value})
    }
    return monthRevenueByCurrency.sort((a,b)=>a.currency>b.currency? 1:-1)
  }

  
  render(){
    const { classes, authStore, ordersStore } = this.props;
    const { currentStatus, months, } = this.state;
    const monthDays = moment().format('YYYY-MM-DD').substring(5, 10)
    if(currentStatus === '') return (<p></p>)
    const orders = this.state.splitedOrders.get(currentStatus)
    const statistic = this.state.orderStatisticByDay.get(currentStatus)
    const header = 
      <Tabs 
        value={currentStatus} 
        onChange={this.handleChange}
        indicatorColor="primary"
        textColor="primary"
        className={classes.tabsRoot}
        >
        { months.map((item, key) => <Tab key={key} value={item.months} label={item.months} /> )}
      </Tabs>

    // console.log([orders])
    // console.log(statistic)
    if(!orders && !statistic) return(
      <Grid container className={classes.root}>
        {header}
        {/* Month overview */}
        <Grid container className={classes.root} >
          <p>No data avilable</p>
        </Grid>
      </Grid>)
    

    let {totalRMB, monthSpliteOrder, monthKPI} = this.generateMonthRevenue(orders)
    let {latestDayStatistic, daysStatisticHead, daysStatistic, latestDayKPIStatistic, daysKPIStatistic}  = this.generateDaysRevenue(statistic)
    let monthRevenueByCurrency = this.generateMonthRevenueByCurrency(statistic)

    return(
      <Grid container className={classes.root}>
        {header}
        {/* Month overview */}
        <Grid container className={classes.root} >
          <Grid  item xs={6}>
            <ExpansionPanel className={classes.panel}>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <div>Revenue Receipt: <span className={classes.underline}>{Math.round(totalRMB) } RMB</span></div>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Table>
                  <TableBody>
                  {monthSpliteOrder}
                  </TableBody>
                </Table>
              </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel className={classes.panel}>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                {latestDayStatistic}
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Table>
                  <TableHead>
                    {daysStatisticHead}
                  </TableHead>
                  <TableBody>
                    {daysStatistic}
                  </TableBody>
                </Table>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </Grid>
          <Grid  item xs={6} >
            <ExpansionPanel className={classes.panel}>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <div>KPI: <span className={classes.underline}>{(Math.round(totalRMB/10)/100)} K</span></div>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Table>
                  <TableBody>
                    {monthKPI}
                  </TableBody>
                </Table>
              </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel className={classes.panel}>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                <div variant="subheading" className={classes.heading}>{latestDayKPIStatistic}</div>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <Table>
                  <TableBody>
                    {daysKPIStatistic}
                  </TableBody>
                </Table>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </Grid>
        </Grid>
        {/* Current days overview */}
        <Grid container className={classes.root} >
          <Grid container item xs={6}>

            <ResponsiveContainer width={'100%'} height={500} layout="vertical">
              <BarChart
                layout="vertical"
                data={monthRevenueByCurrency}
                margin={{
                  top: 5, right: 30, left: 20, bottom: 5,
                }}
              >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis  type="number"/>
                <YAxis type="category" dataKey="currency" />
                <Tooltip />
                <Legend />
                <Bar dataKey="value" fill="#82ca9d" label/>
              </BarChart>
            </ResponsiveContainer>
          </Grid>
          <Grid container item xs={6}>
            <PieChart width={400} height={400}>
              <Pie
                data={data}
                cx={200}
                cy={200}
                labelLine={false}
                label={renderCustomizedLabel}
                outerRadius={80}
                fill="#8884d8"
                dataKey="value"
              >
                {
                  data.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
                }
              </Pie>
            </PieChart>
          </Grid>
        </Grid>
      </Grid>
    );
  }
} 

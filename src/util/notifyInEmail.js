import API_URL from '../config'

const notifyInEmail = (api, email, userName, orderName, urls, textName, textContent) =>{
    const data = {
        email: email,
        userName: userName,
        name: orderName,
        urls: JSON.parse(urls),
        textName: textName,
        textContent: textContent
    }
    // console.log('Xieshou ', data)

    return fetch(`${API_URL.ROOT_API_URL}/${api}`, {
        method: 'POST',
        mode: 'cors',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data)
    })
}

const notifyXieshouInEmail = (email, userName, orderName, urls, textName, textContent) => notifyInEmail('mailXieshou', email, userName, orderName, urls, textName, textContent)
const notifyCustomerInEmail = (email, userName, orderName, urls, textName, textContent) => notifyInEmail('mailCustomer', email, userName, orderName, urls, textName, textContent)

export {notifyXieshouInEmail, notifyCustomerInEmail}
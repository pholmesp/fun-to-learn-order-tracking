import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import moment from 'moment'
import momenttz from 'moment-timezone'
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import {gray, bginfo, bgwarning, bgerror, } from '../common/Colors'

const styles = theme => ({
    bgerror:{
      backgroundColor: bgerror,
    },
    label:{
      padding: '3px',
      paddingLeft: '5px',
      paddingRight: '5px',
      marginRight: '5px',
      marginTop: '4px',
      borderRadius: 3,
      fontSize: '12px',
      color: gray, 
      textDecoration: 'None',
    },
    list:{
      color: gray,
      padding: '3px',
      fontSize: '14px',
      textDecoration: 'underline',
    },
    span: {
      fontSize: '14px',
      color: gray,
      paddingLeft: '5px'
    },
    link: {
        cursor: 'pointer'
    }
  });

@withStyles(styles)
@inject('helperStore', 'authStore')
@withRouter
@observer
class GenerateFileLinkView extends Component{
    render(){
        let filesArray = this.props.files
        let fileMap = new Map()
        if(typeof(filesArray) == 'string') filesArray = JSON.parse(filesArray)
        filesArray = filesArray||[]
        // console.log(filesArray)
        for(let i=0; i<filesArray.length; i++){
        let file = filesArray[i]
        const size = file.size/1024 >= 1024? Math.round(file.size/(1024*1024)*100)/100 + ' MB': Math.round(file.size/1024*100)/100+' KB'
        let fileTypeArray = fileMap.get(file.fileclass)
        // console.log(fileTypeArray)
        if(fileTypeArray){
            let currentSize = fileTypeArray.length
            fileTypeArray.push(
            <li key={i} >{currentSize+1}. &nbsp; 
            {
                this.props.authStore.currentUser.role.indexOf('saleman') < 0 || this.props.authStore.currentUser.role.indexOf('services') < 0 ? 
                <a href={this.props.helperStore.ROOT_API_URL + '/img/' + file.filename} key={i} className={this.props.classes.list} download={file.originalname} >
                {file.originalname} - <span className={this.props.classes.span}>{size}</span>
                </a> 
                :
                <i>
                {file.originalname} - <span className={this.props.classes.span}>{size}</span>
                </i> 
            }
                &nbsp; 
                <span className={this.props.classes.label}>{moment(file.filename.split('-')[0]/1).tz(momenttz.tz.guess()).format('YYYY-MM-DD HH:mm:ss')}</span>
            </li>
            )
            fileMap.set(file.fileclass, fileTypeArray)
        }else{
            fileTypeArray = []
            fileTypeArray.push(
            <li key={i} >1. &nbsp; 
            {
                this.props.authStore.currentUser.role.indexOf('saleman') < 0 || this.props.authStore.currentUser.role.indexOf('services') < 0 ? 
                <a href={this.props.helperStore.ROOT_API_URL + '/img/' + file.filename} key={i} className={this.props.classes.list} download={file.originalname} >
                {file.originalname} - <span className={this.props.classes.span}>{size}</span>
                </a> 
                :
                <i>
                {file.originalname} - <span className={this.props.classes.span}>{size}</span>
                </i> 
            }
                &nbsp; 
                <span className={this.props.classes.label}>{moment(file.filename.split('-')[0]/1).tz(momenttz.tz.guess()).format('YYYY-MM-DD HH:mm:ss')}</span>
            </li>
            )
            fileMap.set(file.fileclass, fileTypeArray)
        }
        }
        let fileLink = []
        // console.log([...fileMap.keys()], [...fileMap.values()])
        for(let key of fileMap.keys()){
        // console.log(key, fileMap.get(key))
        fileLink = fileLink.concat([<Typography  variant="subheading" key={key+'-class'}  gutterBottom> <span className={[this.props.classes.label , this.props.classes.Important].join(' ')}>{key}</span></Typography>])
        fileLink = fileLink.concat(fileMap.get(key))
        }
        // console.log(fileLink)
        return fileLink
  }
}

@withStyles(styles)
@inject('helperStore', 'authStore')
@withRouter
@observer
class GenerateFileLinkEdit extends Component{
    deleteArrayEle(array, filename){
        let newArray = []
        for(let ele of array){
          if(filename!==ele.filename) newArray.push(ele)
        }
        return newArray
    }
    handleFileDelete(filename){
        let fileUrl = this.props.files
        if(typeof(fileUrl) == 'string') fileUrl = JSON.parse(fileUrl)
        fileUrl = fileUrl||[]
        let newFileUrls = this.deleteArrayEle(fileUrl, filename)
        this.props.updateFileUrls(newFileUrls)
    }

    render(){
        let filesArray = this.props.files
        // console.log(filesObj)
        let fileMap = new Map()
        if(typeof(filesArray) == 'string') filesArray = JSON.parse(filesArray)
        filesArray = filesArray||[]
        // console.log(filesArray)
        for(let i=0; i<filesArray.length; i++){
        let file = filesArray[i]
        const size = file.size/1024 >= 1024? Math.round(file.size/(1024*1024)*100)/100 + ' MB': Math.round(file.size/1024*100)/100+' KB'
        let fileTypeArray = fileMap.get(file.fileclass)
        // console.log(fileTypeArray)
        if(fileTypeArray){
            let currentSize = fileTypeArray.length
            fileTypeArray.push(
            <li key={i} >{currentSize+1}. &nbsp; 
            {
                this.props.authStore.currentUser.role.indexOf('saleman') < 0 || this.props.authStore.currentUser.role.indexOf('services') < 0 ? 
                <a href={this.props.helperStore.ROOT_API_URL + '/img/' + file.filename} key={i} className={this.props.classes.list} download={file.originalname} >
                {file.originalname} - <span className={this.props.classes.span}>{size}</span>
                </a> 
                :
                <i>
                {file.originalname} - <span className={this.props.classes.span}>{size}</span>
                </i> 
            }
                
                &nbsp; 
                <span className={this.props.classes.label}>{moment(file.filename.split('-')[0]/1).tz(momenttz.tz.guess()).format('YYYY-MM-DD HH:mm:ss')}</span>
                &nbsp;
                <span role="button" className={[this.props.classes.label, this.props.classes.bgerror, this.props.classes.link].join(' ')} onClick={() => this.handleFileDelete(file.filename)}>
                    DELETE
                </span>
            </li>
            )
            fileMap.set(file.fileclass, fileTypeArray)
        }else{
            fileTypeArray = []
            fileTypeArray.push(
            <li key={i} >1. &nbsp; 
            {
                this.props.authStore.currentUser.role.indexOf('saleman') < 0 || this.props.authStore.currentUser.role.indexOf('services') < 0 ? 
                <a href={this.props.helperStore.ROOT_API_URL + '/img/' + file.filename} key={i} className={this.props.classes.list} download={file.originalname} >
                {file.originalname} - <span className={this.props.classes.span}>{size}</span>
                </a> 
                :
                <i>
                {file.originalname} - <span className={this.props.classes.span}>{size}</span>
                </i> 
            }
                &nbsp; 
                <span className={this.props.classes.label}>{moment(file.filename.split('-')[0]/1).tz(momenttz.tz.guess()).format('YYYY-MM-DD HH:mm:ss')}</span>
                &nbsp;
                <span role="button" className={[this.props.classes.label, this.props.classes.bgerror, this.props.classes.link].join(' ')} onClick={() => this.handleFileDelete(file.filename)}>
                    DELETE
                </span>
            </li>
            )
            fileMap.set(file.fileclass, fileTypeArray)
        }
        }
        let fileLink = []
        // console.log([...fileMap.keys()], [...fileMap.values()])
        for(let key of [...fileMap.keys()]){
            // console.log(key, fileMap.get(key))
            fileLink = fileLink.concat([<Typography  variant="subheading" key={key}  gutterBottom> {key}</Typography>])
            fileLink = fileLink.concat(fileMap.get(key))
        }
        // console.log(fileLink)
        return fileLink
    }
}

export { GenerateFileLinkView, GenerateFileLinkEdit } 
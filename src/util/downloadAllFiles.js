import JSZip from 'jszip'
import { saveAs } from 'file-saver';



const downloadAllFiles = async (zipName, fileArray, txtFile, txtContent) => {
  let zip = new JSZip()
  let urlArray = fileArray
  if(typeof(urlArray) == 'string') urlArray = JSON.parse(urlArray)
  urlArray = urlArray||[]
  let zipPackage = zip.folder(zipName)
  for(let url of urlArray){
      let res =  await fetch( 'https://console.funtolearn.co/api/img/'+url.filename, { encode: null, mimeType: 'text/plain; charset=x-user-defined'});
      // let res =  await fetch( 'http://localhost:3000/api/img/'+url.filename, { encode: null, mimeType: 'text/plain; charset=x-user-defined'});
      // console.log(typeof(res), res)
      zipPackage.file(url.originalname, res.blob(), {binary:true})
      // saveAs('http://localhost:3000/api/img/'+url.filename, url.filename)
  }
  zipPackage.file(txtFile, txtContent)
  zip.generateAsync({type:"blob"}).then(zipFile=>saveAs(zipFile, zipName+'.zip'))
}
export default downloadAllFiles
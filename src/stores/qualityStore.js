import { observable, action } from 'mobx';
import moment from 'moment'
import orderResultStore from './orderResultStore'
import helperStore from './helperStore'
import authStore from './authStore'

import agent from '../agent';

class QualityStore {
    @observable isLoading = false
    @observable checkQuestions = []
    @observable orderQaInvitation= {}

    @observable idorderResult
    @observable idorder
    @observable description = ''
    @observable files = []
    @observable iduser
    @observable datetime = moment().format('YYYY-MM-DDTHH:mm:ss')


    @action setIdorder(idorder){
        this.idorder = idorder
        this.loadCheckQuestions(idorder)
    }
    @action setRecordStatus(id, mark){
        this.checkQuestions.map(action(ele=>{
            if(ele.idcheckQuestion === id){
                console.log(mark)
                ele['status'] = mark
            }
        }))
    }

    @action init(){
    }

    @action loadCheckQuestions(idorder){
        agent.Orders.getInvitations(idorder).then(
            action( invitations => {
              if(invitations.length>0){
                this.orderQaInvitation = invitations.filter(ele => ele.role==='qa')[0]
                if(this.orderQaInvitation.schema&&(this.orderQaInvitation.schema||'').length>0) {
                    this.checkQuestions = JSON.parse(this.orderQaInvitation.schema)
                }else{
                    agent.Quality.getCheckQuestions()
                    .then(action((res) => {
                      this.checkQuestions = res.map(ele => {ele['status']='NO'; return ele})
                    }))
                }
              }
            })
          ) 
      }
    @action submitCheckQuestion(){
        this.isLoading = true
        this.orderQaInvitation['schema'] = JSON.stringify(this.checkQuestions)
        return agent.Orders.updateInvitation(this.orderQaInvitation)
        .then(action(res =>{
            if(res.statusCode !== 200){
                window.alert('Submit Error, please refresh and try again. If the error happens repeatly, please contact your admin to report. ')
            }
            return res.statusCode
        }))
        .finally(action(()=>this.isLoading=false))
    }


    @action uploadFiles(files, filesclass='Attachements:'){
        console.log(files)
        this.isLoading = true
        const formData = new FormData()
        for(let i=0; i<files.length; i++){
            formData.append('file', files[i])
        }
        formData.append('fileclass', filesclass)
        helperStore.uploadFiles(formData).then(res =>{
            this.files = this.files.concat(res)
        }).finally(action(() => this.isLoading = false))
    }
    @action updateFiles(files){
        this.files = files
    }
    @action sumitOrderResult(){
        this.isLoading = true
        let orderResult = {}
        orderResult['idorderResult'] = this.idorderResult
        orderResult['idorder'] = this.idorder
        orderResult['iduser'] = authStore.currentUser.iduser
        orderResult['description'] = this.description
        orderResult['files'] = JSON.stringify(this.files)
        orderResult['datetime'] = moment().format('YYYY-MM-DDTHH:mm:ss')
        agent.Quality.sumitOrderResult(orderResult).then(action(res =>{
            if(res.statusCode === 200){
                this.idorderResult = res.insertId
                // this.handleUpdateOrderStatus(orderResult['idorder'])
                orderResultStore.getOrderResultList(orderResult['idorder'], false)
            }else{
                window.alert('Submit Error, please refresh and try again. If the error happens repeatly, please contact your admin to report. ')
            }
        })).finally(action(() => this.isLoading = false))
    }
    @action updateText(text){
        this.description = text
    }

}

export default new QualityStore()
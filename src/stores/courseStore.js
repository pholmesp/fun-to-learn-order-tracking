import { observable, action, computed, createTransformer } from 'mobx';
import agent from '../agent';

const LIMIT = 100;

export class CourseStore {

  @observable isLoading = false;
  @observable courses = observable.map();


  @computed get Courses() {
    return this.courses.values();
  };
  @computed get getCourseById(){
    return createTransformer(id=>this.courses.get(id)||{})
  }

  @action init(){
    this.loadCourses()
  }

  @action loadCourses() {
    this.isLoading = true;
    return agent.Course.all()
      .then(action((Courses) => {
        this.courses.clear();
        Courses.forEach(course => this.courses.set(course.idcource, course));
      }))
      .finally(action(() => { this.isLoading = false; }));
  }

  @action createCourse(course) {
    this.isLoading = true;
    console.log(course)
    return agent.Course.create(course)
    .then(action(res =>{
      this.loadCourses()
      return res
    }))
    .finally(action(() => { this.isLoading = false;}))
  }

  @action deleteCourse(idcourse) {
    this.isLoading = true;
    return agent.Course.del(idcourse)
      .then(action((res) => {
        this.loadCourses()
        return res
      }))
      .finally(action(() => { this.isLoading = false; }));
  }
  // @action updateCourse(orderObj) {
  //   this.isLoading = true;
  //   console.log(orderObj)
  //   return agent.Course.update(orderObj) ////obj likes {key=key, value=value, idorder=idorder}
  //   .then(action(res =>{
  //     this.loadCourses()
  //     return res
  //   }))
  //   .finally(action(() => { this.isLoading = false;}))
  // }

}

export default new CourseStore();

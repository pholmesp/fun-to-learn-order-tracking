import { observable, computed, action, createTransformer } from 'mobx';
import agent from '../agent';
import authStore from './authStore'


class statisticStore {
  @observable isLoading;
  @observable orderStatistic = observable.map();


  @computed get OrderStatisticByCountryMonth(){
    return createTransformer(({country, month})=> {
      console.log(country, month)
      let statistic = this.orderStatistic.get(country+'-'+month)
      if(statistic){
        return [...statistic.values()]
      }else{
        this.getOrderStatisticByCountryMonth(country, month)
        return []
      }
    })
  }

  @action init(){
    // this.getAllPriceRequest()
    // if(authStore.currentUser.name){
    //   this.getPriceQuoteByIdUser(authStore.currentUser.iduser)
    // }
  }

  @action getOrderStatisticByCountryMonth(country, month){
    this.isLoading = true
    return agent.Statistic.getOrderStatisticByCountryMonth(country, month)
    .then(
      action( res => {
        if(res) this.orderStatistic.set(country+'-'+month, res)
      })
    ).finally(action(() => { this.isLoading = false; }))
  }
}

export default new statisticStore();

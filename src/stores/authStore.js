import { observable, action, computed, reaction, createTransformer } from 'mobx';
import agent from '../agent';
import ordersStore from './ordersStore'
import config from "../config";
import customerStore from "./customerStore";
import {notification} from "antd";

class AuthStore {
  @observable currentUser = JSON.parse(window.localStorage.getItem('current'))||{};
  @observable token = window.localStorage.getItem('jwt');
  @observable isLoading = false;
  @observable errors = undefined;
  @observable ws = undefined;
  @observable ns = observable.map();
  // @observable notifications = [];
  @observable notifications = observable.map();
  @observable notifiTypes = [
      "Order payment has been verified by Admin.",
      "Order has been canceled by QA.",
      "Order status has been modified by Admin.",
      "Order has been assigned to Counsellor by Admin.",
      "Order has been assigned to QA by Admin.",
      "Unread message",
      "Error!"
  ]

  constructor() {
    reaction(
      () => this.token,
      token => {
        if (token) {
          window.localStorage.setItem('jwt', token);
        } else {
          window.localStorage.removeItem('jwt');
        }
      }
    );
  }

  @action init(){

    if(this.currentUser.name){
     const URL = config.WS_URL;
       this.ws = new WebSocket(URL)
      console.log(this.currentUser)
      return agent.Account.getSaleWeChat(this.currentUser.iduser)
          .then ((res) => {
            if (res.length>0 && res[0].wechatName){
              let result = Object.keys(JSON.parse(res[0].wechatName)).map(key =>`${key}(${JSON.parse(res[0].wechatName)[key]})`).join('|');
              this.currentUser['wechat'] = result
              // this.currentUser['wechatName'] = result
              window.localStorage.setItem('current', JSON.stringify(this.currentUser))
            }
          })
          .then(()=>{
            if(this.currentUser.role.indexOf('counsellor')>=0){
              ordersStore.getUserOrders(this.currentUser.iduser)
              ordersStore.getUserOrdersDetail(this.currentUser.iduser)
            }

            if(this.currentUser.role.indexOf('saleman')>=0){
              //console.log("afasfas   "+this.currentUser.iduser)
              customerStore.init();
              ordersStore.getSalesOrdersDetail(this.currentUser.iduser)
              ordersStore.getSaleOrdersResult(this.currentUser.iduser)
              ordersStore.loadSalePackage(this.currentUser.iduser)
            }
          })
    }
  }


  @action login(email, passwd) {
    this.isLoading = true;
    this.errors = undefined;
    return agent.Auth.login(email, passwd)
    .then((user) => {
      this.currentUser = user
      console.log(user.wechatName)
      this.currentUser['role'] = user.role.split('|')
      window.localStorage.setItem('current', JSON.stringify(this.currentUser))
    }).then(res =>{
      this.init()
    })
    .catch(action((err) => {
      console.log( err.response, err.response.body,err.response.body )
      this.errors = err.response && err.response.body && err.response.body.errors;
      if(err.response.statusCode === 401) alert('Email or password is incorrect, please try again ... ')
      else alert("Sign In Error, please refesh page and try again ... ")
      throw err;
    }))
    .finally(action(() => { this.isLoading = false; }));
  }

  @action updateUser(newUser) {
    this.isLoading = true;
    return agent.Auth.save(newUser)
      .then(action((user) => { 
        this.currentUser = user; 
        this.currentUser['role'] = user.role.split('|')
      }))
      .finally(action(() => { this.isLoading = false; }))
  }

  @action forgetUser() {
          // on connecting, do nothing but log it to the console
          console.log('connected' +this.currentUser.iduser)
          let message={
              idUser:this.currentUser.iduser,
              type:'close'
          }
          this.ws.send(JSON.stringify(message))
      this.notifications.clear()

    window.localStorage.setItem('current', '{}')
    this.currentUser = {};
  }

  @action logout() {
    this.forgetUser();

  }

  @action addNotificaitons(notifications) {
    this.isLoading = true;


    notifications.forEach(ele => {
      //console.log(ele)
      this.ns.set(ele.idmessageQueue, ele)
    });
    let sortedKey = Array.from(this.ns.values()).sort((a, b) =>{
      return b.idmessageQueue - a.idmessageQueue
    })
    this.notifications = sortedKey
    //this.notifications = this.ns
    //console.log(sortedKey)
    console.log(this.notifications)
        this.isLoading = false;
  }
  @action refreshUser(email){
    console.log(this.currentUser)
    this.isLoading=true;
      return agent.Account.refreshUser(email)
          .then((res) => {
                console.log(JSON.stringify(res[0].role.split('|')))

            console.log("refresh 1"+ res+ res[0])
            //this.currentUser = res[0]
            console.log("refresh "+ res[0])
            this.currentUser['note'] = res[0].note
            this.currentUser['phone'] = res[0].phone
            window.localStorage.setItem('current', JSON.stringify(this.currentUser))
          })
          .finally(action(()=>{this.isLoading=false;}))
  }

  @action updateUserNote(note,iduser,email){
    this.isLoading=true;
    return agent.Account.updateUserNote(note,iduser)
        .then(this.refreshUser(email))
        .finally(action(()=>{this.isLoading=false;}))
  }
  @action updateUserPhone(phone,iduser,email){
    this.isLoading=true;
    return agent.Account.updateUserPhone(phone,iduser)
        .then(this.refreshUser(email))
        .finally(action(()=>{this.isLoading=false;}))
  }
}

export default new AuthStore();

import { observable, computed, action, createTransformer } from 'mobx';
import helperStore from './helperStore'
import userStore from './userStore'
import customerStore from './customerStore'
import ordersStore from './ordersStore'
import agent from '../agent';
import authStore from './authStore'
import {notifyCustomerInEmail} from '../util/notifyInEmail'
import {addOrderComment} from '../util/addOrderComment'

import moment from 'moment'


class OrderResultStore {
    @observable isLoading = false
    @observable orderResultIsLoading = false 
    @observable orderStoreIsLoading = false
    @observable idorderResult
    @observable idorder
    @observable description = ''
    @observable files = []
    @observable iduser
    @observable datetime = moment().format('YYYY-MM-DDTHH:mm:ss')
    @observable orderResultList = observable.map()
    @observable orderScore = observable.map()

    @action init(){}

    @action sendToCustomer(order, orderResult){
        if(window.confirm('Are you sure to send this result to customer? ')){
            // userEmail, userName, orderName, orderResult
            const customer = customerStore.getCustomerById(order.idcustomer)
            console.log(order.idcustomer, customer)
            const processor = userStore.getUserById(orderResult.iduser)
            const mail_subject = processor.role.split(':').indexOf('qa')>=0 ? `Quality-Check: ${order.name}` : `Assignment Result: ${order.name}`
            if(!customer.customerEmail) {
                alert(`Customer ${customer.customerName}'s email does not exist, please add the email first. `)
                return
            }
            if(order.paymentStatus === 'full paid' && order.paymentVerify === 'verified'){
                this.orderResultIsLoading = true
                notifyCustomerInEmail(customer.customerEmail, customer.customerName, mail_subject, orderResult.files, 'description.txt', orderResult.description)
                .then(res => {
                    if(res.status === 200){
                        alert(`Order result has been sent to ${customer.customerName} in ${customer.customerEmail}.`)
                        addOrderComment(order, authStore.currentUser.iduser, `${authStore.currentUser.name}(${authStore.currentUser.iduser}) send order result to ${customer.customerName} in ${customer.customerEmail}. `)
                    }else{
                        alert(`Send Result Error!!!  please check ${customer.customerName}\'s email ${customer.customerEmail}, and contact with ${customer.customerName} in other way to send result. `)
                    }
                }).finally(()=>this.orderResultIsLoading = false)
            }else if(order.paymentStatus !== 'full paid'){
                alert('The order does not FULL PAID, please contact sale to update the payment as full paid. ')
            }else if(order.paymentVerify !== 'verified'){
                alert('The full payment does NOT verified!, please verify (admin or account) the payment first! ')
            }
        }
    }
    @action getOrderResultList(idorder, useCache=true){
        this.idorder = idorder
        const cache = this.orderResultList.get(idorder)
        if(useCache && cache){
            return useCache
        }
        else{
            this.orderResultIsLoading = true
            agent.Quality.getOrderResultList(idorder)
            .then(action((res) => {
              this.orderResultList.set(idorder, res)
            })).finally(action(()=> this.orderResultIsLoading = false))
        } 
        this.getOrderScore(idorder)
      }

      @action getOrderScore(idorder, useCache=true){
        const cache = this.orderScore.get(idorder)
        if(useCache && cache){
            return useCache
        }
        else{
            this.orderStoreIsLoading = true
            agent.Orders.getOrderResultByOrderId(idorder)
            .then(action((res) => {
                const temp = res.filter(ele=>ele.confirm==1)[0]||{}
                if(temp){
                    this.orderScore.set(idorder, temp)
                }
            })).finally(action(()=> this.orderStoreIsLoading = false))
        } 
      }

    @action updateDescription(text){
        this.description = text
    }
    @action syncOrderResult(orderResult){
        console.log(orderResult)
        this.idorderResult = orderResult.idorderResult
        this.iduser = orderResult.iduser
        this.idorder = orderResult.idorder
        this.description = orderResult.description
        if(typeof(orderResult.files) == 'string') this.files = JSON.parse(orderResult.files)
        else this.files = []
        this.datetime = orderResult.datetime
    }
    @action uploadFiles(files, filesclass='Attachements:'){
        console.log(files)
        this.isLoading = true
        const formData = new FormData()
        for(let i=0; i<files.length; i++){
            formData.append('file', files[i])
        }
        formData.append('fileclass', filesclass)
        helperStore.uploadFiles(formData).then(res =>{
            this.files = this.files.concat(res)
        }).finally(action(() => this.isLoading = false))
    }
    @action updateFiles(files){
        this.files = files
    }
    @action sumitOrderResult(){
        this.isLoading = true
        let orderResult = {}
        orderResult['idorderResult'] = this.idorderResult
        orderResult['idorder'] = this.idorder
        orderResult['iduser'] = authStore.currentUser.iduser
        orderResult['description'] = this.description
        orderResult['files'] = JSON.stringify(this.files)
        orderResult['datetime'] = moment().format('YYYY-MM-DDTHH:mm:ss')
        let order = Object.assign({}, ordersStore.getOrderById(this.idorder))
        agent.Quality.sumitOrderResult(orderResult).then(action(res =>{
            if(res.statusCode === 200){
                this.handleUpdateOrderStatus(orderResult['idorder']).then(action(res =>{
                    if(res.statusCode === 200){
                        this.getOrderResultList(orderResult['idorder'], false)
                        console.log(this.idorder)
                        ordersStore.emailSale(this.idorder, "","" ,"" ,2).then(action(res =>{
                            if(res.statusCode === 200){

                                // userEmail, userName, orderName, orderResult
                                const customer = customerStore.getCustomerById(order.idcustomer)
                                console.log(order.idcustomer, customer)
                                const processor = userStore.getUserById(orderResult.iduser)
                                const mail_subject = processor.role.split(':').indexOf('qa')>=0 ? `Quality-Check: ${order.name}` : `Assignment Result: ${order.name}`
                                if(!customer.customerEmail) {
                                    alert(`Customer ${customer.customerName}'s email does not exist, please add the email first. `)
                                    return
                                }
                                if(order.paymentStatus === 'full paid' && order.paymentVerify === 'verified'){
                                    this.orderResultIsLoading = true
                                    notifyCustomerInEmail(customer.customerEmail, customer.customerName, mail_subject, orderResult.files, 'description.txt', orderResult.description)
                                        .then(res => {
                                            if(res.status === 200){
                                                alert(`Order result has been sent to customer.`)
                                                addOrderComment(order, authStore.currentUser.iduser, `${authStore.currentUser.name}(${authStore.currentUser.iduser}) send order result to ${customer.customerName} in ${customer.customerEmail}. `)
                                            }else{
                                                alert(`Send Result Error!!!  please contact admin for help. `)
                                            }
                                        }).finally(()=>{
                                            this.orderResultIsLoading = false
                                            this.isLoading = false
                                        })
                                }else if(order.paymentStatus !== 'full paid'){
                                    alert('The order does not FULL PAID, please contact sale to update the payment as full paid. ')
                                    this.isLoading = false
                                }else if(order.paymentVerify !== 'verified'){
                                    alert('The full payment does NOT verified!, please verify (admin or account) the payment first! ')
                                    this.isLoading = false
                                }

                            }
                            else {
                                window.alert('Submit Successfully, but falied to notify salesman, please contact your admin to report. ')
                                this.isLoading = false
                            }
                        }))

                    }else{
                        window.alert('Submit Error, please refresh and try again. If the error happens repeatly, please contact your admin to report. ')
                        this.isLoading = false
                    }
                }))
            }else{
                window.alert('Submit Error, please refresh and try again. If the error happens repeatly, please contact your admin to report. ')
                this.isLoading = false
            }
        }))
    }
    @action updateText(text){
        this.description = text
    }
    @action  handleUpdateOrderStatus(idorder) {
        let order = Object.assign({}, ordersStore.getOrderById(idorder))
        let orderObj = {} 
        orderObj['idorder'] = idorder
        let fieldStatus = {}
        let fieldCurrentProcessor = {}
        fieldStatus['key'] = 'status'
        fieldCurrentProcessor['key'] = 'currentProcessorRole'
        if((order.autoProcess===0 || order.autoProcess===null || order)) {fieldStatus['value'] = 'verifying'; fieldCurrentProcessor['value'] = 'qa'}
        if(order.autoProcess==1)
        {
            fieldStatus['value'] = 'complete'; fieldCurrentProcessor['value'] = 'customer'
        }
        orderObj['fields'] = [fieldStatus, fieldCurrentProcessor]
        // console.log(orderObj)
        return ordersStore.updateOrder(orderObj)
    }

    @action deleteResult(orderResult){
        if(window.confirm('Are you sure to delete this order result? ')){
            this.isLoading = true
            agent.Quality.deleteOrderResult(orderResult.idorderResult).then(action(res=>{
                if(res.statusCode === 200){
                    this.getOrderResultList(orderResult['idorder'], false)
                }else{
                    window.alert('Delete Order Result Error, please refresh and try again. If the error happens repeatly, please contact your admin to report. ')
                }
            })).finally(action(()=>this.isLoading=false))
        }
    }

    
}

export default new OrderResultStore()
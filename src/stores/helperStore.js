import { observable, action, computed, createTransformer } from 'mobx';
import agent from '../agent';
import config from '../config'

export class HelperStore {

  @observable isLoading = false;
  @observable ROOT_API_URL = config.ROOT_API_URL
  @observable files = []
  @observable currency = observable.map();
  @observable timezone = observable.map();
  @observable roles = [
    {label: 'Admin', value: "admin"},
    {label: 'Sales', value: "saleman"},
    {label: 'Xieshou', value: "counsellor"},
    {label: 'Customer Service', value: "services"},
    {label: 'Translator', value: "translater"},
    {label: 'QA', value: "qa"},
    {label: 'Exchanger', value: "exchanger"}]
  @observable fileClass = [ "Marking/Assessment Criteria", "Case Study",  "Guidance", "Lecture Note",  "Prescribed Material", "Reference", "Requirement",  "Samples/Model", "Suggested Readings", "Completed", "Feedback", "Other Materials"]
  @observable paymentType = ["Bound Payment", "Part Payment", "Full Payment"]
  @observable courseType = ["Accounting", "Arts", "Business", "Economics", "Education",  "Engineering", "Finance", "HRM", "History", "IT", "Language", "Law", "Management",  "Management-hotel",  "Management-", "Marketing", "Math",  "Media", "Politics", "Statistic", "other"]
  @observable assignmentType = ["Assignment", "Annotated Bibliography", "Case Study", "Discussion", "Draft", "Dissertation", "Essay", "Examination", "Proofreading", "Reflection", "IT Program", "Language Course", "Outline", "Persentation", "Plan", "Portfolio", "Proposal", "Quiz", "Report",  "Other"]
  @observable referenceType = ["APA", "MLA", "Harvard", "IEEE", "Chicago", "Turabian", "Other - specify at bottom", "Unknown"]
  @observable universities = [];
  @observable countryuni = [];
  @observable country = [];
  universityMap = {
    america: [
        { value: 'OSU, Ohio State University', label: 'OSU, Ohio State University'},
        { value: '肯特州立大学, Kent State University', label: '肯特州立大学, Kent State University'}
        ],
    'new zealand': [
        { value: '奥克兰理工大学, Auckland University of Technology', label: '奥克兰理工大学, Auckland University of Technology'}
        ],
    england: [
      { value: 'University of Exeter', label: 'University of Exeter'},
      { value: 'University of Surrey', label: 'University of Surrey'},
      { value: 'Lancaster University', label: 'Lancaster University'},
      { value: 'Loughborough University', label: 'Loughborough University'},
      { value: 'Leeds University', label: 'Leeds University'},
      { value: 'The University of York', label: 'The University of York'},
      { value: 'University of Southampton', label: 'University of Southampton'},
      { value: 'University of Birmingham', label: 'University of Birmingham'},
      { value: 'University of East Anglia', label: 'University of East Anglia'},
      { value: 'University of Sussex', label: 'University of Sussex'},
      { value: 'University of Bristol', label: 'University of Bristol'},
      { value: 'University of Sheffield', label: 'University of Sheffield'},
      { value: 'University College London', label: 'University College London'},
      { value: 'University of Reading', label: 'University of Reading'},
      { value: 'The University of Edinburgh', label: 'The University of Edinburgh'},
      { value: 'University of Kent', label: 'University of Kent'},
      { value: 'Newcastle University', label: 'Newcastle University'},
      { value: 'University of Nottingham', label: 'University of Nottingham'},
      { value: 'University of Glasgow', label: 'University of Glasgow'},
      { value: 'King\'s College London', label: 'King\'s College London'},
      { value: 'University of Leicester', label: 'University of Leicester'},
      { value: 'The University of Manchester', label: 'The University of Manchester'},
      { value: 'Aston University', label: 'Aston University'},
      { value: 'Queen\'s University, Belfast', label: 'Queen\'s University, Belfast'},
      { value: 'University of Essex', label: 'University of Essex'},
      { value: 'University of Dundee', label: 'University of Dundee'},
      { value: 'University of Liverpool', label: 'University of Liverpool'},
      { value: 'University of Buckingham', label: 'University of Buckingham'},
      { value: 'City University London', label: 'City University London'},
      { value: 'School of Oriental and African Studies', label: 'School of Oriental and African Studies'},
      { value: 'University of Aberdeen', label: 'University of Aberdeen'},
      { value: 'Bangor University', label: 'Bangor University'},
      { value: 'De Montfort University', label: 'De Montfort University'},
      { value: 'Oxford Brookes University', label: 'Oxford Brookes University'},
      { value: 'Brunel University', label: 'Brunel University'},
      { value: 'University of Gloucestershire', label: 'University of Gloucestershire'},
      { value: 'Northumbria University', label: 'Northumbria University'},
      { value: 'The University of Winchester', label: 'The University of Winchester'},
      { value: 'University of Huddersfield', label: 'University of Huddersfield'},
      { value: 'The Robert Gordon University', label: 'The Robert Gordon University'},
      { value: 'Sheffield Hallam University', label: 'Sheffield Hallam University'},
      { value: 'University of the West of England', label: 'University of the West of England'},
      { value: 'University of Bradford', label: 'University of Bradford'},
      { value: 'University of Hertfordshire', label: 'University of Hertfordshire'},
      { value: 'Manchester Metropolitan University', label: 'Manchester Metropolitan University'},
      { value: 'The University of Northampton', label: 'The University of Northampton'},
      { value: 'University of Derby ', label: 'University of Derby '},
      { value: 'Middlesex University', label: 'Middlesex University'},
      { value: 'Plymouth University', label: 'Plymouth University'},
      { value: 'University of Brighton', label: 'University of Brighton'},
      { value: 'University of Central Lancashire', label: 'University of Central Lancashire'},
      { value: 'Edinburgh Napier University', label: 'Edinburgh Napier University'},
      { value: 'University of the Arts London', label: 'University of the Arts London'},
      { value: 'University of Sunderland', label: 'University of Sunderland'},
      { value: 'Birmingham City University', label: 'Birmingham City University'},
      { value: 'Kingston University', label: 'Kingston University'},
      { value: 'University of Greenwich', label: 'University of Greenwich'},
      { value: 'University of Westminster', label: 'University of Westminster'},
      { value: 'Coventry University', label: 'Coventry University'},
      { value: 'Swansea University', label: 'Swansea University'},
      { value: 'University of Lincoln', label: 'University of Lincoln'},
      { value: 'Heriot-Watt University ', label: 'Heriot-Watt University '},
      { value: 'University of Portsmouth', label: 'University of Portsmouth'},
      { value: 'University of Stirling', label: 'University of Stirling'},
      { value: 'Nottingham Trent University', label: 'Nottingham Trent University'},
      { value: 'Royal Holloway, University of London', label: 'Royal Holloway, University of London'},
      { value: 'University of Strathclyde', label: 'University of Strathclyde'},
      { value: 'University of Ulster', label: 'University of Ulster'},
      { value: 'Goldsmiths, University of London', label: 'Goldsmiths, University of London'},
      { value: 'Leeds Beckett University', label: 'Leeds Beckett University'},
      { value: 'University of Bath', label: 'University of Bath'},
      { value: 'Bath Spa University', label: 'Bath Spa University'},
      { value: 'The University of Hull', label: 'The University of Hull'},
      { value: 'Bournemouth University', label: 'Bournemouth University'},
      { value: 'University of Salford', label: 'University of Salford'},
      { value: 'University of Bedfordshire', label: 'University of Bedfordshire'},
      { value: 'Keele University', label: 'Keele University'},
      { value: 'Roehampton University', label: 'Roehampton University'},
      { value: 'University of Gloucestershire', label: 'University of Gloucestershire'},
      { value: 'London Metropolitan University', label: 'London Metropolitan University'},
      { value: 'University of Chichester', label: 'University of Chichester'},
      { value: 'University of Chester', label: 'University of Chester'},
      { value: 'London South Bank University', label: 'London South Bank University'},
      { value: 'Queen Margaret University Edinburgh', label: 'Queen Margaret University Edinburgh'},
      { value: 'Glasgow Caledonian University', label: 'Glasgow Caledonian University'},
      { value: 'Anglia Ruskin University', label: 'Anglia Ruskin University'},
      { value: 'The University of Sunderlan', label: 'The University of Sunderlan'},
      { value: '卡迪夫大学 Cardiff University', label: '卡迪夫大学 Cardiff University'},
      { value: '布鲁内尔大学 Brunel University London', label: '布鲁内尔大学 Brunel University London'},
      { value: 'Falmouth University', label: 'Falmouth University'}],

    australia: [
      { value: 'ANU,  Australian National University', label: 'ANU,  Australian National University'},
      { value: 'Melb, UniMelb,  The University of Melbourne', label: 'Melb, UniMelb,  The University of Melbourne'},
      { value: 'MIT, Melbourne Insitute of Technology, Sydney', label: 'MIT, Melbourne Insitute of Technology, Sydney'},
      { value: 'USYD,  The University of Sydney', label: 'USYD,  The University of Sydney'},
      { value: 'UNSW ,  University of New South Wales', label: 'UNSW ,  University of New South Wales'},
      { value: 'UQ,  The University of Queensland', label: 'UQ,  The University of Queensland'},
      { value: 'MONASH,  Monash University', label: 'MONASH,  Monash University'},
      { value: 'MONASH,  Monash Sunway College', label: 'MONASH,  Monash Sunway College'},
      { value: 'UWA,  The University of Western Australia', label: 'UWA,  The University of Western Australia'},
      { value: 'Adelaide,  The University of Adelaide', label: 'Adelaide,  The University of Adelaide'},
      { value: 'UTS ,  University of Technology, Sydney', label: 'UTS ,  University of Technology, Sydney'},
      { value: 'MQ,  Macquarie University', label: 'MQ,  Macquarie University'},
      { value: 'UOW,  University of Wollongong', label: 'UOW,  University of Wollongong'},
      { value: 'NCL, 纽大,  University of Newcastle Australia', label: 'NCL, 纽大,  University of Newcastle Australia'},
      { value: 'QUT,  The Queensland University of Technology', label: 'QUT,  The Queensland University of Technology'},
      { value: 'RMIT,  RMIT University', label: 'RMIT,  RMIT University'},
      { value: 'CURTIN,  Curtin University', label: 'CURTIN,  Curtin University'},
      { value: 'UNISA,  The University of South Australia', label: 'UNISA,  The University of South Australia'},
      { value: 'DEAKIN,  Deakin University', label: 'DEAKIN,  Deakin University'},
      { value: 'GU,  Griffith University', label: 'GU,  Griffith University'},
      { value: 'UTAS,  University of Tasmania', label: 'UTAS,  University of Tasmania'},
      { value: 'JCU,  James Cook University', label: 'JCU,  James Cook University'},
      { value: 'La Trobe,  La Trobe University', label: 'La Trobe,  La Trobe University'},
      { value: 'BOND,  Bond University', label: 'BOND,  Bond University'},
      { value: 'FLINDERS,  The Flinders University', label: 'FLINDERS,  The Flinders University'},
      { value: 'SUT,  Swinburne University of Technology', label: 'SUT,  Swinburne University of Technology'},
      { value: 'CDU,  Charles Darwin University', label: 'CDU,  Charles Darwin University'},
      { value: 'Murdoch,  Murdoch University', label: 'Murdoch,  Murdoch University'},
      { value: 'UC,  University of Canberra', label: 'UC,  University of Canberra'},
      { value: 'VUT,  Victoria University', label: 'VUT,  Victoria University'},
      { value: 'UNE,  University of New England', label: 'UNE,  University of New England'},
      { value: 'UWS,  University of Western Sydney', label: 'UWS,  University of Western Sydney'},
      { value: 'CSU,  Charles Sturt University', label: 'CSU,  Charles Sturt University'},
      { value: 'ECU,  Edith Cowan University', label: 'ECU,  Edith Cowan University'},
      { value: 'USQ,  University of Southern Queensland', label: 'USQ,  University of Southern Queensland'},
      { value: 'CQU,  Central Queensland University', label: 'CQU,  Central Queensland University'},
      { value: 'Top Education Institute Sydney, Australia', label: 'Top Education Institute Sydney, Australia'},
      { value: '澳大利亚泰勒学院 taylors college', label: '澳大利亚泰勒学院 taylors college'},
      { value: 'SCU,  Southern Cross University', label: 'SCU,  Southern Cross University'},

      { value: 'SIBT, 麦考瑞大学', label: 'SIBT, 麦考瑞大学'},
      { value: 'MQC, 麦考瑞大学悉尼市中心校区', label: 'MQC, 麦考瑞大学悉尼市中心校区'},
      { value: 'QIBT, 格里菲斯大学', label: 'QIBT, 格里菲斯大学'},
      { value: 'MIBT, 迪肯大学', label: 'MIBT, 迪肯大学'},
      { value: 'SAIBT, 南澳国立大学', label: 'SAIBT, 南澳国立大学'},
      { value: 'ACN, 拉筹伯大学悉尼校区', label: 'ACN, 拉筹伯大学悉尼校区'},
      { value: 'EI, 阿德莱德, 弗林德斯, 南澳国立', label: 'EI, 阿德莱德, 弗林德斯, 南澳国立'},
      { value: 'PIBT, 埃迪斯科文大学', label: 'PIBT, 埃迪斯科文大学'},
      { value: 'CUS, 科廷大学悉尼校区', label: 'CUS, 科廷大学悉尼校区'},
      { value: 'CIC, 科廷科技大学', label: 'CIC, 科廷科技大学'},
      { value: 'LTM, 拉筹伯大学墨尔本校区', label: 'LTM, 拉筹伯大学墨尔本校区'},
      { value: 'Kaplan business school', label: 'Kaplan business school'},
      { value: '麦克莱尔学院', label: '麦克莱尔学院'},
      { value: 'TAFE, 澳大利亚继续教育学院', label: 'TAFE, 澳大利亚继续教育学院'}
    ],
    canada: [
      { value: 'UBC, The University of British Columbia', label: 'UBC, The University of British Columbia'},
      { value: 'Royal Roads University', label: 'Royal Roads University'},
      { value: '西蒙菲莎大学（Simon Fraser）', label: '西蒙菲莎大学（Simon Fraser）'},
      { value: '滑铁卢大学（Waterloo）', label: '滑铁卢大学（Waterloo）'},
      { value: '维多利亚大学（University of Victoria）', label: '维多利亚大学（University of Victoria）'},
      { value: '卡尔顿大学(Carleton)', label: '卡尔顿大学(Carleton)'},
      { value: '新不伦瑞克大学（New Brunswick）', label: '新不伦瑞克大学（New Brunswick）'},
      { value: '纽芬兰纪念大学(Memorial)', label: '纽芬兰纪念大学(Memorial)'},
      { value: '约克大学(York)', label: '约克大学(York)'},
      { value: '劳里埃大学(Wilfrid Laurier)', label: '劳里埃大学(Wilfrid Laurier)'},
      { value: '康考迪亚大学(Concordia)', label: '康考迪亚大学(Concordia)'},
      { value: '瑞尔森大学(Ryerson)', label: '瑞尔森大学(Ryerson)'},
      { value: '魁北克大学蒙特利尔分校(UQAM)', label: '魁北克大学蒙特利尔分校(UQAM)'},
      { value: '里贾纳大学(Regina)', label: '里贾纳大学(Regina)'},
      { value: '温莎大学(Windsor)', label: '温莎大学(Windsor)'},
      { value: '布鲁克大学(Brock)', label: '布鲁克大学(Brock)'},
      { value: '北英属哥伦比亚大学(UNBC)', label: '北英属哥伦比亚大学(UNBC)'},
      { value: '莱斯布里奇大学(Lethbridge)', label: '莱斯布里奇大学(Lethbridge)'},
      { value: '川特大学(Trent)', label: '川特大学(Trent)'},
      { value: '阿卡迪亚大学(Acadia)', label: '阿卡迪亚大学(Acadia)'},
      { value: '圣弗朗西斯泽维尔大学(St. Francis Xavier)', label: '圣弗朗西斯泽维尔大学(St. Francis Xavier)'},
      { value: '圣玛丽大学(Saint Mary’s)', label: '圣玛丽大学(Saint Mary’s)'},
      { value: '爱德华王子岛大学(UPEI)', label: '爱德华王子岛大学(UPEI)'},
      { value: '毕索大学(Bishop’s)', label: '毕索大学(Bishop’s)'},
      { value: '劳伦森大学(Lautentian)', label: '劳伦森大学(Lautentian)'},
      { value: '湖首大学(Lakehead)', label: '湖首大学(Lakehead)'},
      { value: '安大略省理工大学(UOIT)', label: '安大略省理工大学(UOIT)'},
      { value: '温尼伯大学(Winnipeg)', label: '温尼伯大学(Winnipeg)'},
      { value: '麦克敦大学(Moncton)', label: '麦克敦大学(Moncton)'},
      { value: '圣托马斯大学(St. Thomas)', label: '圣托马斯大学(St. Thomas)'},
      { value: '麦吉尔大学(McGill)', label: '麦吉尔大学(McGill)'},
      { value: '多伦多大学(Toronto)', label: '多伦多大学(Toronto)'},
      { value: '英属哥伦比亚大学(UBC)', label: '英属哥伦比亚大学(UBC)'},
      { value: '女王大学(Queen’s)', label: '女王大学(Queen’s)'},
      { value: '阿尔伯塔大学(Alberta)', label: '阿尔伯塔大学(Alberta)'},
      { value: '麦克马斯特大学(McMaster)', label: '麦克马斯特大学(McMaster)'},
      { value: '戴尔豪斯大学(Dalhousie)', label: '戴尔豪斯大学(Dalhousie)'},
      { value: '渥太华大学(Ottawa)', label: '渥太华大学(Ottawa)'},
      { value: '西安大略大学(Western)', label: '西安大略大学(Western)'},
      { value: '卡尔加里大学(Calgary)', label: '卡尔加里大学(Calgary)'},
      { value: '蒙特利尔大学(Montreal)', label: '蒙特利尔大学(Montreal)'},
      { value: '拉瓦尔大学(Laval)', label: '拉瓦尔大学(Laval)'},
      { value: '谢布克大学(Sherbrooke)', label: '谢布克大学(Sherbrooke)'},
      { value: '曼尼托巴大学 (Manitoba)', label: '曼尼托巴大学 (Manitoba)'},
      { value: '萨省大学(Saskatchewan)', label: '萨省大学(Saskatchewan)'},
      { value: 'Mount Saint Vincent University', label: 'Mount Saint Vincent University'}],
    hongkong: [
      { value: '香港城市大学', label: '香港城市大学'},
      { value: '香港公开大学', label: '香港公开大学'},
    ]
  }

  @computed get university(){
    let all = []
    for(let value of Object.values(this.universityMap)){
      console.log(value)
      all = all.concat(value)
    }
    console.log(all)
    return all
  }

  @observable degreeLevels = [
  'Master', 
  'Bachelor',
  'Diploma',
  'Foundation',
  'Language School',
  'High School',
];

  @observable englishLevels = [
  'IELTS 3.0', 
  'IELST 3.5',
  'IELTS 4.0',
  'IELTS 4.5',
  'IELTS 5.0',
  'IELTS 5.5',
  'IELTS 6.0',
  'IELTS 6.5',
  'IELTS 7.0',
];

  
  @observable course = observable.map();


  @computed get countries() {
    let countries = []
    let currencyArray = this.currency.values()

    for (let currency of currencyArray) {
      countries.push(currency.country)
    }
    
    return countries
  }

  @computed get currencyArray() {
    return this.currency.values();
  };
  @computed get timezoneArray(){
    return this.timezone.values()
  }
  @computed get Courses(){
    return this.course.values()
  }

  @computed get getCurrency(){
    return createTransformer(id=>this.currency.get(id)||{})
  }
  @computed get getCurrencyByName(){
    return createTransformer(name => {
      for(let c of this.currency.values()){
        if(c.currency === name) return c
      }
      return {}
    })
  }
  @computed get getTimezone(){
    return createTransformer(id=>this.timezone.get(id)||{})
  }
  

  clear() {
    this.currency.clear();
  }

  @action init(){
    this.loadCurrency()
    this.loadTimezone()
    this.loadCourse()
    this.loadUniversities()
    this.getCountry()
  }

  @action loadCurrency() {
    this.isLoading = true;
    return agent.Helper.getCurrency()
      .then(action((Currency) => {
        this.currency.clear();
        Currency.forEach(c => this.currency.set(c.idcurrency, c));
      }))
      .finally(action(() => { this.isLoading = false; }));
  }
  @action loadUniversities() {
    this.isLoading = true;
    return agent.Helper.getAllUni()
        .then(action((uni) => {
          if (uni.length>0){
            this.universities = uni;
          }

        }))
        .finally(action(() => { this.isLoading = false; }));
  }
  @action getCountryUni(country) {
    this.isLoading = true;
    return agent.Helper.getCountryUni(country)
        .then(action((uni) => {
          if (uni.length>0) {
            this.countryuni= uni;
          }
          else{
            this.countryuni =[];
          }
        }))
        .finally(action(() => { this.isLoading = false; }));
  }
  @action getCountry() {
    this.isLoading = true;
    return agent.Helper.getCountry()
        .then(action((uni) => {
          if (uni.length>0) {
            // console.log(uni)
            this.country = uni;
          }
        }))
        .finally(action(() => { this.isLoading = false; }));
  }
  @action loadTimezone() {
    this.isLoading = true;
    return agent.Helper.getTimezone()
      .then(action((timezone) => {
        this.timezone.clear();
        timezone.forEach(c => this.timezone.set(c.idtimezone, c));
      }))
      .finally(action(() => { this.isLoading = false; }));
  }
  @action loadCourse() {
    this.isLoading = true;
    return agent.Helper.getCourse()
      .then(action((res) => {
        this.course.clear();
        res.forEach(c => this.course.set(c.idcource, c));
      }))
      .finally(action(() => { this.isLoading = false; }));
  }
  @action uploadFiles(formData){
    return agent.Helper.uploadFiles(formData)
      .then(action((res) => {
        console.log('helpserStore',res)
        this.files = res;
        return res
      }))
  }

}

export default new HelperStore();

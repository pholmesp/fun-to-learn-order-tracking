import { observable, computed, action, createTransformer } from 'mobx';
import agent from '../agent';

class ExchangeReserveStore {
    @observable isLoading;
    @observable reserves = observable.map();
  
    @computed get exchangeReserves(){
      return this.reserves.values()
    }
  
    @computed get getExchangeReserveByUserId(){
      return createTransformer(iduser => {
        let exchangeReserves = []
        for(let [key, value] of this.reserves) {
          if(value.creatIduser == iduser) {
            exchangeReserves.push(value)
          }
        }
        return exchangeReserves
      })
    }
  
    @action init(){
      this.getAllExchangeReserves()
    }
  
    @action getAllExchangeReserves(){
      this.isLoading = true;
      return agent.Exchange.selectReserve()
        .then(action((res) => { 
          this.reserves.clear()
          if(res) res.forEach(ele => this.reserves.set(ele.idexchangeReserve, ele));
        }))
        .finally(action(() => { this.isLoading = false; }))
    }
  
    @action deleteExchangeReserve(idexchangeReserve) {
        this.isLoading = true;
        return agent.Exchange.deleteReserve(idexchangeReserve)
            .then(action(() => { this.getAllExchangeReserves() }))
            .finally(action(() => { this.isLoading = false; }))
    }
  
    @action updateExchangeReserve(exchangeReserve) {
        this.isLoading = true;
        return agent.Exchange.updateReserve(exchangeReserve)
          .then(action((res) => { 
            this.getAllExchangeReserves()
            return res 
          }))
          .finally(action(() => { this.isLoading = false; }))
    }
    
  }
  
  export default new ExchangeReserveStore();
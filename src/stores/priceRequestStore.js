import { observable, computed, action, createTransformer } from 'mobx';
import agent from '../agent';
import authStore from './authStore'


class PriceRequestStore {
  @observable isLoading;
  @observable priceRequest = observable.map();
  @observable priceQuote = observable.map();
  @observable userPriceQuote = [];

  @computed get PriceRequests(){
    return this.priceRequest.values()
  }

  @computed get getUserPriceRequest(){
    let userPriceRequest = []
    for(let ele of [...this.priceRequest.values()]){
      if(ele.idUser == authStore.currentUser.iduser){
        userPriceRequest.push(ele)
      }
    }
    return userPriceRequest
  }

  @computed get getUserPriceQuote() {
    let userPriceQuotes = []
    for(let ele of [...this.userPriceQuote]) {
      if(ele.user_iduser == authStore.currentUser.iduser) {
        userPriceQuotes.push(ele)
      }
    }
    return userPriceQuotes
  }

  @computed get getPriceRequestByIdpriceRequest(){
    return createTransformer(idpriceRequest=> this.priceRequest.get(idpriceRequest))
  }

  @computed get getQuotesByIdpriceRequest(){
    return createTransformer(idpriceRequest=>{
      if(this.priceQuote.get(idpriceRequest)) return this.priceQuote.get(idpriceRequest)
      else {
        this.getPriceQuoteByIdpriceRequest(idpriceRequest)
        return []
      }
    })
  }

  @action init(){
    this.getAllPriceRequest()
    if(authStore.currentUser.name){
      this.getPriceQuoteByIdUser(authStore.currentUser.iduser)
    }
  }

  @action updatePriceRequest(priceRequest) {
    this.isLoading = true;
    return agent.Price.updateRequest(priceRequest)
      .then(action((res) => { 
        this.init() 
        return res
      }))
      .finally(action(() => { this.isLoading = false; }))
  }

  @action updatePriceQuote(priceQuote) {
    this.isLoading = true;
    return agent.Price.updateQuote(priceQuote)
      .then(action((res) => { 
        this.init()
        return res
      }))
      .finally(action(() => { this.isLoading = false; }))
  }
  @action getAllPriceRequest(){
    this.isLoading = true;
    return agent.Price.allPriceRequest()
      .then(action((res) => { 
        this.priceRequest.clear()
        res.forEach(ele => this.priceRequest.set(ele.idpriceRequest, ele));
      }))
      .finally(action(() => { this.isLoading = false; }))
  }

  @action getPriceQuoteByIdpriceRequest(idrequest){
    return agent.Price.getQuoteByIdpriceRequest(idrequest).then(
      action( res => {
        this.priceQuote.set(idrequest, res)
      })
    )
  }
  @action getPriceQuoteByIdUser(iduser){
    return agent.Price.getUserPriceQuote(iduser).then(
      action( res => {
        this.userPriceQuote = res
      })
    )
  }
}

export default new PriceRequestStore();

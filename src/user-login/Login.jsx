import React from 'react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import { inject, observer } from 'mobx-react';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import config from '../config'


const styles = theme => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing.unit *2,
    marginTop: theme.spacing.unit * 8
  },
  button: {
    margin: theme.spacing.unit,
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit * 2,
    // width: theme.breakpoints.values.sm,
  },
})

@withStyles(styles)
@inject('authStore')
@withRouter
@observer
export default class Login extends React.Component {
  state={
    email: '',
    passwd: '', 
    emailValidate: '',
    passwdValidate: '',
  }



  handleSignIn = (e) => {

    if(this.validate()){
      this.props.authStore.login(this.state.email, this.state.passwd)
      .then(() => {
        this.props.history.push('/')
        window.location.reload();
        // this.props.history.goBack());
      })
    }
  };

  handleInputChange = (event) =>{
    let {id, value} = event.target
    this.setState({
      [id]: value,
      [id+'Validate']: ''
    })
  }

  validate(){
    let pass = true
    if(this.state.email.length===0) {
      this.setState({emailValidate: 'Email Can not be empty'})
      pass = false
    }
    if(this.state.passwd.length === 0){
      this.setState({passwdValidate: 'Password Can not be empty'})
      pass = false
    }
    if(this.state.email.indexOf('@') === -1){
      this.setState({emailValidate: 'Please provide a valide email address'})
      pass = false
    }
    return pass
  }

  render() {
    const { currentUser, isLoading, error } = this.props.authStore;
    const {classes} = this.props
    return (
      <div className={classes.root}>
        <Grid container justify="center" alignItems="flex-start">
          <Grid item lg={4} sm={12}>
            <h1 className="text-xs-center">Sign In</h1>
              <p className="text-xs-center">
                  Need an account? Contact <a href="mailto:admin@funtolearn.co">admin@funtolearn.co</a>
              </p>
            <TextField 
              id="email"
              fullWidth
              required
              value={this.state.email}
              onChange={this.handleInputChange}
              label="Email"
              error ={this.state.emailValidate ? true : false  }
              helperText={this.state.emailValidate}
              className={classes.textField}
              InputLabelProps={{
                 shrink: true,
              }}
            />
             <TextField 
              id="passwd"
              fullWidth
              required
              value={this.state.passwd}
              type="password"
              onChange={this.handleInputChange}
              label="Password"
              error ={this.state.passwdValidate ? true : false  }
              helperText={this.state.passwdValidate}
              className={classes.textField}
              InputLabelProps={{
                 shrink: true,
              }}
            />
            <br />
            <Grid container item xs={12}>
              <Grid item xs={9}></Grid>
              <Grid item xs={3}>
                <Button  variant="contained" onClick={this.handleSignIn}  color="secondary" className={classes.button}>Sign In</Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import loading from '../util/loading'
import {Modal, Form, Input, Alert, notification} from 'antd';
import agent from "../agent";
import userStore from "../stores/userStore";
import authStore from "../stores/authStore";

const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  button: {
    margin: theme.spacing.unit,
  },
});

const EditInfo = Form.create({ name: 'form_in_modal' })(
    class extends React.Component {
      render() {
        const { visible, onCancel, onCreate, form, type } = this.props;
        const { getFieldDecorator } = form;
        return (
            <Modal
                visible={visible}
                title= {type}
                okText="Create"
                onCancel={onCancel}
                onOk={onCreate}
            >
              <Form layout="vertical">
                <Form.Item label={type} >
                  {getFieldDecorator('value', {
                    rules: [{ required: true, message: 'Please input the Information' }],
                  })(<Input />)}
                </Form.Item>
              </Form>
            </Modal>
        );
      }
    },
);
const openNotificationWithIcon = (type) => {
  notification.config({
    placement: 'bottomRight',
    bottom: 50,
    duration: 5,
  });

  notification[type]({
    message: 'Edit Successfully',
  });
};


@withStyles(styles)
@inject('helperStore', 'authStore','userStore')
@withRouter
@observer
export default class Profile extends Component{

  state = {
    visible: false,
    type:"bankInfo"
  };

  handleLogout = () => {
    this.props.authStore.logout()
    this.props.history.push('/dashboard')
  }
  showModal = (type) => {
    this.setState({ visible: true, type: type});
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  saveFormRef = formRef => {
    this.formRef = formRef;
  };
  handleCreate = () => {
    const { form } = this.formRef.props;
    form.validateFields((err, values) => {
      if (err) {
        return;
      }
      if(this.state.type==="note"){
        authStore.updateUserNote(values.value.toString(),authStore.currentUser.iduser,authStore.currentUser.email)
            .then((res)=>{
              console.log(res)
              if (res){
                form.resetFields();
                this.setState({ visible: false });
                openNotificationWithIcon('success')
              }
            })
      }
      if(this.state.type==="phone"){
        authStore.updateUserPhone(values.value.toString(),authStore.currentUser.iduser,authStore.currentUser.email)
            .then((res)=>{
              console.log(res)
              if (res){
                form.resetFields();
                this.setState({ visible: false });
                openNotificationWithIcon('success')
              }
            })
      }

    })
  };

  render(){
    const { classes, authStore } = this.props;
    const user = authStore.currentUser
    user.name||loading

    return(
      <Grid className={classes.root}>
        <h3>Profile</h3>
        <Table>
          <TableBody>
            <TableRow>
              <TableCell align="right">Name</TableCell>
              <TableCell align="left">{user.name}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell align="right">Email</TableCell>
              <TableCell align="left">{user.email}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell align="right">Phone</TableCell>
              <TableCell align="left">
                {user.phone}
                <Button color="primary" id="phone" onClick={()=>this.showModal('phone')} >
                  Edit
                </Button>

              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell align="right">Country</TableCell>
              <TableCell align="left">{user.country}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell align="right">Role</TableCell>
              <TableCell align="left">{user.role.join(', ')}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell align="right">WeChat</TableCell>
              <TableCell align="left">{user.wechat}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell align="right">Bank Info</TableCell>
              <TableCell align="left">
                {user.note}
                <Button color="primary" id="bankInfo" onClick={()=>this.showModal('note')}>
                  Edit
                </Button>
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell align="right">Join Date</TableCell>
              <TableCell align="left">{user.createTime}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell align="right"></TableCell>
              <TableCell align="left">
                <Button color="secondary" onClick={this.handleLogout}>
                    Logout
                </Button>
              </TableCell>
            </TableRow>
          </TableBody>
          <EditInfo
              wrappedComponentRef={this.saveFormRef}
              visible={this.state.visible}
              onCancel={this.handleCancel}
              onCreate={this.handleCreate}
              errorVisible={this.state.errorVisible}
              onclose={this.onClose}
              type={this.state.type}
          />
        </Table>
        <p>
          Profile information incorrect? <br/>
          Need help for updating profile ? <br />
          Contact administrator by email: <a href="mailto:admin@funtolearn.co"> admin@funtolearn.co</a>
        </p>
      </Grid>
    );
  }
} 

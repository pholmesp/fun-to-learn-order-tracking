import React from 'react';
import { inject, observer } from 'mobx-react';
import { Switch, Route, withRouter, Link } from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Popover from '@material-ui/core/Popover'

import MenuIcon from '@material-ui/icons/Menu';
import MailIcon from '@material-ui/icons/Mail';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import PersonOutline from '@material-ui/icons/PersonOutline';

import classNames from 'classnames';
import {PermissibleRender} from '@brainhubeu/react-permissible';

import AdminPanel from './admin-panel/AdminPanel'
import ExchangePanel from './exchange-panel/ExchangePanel'
import SalePanel from './sale-panel/SalePanel'
import QualityPanel from './quality-panel/QualityPanel'
import CounsellorPanel from './counsellor-panel/CounsellorPanel'
import AccountPanel from './account-panel/AccountPanel'

import Login from './user-login/Login'
import Profile from './user-login/Profile'
import NotificationList from './common/NotificationBadge'
// import {  Badge, Icon, Dropdown, Popover } from 'antd';
import {getRolePanels, getRoleRoutes} from './privilegeRoutes'
import ServicesPanel from './services-panel/ServicesPanel';
import config from './config'
import {notification, Modal, Input} from "antd";
import IdleTimer from "react-idle-timer";
import AFK from "./common/afk";
const drawerWidth = 240;



const styles = theme => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  margin: {
    margin: 2,
    marginRight: 3,
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  menuButtonHidden: {
    display: 'none',
  },
  title: {
    flexGrow: 1,
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9,
    },
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 1,
    height: '100vh',
    overflow: 'auto',
  },
  chartContainer: {
    marginLeft: -22,
  },
  tableContainer: {
    height: 320,
  },
  h5: {
    marginBottom: theme.spacing.unit * 2,
  },
});

const StyledBadge = withStyles(theme => ({
  badge: {
    top: '10%',
    right: -15,
    // The border color match the background color.
    border: `1px solid ${
      theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[900]
    }`,
  },
}))(Badge);




@withStyles(styles)
@inject('helperStore', 'authStore')
@withRouter
@observer
export default class Dashboard extends React.Component {
  state = {
    open: false,
    anchorEl: null,
    timer: null,
    visible: false,
    passw: null,
    inputError: false,
    inputText: null
  };

  componentDidMount(){
    this.props.authStore.ws.onopen =()=> {
      // on connecting, do nothing but log it to the console
      console.log('connected' +this.props.authStore.currentUser.iduser)
      let message={
        idUser:this.props.authStore.currentUser.iduser,
        type:'conn'
      }
      this.props.authStore.ws.send(JSON.stringify(message))
    }
    this.props.authStore.ws.onmessage = evt => {
      // on receiving a message, add it to the list of messages
      let message = JSON.parse(evt.data)
      let unreadNum = message.filter(x=>x.status===0).length
      if (unreadNum>0){
        notification.open({
          message: 'New Incoming Message',
          description:
              `You have ${unreadNum} new messages, please check your message center for details.`,
        });
      }
      console.log(JSON.parse(evt.data)
      )
      this.props.authStore.addNotificaitons(JSON.parse(evt.data))
    }
    if(!this.props.authStore.currentUser){
      this.props.history.push('/login')
    }else{
      this.props.history.push('/')

    }
  }

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };
  handleInfoClose = () => {
    this.setState({
      anchorEl: null
    })
  }
  handleInfoShow = (event) =>{
    this.setState({
      anchorEl: event.currentTarget
    })
  }

  handleClickOpen = () => {
    this.setState({
      visible: true,
      timer: null
    });
  };

  handleOk = (pass) => {
  if(this.props.authStore.currentUser.password === pass){
    this.setState({
      visible: false,
      inputError: false,
      inputText: null
    });
  }
  else {
    this.setState({
      visible: true,
      inputError: true,
      inputText: "Wrong password, please try again."
    });
  }
  };


  onIdle=(e)=> {
    console.log('user is idle', e)
    console.log('last active', this.state.timer.getLastActiveTime())
    this.handleClickOpen()
  }



  render() {
    const { classes, authStore } = this.props;
    if(!this.props.authStore.currentUser.name) return(<Login />)
    const roles = this.props.authStore.currentUser.role
    const panels = getRolePanels(roles)
    const routes = getRoleRoutes(roles)


    return (
      <React.Fragment>

        <CssBaseline />

        <div className={classes.root}>

          <AppBar
            position="absolute"
            className={classNames(classes.appBar, this.state.open && classes.appBarShift)}
            color='primary'
          >
            <Toolbar disableGutters={!this.state.open} className={classes.toolbar}>
              <Button
                color="inherit"
                aria-label="Open drawer"
                onClick={this.handleDrawerOpen}
                className={classNames(
                  classes.menuButton,
                  this.state.open && classes.menuButtonHidden,
                )}
              >
                <MenuIcon />
              </Button>
              <Typography
                component="h1"
                variant="h6"
                color="inherit"
                noWrap
                className={classes.title}
              >
                <Link to='/'  className="nav-link">乐亦学 Fun to Learn</Link>
              </Typography>
              <Button color="inherit" component={Link} to="/profile">
                  <PersonOutline />
                  <Typography color="inherit">
                    {this.props.authStore.currentUser.name}
                  </Typography>
              </Button>


              <IconButton aria-describedby="info" onClick={this.handleInfoShow}>
                <Badge
                    className={classes.margin}
                    badgeContent={Array.from(authStore.notifications.values()).filter(x => x.status===0).length}
                    color="secondary">
                  <MailIcon />
                </Badge>
              </IconButton>


                {/*<Dropdown overlay={<NotificationList />} trigger={['click']} placement="bottomLeft">*/}
                {/*  <a className="ant-dropdown-link" href="#">*/}
                {/*    <Icon type="mail" style={{ fontSize: '30px' }} theme="outlined"/>*/}
                {/*  </a>*/}
                {/*</Dropdown>*/}

                {/*<Popover*/}
                {/*    placement="bottom"*/}
                {/*    content={content}*/}
                {/*    trigger="click"*/}
                {/*>*/}
                {/*  <Badge count={authStore.notifications.size} overflowCount={99}>*/}
                {/*    <Icon type="mail" style={{ fontSize: '30px' }} theme="outlined"/>*/}
                {/*  </Badge>*/}
                {/*</Popover>*/}


              <Popover
                id='info'
                open={Boolean(this.state.anchorEl)}
                anchorEl={this.state.anchorEl}
                onClose={this.handleInfoClose}
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'center',
                }}
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'center',
                }}
                style={{
                  padding: '20px',
                  position: 'absolute',
                  maxWidth: '700px'
                }}
              >
                <div>
                  <NotificationList />
                </div>
              </Popover>

            </Toolbar>
          </AppBar>
          
          <Drawer
            variant="permanent"
            classes={{
              paper: classNames(classes.drawerPaper, !this.state.open && classes.drawerPaperClose),
            }}
            open={this.state.open}
          >
            <div className={classes.toolbarIcon}>
              <IconButton onClick={this.handleDrawerClose} >
                <ChevronLeftIcon />
              </IconButton>
            </div>
            <Divider />
            {/* { panels.map( panel => panel) } */}
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin']} oneperm>
                <AdminPanel/>
              </PermissibleRender>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'account']} oneperm>
                <AccountPanel/>
              </PermissibleRender>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'saleman']} oneperm>
                <SalePanel/>
              </PermissibleRender>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={[ 'admin', 'qa']} oneperm>
                <QualityPanel/>
              </PermissibleRender>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['admin', 'exchanger']} oneperm>
                <ExchangePanel/>
              </PermissibleRender>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['counsellor']} oneperm>
                <CounsellorPanel/>
              </PermissibleRender>
              <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['services']} oneperm>
                <ServicesPanel/>
              </PermissibleRender>
              {/* <PermissibleRender userPermissions={authStore.currentUser.role} requiredPermissions={['transaler']} oneperm>
                <AdminPanel/>
              </PermissibleRender> */}
          </Drawer>
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Switch>
              <Route exact path="/login" component={Login} />
              <Route exact path="/profile" component={Profile} />
              {routes.map( route => route)}

          </Switch>
          </main>
        </div>
        <IdleTimer
            ref={ref => { this.state.timer = ref }}
            element={document}
            onIdle={this.onIdle}
            debounce={250}
            timeout={600000} />
        <AFK
            input={this.state.passw}
            error={this.state.inputError}
            text={this.state.inputText}
            visible={this.state.visible}
            handleSubmit={this.handleOk}
            handleInput={this.handleAFKInput}
        />
      </React.Fragment>

    );
  }
}

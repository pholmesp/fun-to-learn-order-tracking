import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search'
import TablePagination from "@material-ui/core/TablePagination";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { gray, lightBlue} from '../common/Colors'

import OrderDetail from '../common/OrderDetail'
import loading from '../util/loading'

const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  paddingTop: {
    paddingTop: theme.spacing.unit,
    backgroundColor: 'None',
  },
  tabsRoot: {
    width: '100%',
    borderBottom: '1px solid #e8e8e8',
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  label:{
    padding: '3px',
    paddingLeft: '5px',
    paddingRight: '5px',
    marginRight: '5px',
    marginTop: '4px',
    marginLeft: '3px',
    borderRadius: 3,
    fontSize: '12px',
    textDecoration: 'None',
    backgroundColor: lightBlue,
  },
});

@withStyles(styles)
@inject('helperStore', 'authStore', 'ordersStore')
@withRouter
@observer
export default class CounsellorOrders extends Component{
  state = {
    currentStatus: 'processing',
    orderStatus: [
      { label: 'Waiting Writer Confirm', value: 'waiting writer'},  
      { label: "Writer Processing", value: 'processing'},
      { label: "QA Verifying", value: 'verifying'},
      { label: "Complete", value: 'complete'},
      { label: "Revision/Modifying", value: 'modifying'},
      { label: "Error/Refund Orders", value: 'error'}],
    search: '',
    page: 0, 
    rowsPerPage: 10,
  };

  handleChange = (event, currentStatus) => {
    this.setState({ 
      currentStatus: currentStatus,
      page: 0
    });
  };
  handleInputChange = event => {
    const {id, value} = event.target
    this.setState({[id]: value})
  }
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = event => {
    this.setState({ rowsPerPage: event.target.value });
  };
 
 
  render(){
    const { classes, ordersStore} = this.props;
    const { currentStatus, orderStatus, page, rowsPerPage } = this.state;
    if(ordersStore.isCounsellorOrdersLoading) return loading
    console.log(ordersStore.isCounsellorOrdersLoading)
    const UserOrdersDetailArray = ordersStore.UserOrdersDetail
    ordersStore.isLoading || loading
    // console.log(UserOrdersDetailArray, UserOrdersDetailArray.length)

    let filtered = []
    if(this.state.search){
      for(let ele of UserOrdersDetailArray){
        if(JSON.stringify(ele).toLowerCase().indexOf(this.state.search.toLowerCase())>=0){
          filtered.push(ele)
        }
      }
    }else{
      filtered = UserOrdersDetailArray
    }

    return(
      <Grid container className={classes.root}>
        <Grid container justify={'space-between'} >
          <Grid item className={classes.search}>
            <TextField 
              id='search'
              placeholder="Search Orders" 
              className={classes.textField} 
              onChange={this.handleInputChange}
              value={this.state.search}
              />
            <IconButton className={classes.iconButton} aria-label="Search">
              <SearchIcon />
            </IconButton>
          </Grid>
        </Grid>
        <Tabs 
          value={currentStatus} 
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
          className={classes.tabsRoot}
          >
          { orderStatus.map((item, key) => <Tab key={key} value={item.value} label={
            <div>{item.label} 
              <span className={this.props.classes.label}>
                {filtered.filter(ele => ele.status === item.value).length||0}
              </span>
           </div>
          } /> )}
        </Tabs>
        <Grid container item spacing={16} xs={12} className={classes.paddingTop}>
          {filtered.filter(ele=>ele.status===currentStatus).slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((order, index) => {
              return <OrderDetail order={order}  key={index}  />
          })}
        </Grid>
        <TablePagination
            rowsPerPageOptions={[10, 20, 50]}
            component="div"
            count={ filtered.filter(ele=>ele.status===currentStatus).length }
            rowsPerPage={rowsPerPage}
            page={page}
            backIconButtonProps={{
              'aria-label': 'Previous Page',
            }}
            nextIconButtonProps={{
              'aria-label': 'Next Page',
            }}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
      </Grid>
    );
  }
} 

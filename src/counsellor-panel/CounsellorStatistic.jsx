import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import blue from '@material-ui/core/colors/blue';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    marginTop: theme.spacing.unit * 1,
    overflowX: 'auto',
  },
  paddingTop: {
    paddingTop: theme.spacing.unit * 2,
  },
  tabsRoot: {
    width: '100%',
    borderBottom: '1px solid #e8e8e8',
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  listitemBackColor: {
    backgroundColor: blue[100],
  }
});

@withStyles(styles)
@inject('ordersStore', 'authStore')
@withRouter
@observer
export default class CounsellorStatistic extends Component{
  state = {
    orderStatus: ['confirming', 'waiting', 'processing', 'verifying', 'complete', 'error']
  };

  handleChange = (event, currentStatus) => {
    this.setState({ currentStatus: currentStatus });
  };
 
  render(){
    const { classes, ordersStore, authStore } = this.props;
    let orderStatus = authStore.currentUser.role.indexOf('counsellor')>=0 ? ['processing', 'verifying', 'complete', 'error'] : ['confirming', 'waiting', 'processing', 'verifying', 'complete', 'error']
    return(
    <Paper className={classes.root}>
      <Grid className={classes.root}>
        <h4>Orders' Statistic</h4>
      </Grid>
      { authStore.currentUser.role.indexOf('counsellor')>=0 ? 
        <Grid className={classes.root}>
          Total {ordersStore.userOrdersDetail.size} orders, {ordersStore.UserOrdersPaidNumber} paid. 
        </Grid>
        : ''
      }
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>No.</TableCell>
            <TableCell align="right">Order Status</TableCell>
            {/* <TableCell align="left">Order Number</TableCell> */}
            <TableCell align="left">My Order Number</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {orderStatus.map((row, index) => {
            return (
              <TableRow key={row.id} key={index}>
                <TableCell >{index+1}</TableCell>
                <TableCell align="right">{row}</TableCell>
                {/* <TableCell align="left">{ordersStore.getStatisticByKey(row)}</TableCell> */}
                <TableCell align="left">{ordersStore.getMyStatisticByKey(row)||0}</TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
      
    );
  }
} 

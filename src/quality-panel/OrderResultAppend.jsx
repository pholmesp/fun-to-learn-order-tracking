import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { Spin, Input, List, Button} from 'antd';
import { GenerateFileLinkEdit} from '../util/generateFileLink'
import {info, bginfo} from '../common/Colors'
import './quality.css'
const { TextArea } = Input;

@inject('ordersStore', 'qualityStore', 'authStore', 'orderResultStore')
@withRouter
@observer
export default class OrderResultAppend extends Component{
  componentDidMount(){
    this.props.qualityStore.setIdorder(this.props.match.params.idorder)
  }
  handleUpdateUserFile = newFileUrl => {
    this.props.qualityStore.updateFiles(newFileUrl)
  }

  render(){
    let { userStore, authStore, qualityStore} = this.props;
    let user = qualityStore.iduser ? userStore.getUserById(qualityStore.iduser) : authStore.currentUser
    return(
      <div className='root'>
        <Spin spinning={qualityStore.isLoading}>
        <List.Item style={{padding: '10px', background: info}}>
          <List.Item.Meta
            title={`${qualityStore.datetime}: ${user.name}(${user.role})`}
            description={
              <div>
                <TextArea rows={12} onChange={(e) => qualityStore.updateText(e.target.value)} value={qualityStore.description||`
Overall description about the quality of the result:

Detail Statement:

      `}/>
              - Marks Schema (noted): 
              <Input
                  id="attachement"
                  label="Marks Schema (noted)"
                  type="file"
                  multiple
                  onChange = {(e)=>qualityStore.uploadFiles(e.target.files, 'Marks Schema (noted)')}
                />
                - Assignment documents (noted): 
                <Input
                  id="attachement"
                  label="Assignment Documents (noted)"
                  type="file"
                  multiple
                  onChange = {(e)=>qualityStore.uploadFiles(e.target.files, 'Assignment Documents (noted)')}
                />
                - Other Support Documents: 
                <Input
                  id="attachement"
                  label="Other"
                  type="file"
                  multiple
                  onChange = {(e)=>qualityStore.uploadFiles(e.target.files, 'Other Support Documents')}
                />
                <GenerateFileLinkEdit files={qualityStore.files} updateFileUrls = {this.handleUpdateUserFile} />
                {/* <hr/>
                <Button  onClick={() => {
                    qualityStore.sumitOrderResult()

                    this.props.cancel()
                  }}>
                  Save
                </Button> */}
              </div>
            }
          />
        
        </List.Item>
        </Spin>
      </div>
    );
  }
} 

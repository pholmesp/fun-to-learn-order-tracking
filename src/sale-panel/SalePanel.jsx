import React, { Component } from 'react';
import { NavLink } from "react-router-dom";
import { withStyles } from '@material-ui/core/styles';
import Collapse from '@material-ui/core/Collapse'
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Public from '@material-ui/icons/Public';
import ListIcon from '@material-ui/icons/List';
import TrackChanges from '@material-ui/icons/TrackChanges';
import TouchApp from '@material-ui/icons/TouchApp';
import CompareArrows from '@material-ui/icons/CompareArrows';
import ViewQuilt from '@material-ui/icons/ViewQuilt';
import HdrWeak from '@material-ui/icons/HdrWeak'
import Folder from '@material-ui/icons/Folder';
import ShowChart from '@material-ui/icons/ShowChart'
import NaturePeople from '@material-ui/icons/NaturePeople';
import LibraryBooks from '@material-ui/icons/LibraryBooks';
import History from '@material-ui/icons/History';
import BlurOn from '@material-ui/icons/BlurOn';
import cyan from '@material-ui/core/colors/cyan';
import YoutubeSearchedFor from '@material-ui/icons/YoutubeSearchedFor';

@withStyles( theme =>({
  active: {
    backgroundColor: cyan[100],
  },
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing.unit * 3,
  },
}))
export default class SalePanel extends Component{
  state = {
    open: true,
  };

  handleClick = () => {
    this.setState(state => ({ open: !state.open }));
  };
  render(){
    const { classes } = this.props;
    return(
      <List>
        <ListItem button onClick={this.handleClick}>
          <ListItemIcon>
            <Public />
          </ListItemIcon>
          <ListItemText inset primary="Market" />
          {this.state.open ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
    
        <Collapse in={this.state.open} timeout="auto" unmountOnExit>
          <Divider />
          <List component="div"  >
            
            <ListItem button  className={classes.nested} component={NavLink} to="/saleOrders" exact activeClassName={classes.active}>
              <ListItemIcon>
                <ListIcon />
              </ListItemIcon>
              <ListItemText primary="Orders" />
            </ListItem>
            <ListItem button component={NavLink} to="/saleHistoryOrders"  className={classes.nested}  exact activeClassName={classes.active} >
              <ListItemIcon>
                <History />
              </ListItemIcon>
              <ListItemText primary="History Orders" />
            </ListItem>
            <ListItem button  className={classes.nested} component={NavLink} to="/salePackages" exact activeClassName={classes.active}>
              <ListItemIcon>
                <Folder />
              </ListItemIcon>
              <ListItemText primary="Packages" />
            </ListItem>
            <ListItem button  className={classes.nested} component={NavLink} to="/courses" exact activeClassName={classes.active}>
              <ListItemIcon>
                <LibraryBooks />
              </ListItemIcon>
              <ListItemText primary="Courses" />
            </ListItem>
            <ListItem button className={classes.nested} component={NavLink} to="/customer" exact activeClassName={classes.active}>
              <ListItemIcon>
                <NaturePeople />
              </ListItemIcon>
              <ListItemText primary="Customer" />
            </ListItem>
            <ListItem button  className={classes.nested} component={NavLink} to="/salePriceRequest" exact activeClassName={classes.active}>
              <ListItemIcon>
                <TrackChanges />
              </ListItemIcon>
              <ListItemText primary="Price Quote" />
            </ListItem>
              <ListItem button  className={classes.nested} component={NavLink} to="/salePriceSearch" exact activeClassName={classes.active}>
                  <ListItemIcon>
                      <YoutubeSearchedFor />
                  </ListItemIcon>
                  <ListItemText primary="Price Search" />
              </ListItem>
            <ListItem button  className={classes.nested} component={NavLink} to="/saleExchangeRequests" exact activeClassName={classes.active}>
              <ListItemIcon>
                <CompareArrows />
              </ListItemIcon>
              <ListItemText primary="Exchange Requests" />
            </ListItem>
            <ListItem button  className={classes.nested} component={NavLink} to="/saleExchangeReserved" exact activeClassName={classes.active}>
              <ListItemIcon>
                <TouchApp />
              </ListItemIcon>
              <ListItemText primary="Exchange Reserved" />
            </ListItem>
            <ListItem button  className={classes.nested} component={NavLink} to="/saleKPI" exact activeClassName={classes.active}>
              <ListItemIcon>
                <ViewQuilt />
              </ListItemIcon>
              <ListItemText primary="Statistic" />
            </ListItem>
            <ListItem button  className={classes.nested} component={NavLink} to="/saleOrderTrend" exact activeClassName={classes.active}>
              <ListItemIcon>
                <ShowChart />
              </ListItemIcon>
              <ListItemText primary="Order Trend" />
            </ListItem>
            <ListItem button  className={classes.nested} component={NavLink} to="/heatmapAnalysis" exact activeClassName={classes.active}>
              <ListItemIcon>
                <BlurOn />
              </ListItemIcon>
              <ListItemText primary="Order increase " />
            </ListItem>
            <ListItem button  className={classes.nested} component={NavLink} to="/statistic" exact activeClassName={classes.active}>
              <ListItemIcon>
                <HdrWeak />
              </ListItemIcon>
              <ListItemText primary="Statistic" />
            </ListItem>

          </List>
        </Collapse>
        <Divider />
      </List>
    );
  }
} 

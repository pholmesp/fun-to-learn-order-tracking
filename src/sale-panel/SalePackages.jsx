import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';

import blue from '@material-ui/core/colors/blue';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search'

import PackageDetail from '../common/PackageDetail'
import AssignFuncBar from '../common/AssignFuncBar'
import moment from 'moment'
import { gray, lightBlue} from '../common/Colors'


const styles = theme => ({
  root: {
    width: '100%',
    padding: theme.spacing.unit * 2,
    backgroundColor: theme.palette.background.paper,
  },
  tabsRoot: {
    width: '100%',
    borderBottom: '1px solid #e8e8e8',
  },
  paddingTop: {
    paddingTop: theme.spacing.unit,
    backgroundColor: 'None',
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  listitemBackColor: {
    backgroundColor: blue[100],

  },
  span: {
    fontSize: '14px',
    color: gray,
    paddingLeft: '5px'
  },
  label:{
    padding: '3px',
    paddingLeft: '5px',
    paddingRight: '5px',
    marginRight: '5px',
    marginTop: '4px',
    marginLeft: '3px',
    borderRadius: 3,
    fontSize: '12px',
    textDecoration: 'None',
    backgroundColor: lightBlue,
  },
});

@withStyles(styles)
@inject('ordersStore')
@withRouter
@observer
export default class SalePackages extends Component{
  state = {
    currentStatus: 'confirming',
    // orderStatus: ['confirming', 'waiting', 'waiting writer', 'processing', 'verifying', 'complete', 'modifying', 'error'],
    orderStatus: [
      { label: 'Admin Confirming', value: 'confirming'}, 
      { label: 'Confirmed & Waiting Assign', value: 'waiting'},
      { label: 'Waiting Writer Confirm', value: 'waiting writer'}, 
      { label: "Writer Processing", value: 'processing'},
      { label: "QA Verifying", value: 'verifying'},
      { label: "Complete", value: 'complete'},
      { label: "Revision/Modifying", value: 'modifying'},
      { label: "Error/Refund Orders", value: 'error'}],
  };
  handleChange = (event, currentStatus) => {
    this.setState({ currentStatus: currentStatus });
  };
 
render(){
  const { classes } = this.props;
  const { currentStatus, orderStatus } = this.state;
  const orderPackages = this.props.ordersStore.SalePackages
  console.log(orderPackages)
  return(
    <Grid container className={classes.root}>
      <Grid container justify={'space-between'} >
        <Grid item className={classes.search}>
          <TextField placeholder="Search Orders" className={classes.textField} />
          <IconButton className={classes.iconButton} aria-label="Search">
            <SearchIcon />
          </IconButton>
        </Grid>
        <AssignFuncBar currentStatus={currentStatus} />
      </Grid>
      <Tabs 
        value={currentStatus} 
        onChange={this.handleChange}
        indicatorColor="primary"
          variant="scrollable"
          textColor="primary"
        className={classes.tabsRoot}
        >
        {/* { orderStatus.map((item, key) => <Tab key={key} value={item} label={item} /> )} */}
        { orderStatus.map((item, key) => <Tab key={key} value={item.value} label={
            <div>{item.label} 
              {/* <span className={this.props.classes.label}>
                {orderStatusCount.get(item.value)||0}
              </span> */}
            </div>
          } /> )}
      </Tabs>
      <Grid container item spacing={16} xs={12} className={classes.paddingTop}>
        {orderPackages.map((row, index) => {
          console.log(row)
          if(row.status == currentStatus && row.idorderPackage)
            return <PackageDetail packageOrder={row} key={index} />
        })}
      </Grid>
    </Grid>
    );
  }
} 


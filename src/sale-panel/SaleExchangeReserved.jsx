import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import {bgerror, gray, bgsuccess} from '../common/Colors'
import loading from '../common/loading'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import ExchangeReserveDetail from '../common/ExchangeReserveDetail'
  
  const styles = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
      overflowX: 'auto',
    },
    table: {
      minWidth: 700,
    },
    row: {
      '&:nth-of-type(odd)': { 
        backgroundColor: theme.palette.background.default,
      },
    },
    button: {
      margin: theme.spacing.unit,
    },
    rightIcon: {
      marginLeft: theme.spacing.unit,
    },
    input: {
      display: 'none',
    },
    bgerror: {
      backgroundColor: bgerror,
    },
    label:{
      padding: '3px',
      paddingLeft: '5px',
      paddingRight: '5px',
      marginRight: '5px',
      marginTop: '4px',
      borderRadius: 3,
      fontSize: '14px',
      color: gray, 
      textDecoration: 'None',
      backgroundColor: bgsuccess,
    },
  });
  
  @withStyles(styles)
  @inject('exchangeReserveStore', 'exchangeRequestStore', 'authStore')
  @withRouter
  @observer
  
  export default class SaleExchangeReserved extends Component {

    state = {
      currentStatus: 'reserved', 
      exchangeReserveStatus: ['reserved', 'confirming', 'completed']
    }
  
    handleChange = (event, currentStatus) => {
      this.setState({ currentStatus: currentStatus });
    };

    render() {
      const { classes, exchangeReserveStore, exchangeRequestStore, authStore } = this.props;
      const { currentStatus, exchangeReserveStatus } = this.state
      const reserves = exchangeReserveStore.getExchangeReserveByUserId(authStore.currentUser.iduser)
      if(exchangeReserveStore.isLoading) return loading  

        return (
          <div className={classes.root}>
            <Tabs 
              value={currentStatus} 
              onChange={this.handleChange}
              indicatorColor="primary"
              textColor="primary"
              className={classes.tabsRoot}
            >
              {exchangeReserveStatus.map((item, key) => <Tab key={key} value={item} label={item} /> )}
            </Tabs>
            <Grid container className={classes.paddingTop}>
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <TableCell>ID</TableCell>
                    <TableCell>Exchange Amount</TableCell>
                    <TableCell>Paid Amount</TableCell>
                    <TableCell>Account</TableCell>
                    <TableCell>Create Time</TableCell>
                    <TableCell>Deadline</TableCell>
                    <TableCell>Document</TableCell>
                    <TableCell>Status</TableCell>
                    <TableCell>Operation</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {reserves.map((row, index) => {
                    let request = exchangeRequestStore.getExchangeRequestById(row.idexchangeRequest)
                    console.log(request)
                    if(request.status == currentStatus) return <ExchangeReserveDetail reserve={row} request={request} key={index} currentStatus={currentStatus}/>
                  })}
                </TableBody>
              </Table>
            </Grid>
          </div>
        )
    }
    
  }
